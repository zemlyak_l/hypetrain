package api

import (
	"fmt"
)

type keyResponse struct {
	Success  bool   `json:"success"`
	Cause    string `json:"cause"`
	Throttle bool   `json:"throttle"`
	Global   bool   `json:"global"`
	Record   struct {
		Key              string `json:"key"`
		Owner            string `json:"owner"`
		Limit            int    `json:"limit"`
		QueriesInPastMin int    `json:"queriesInPastMin"`
		TotalQueries     int    `json:"totalQueries"`
	} `json:"record"`
}

func (client *Client) Key(key string) (res *statusResponse, err error) {
    res = &statusResponse{};
    return res, client.getJson(fmt.Sprintf("%s/key?key=%s", apiUrl, key), res)
}
