package api

import (
	"fmt"
	"net/url"

	"github.com/zemlyak/hypetrain/pkg/api/structs"
)

func (client *Client) Guild(params Params) (res *structs.GuildResponse, err error) {
    res = &structs.GuildResponse{}
	var field string
	switch {
		case params.Player != "":
			field = "player="+params.Player
		case params.Id != "":
			field = "id="+params.Id
		case params.Name != "":
			field = "name="+url.QueryEscape(params.Player)
	}
	fmt.Println(field)
    return res, client.getJson(fmt.Sprintf("%s/guild?key=%s&%s", apiUrl, client.key, field), res)
}
