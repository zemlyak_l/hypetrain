package api

import (
	"fmt"
)

type statusResponse struct {
	Success  bool   `json:"success"`
	UUID     string `json:"uuid"`
	Cause    string `json:"cause"`
	Throttle bool   `json:"throttle"`
	Global   bool   `json:"global"`
	Session  struct {
		Online   bool   `json:"online"`
		GameType string `json:"gameType"`
		Mode     string `json:"mode"`
		Map      string `json:"map"`
	} `json:"session"`
}

func (client *Client) Status(uuid string) (res *statusResponse, err error) {
	res = &statusResponse{};
	return res, client.getJson(fmt.Sprintf("%s/status?key=%s&uuid=%s", apiUrl, client.key, uuid), res)
}

