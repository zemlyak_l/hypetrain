package api

import (
	"fmt"

	"github.com/zemlyak/hypetrain/pkg/api/structs"
)

func (client *Client) Player(uuid string) (res *structs.PlayerResponse, err error) {
    res = &structs.PlayerResponse{};
    return res, client.getJson(fmt.Sprintf("%s/player?key=%s&uuid=%s", apiUrl, client.key, uuid), res)
}
