package api_test

import (
	"testing"
	"os"

	"github.com/zemlyak/hypetrain/pkg/api"
)

func TestGuild(t *testing.T) {
	hypetrain := api.New(os.Getenv("HYPIXEL_KEY"));
	// by uuid
	res, err := hypetrain.Guild(api.Params{Player: "1c6825bf2ae44169aa0fde74d1cf007f"})
    t.Logf("%+v", res)
	if err != nil {
		t.Errorf("Error while testing: %s", err)
	}

	// by guild id
	res, err = hypetrain.Guild(api.Params{Id: "5f663f398ea8c99fd45b27ba"})
    t.Logf("%+v", res)
	if err != nil {
		t.Errorf("Error while testing: %s", err)
	}

	// by guild name
	res, err = hypetrain.Guild(api.Params{Name: "The the the the"})
    t.Logf("%+v", res)
	if err != nil {
		t.Errorf("Error while testing: %s", err)
	}
}
