package api

import (
	"time"

	"github.com/valyala/fasthttp"
)

type Client struct {
	http *fasthttp.Client
	key string
}

func New(key string) *Client {
	return &Client{
		key: key,
		http: &fasthttp.Client{
			ReadTimeout: time.Second*5,
		},
	}
}
