package api_test

import (
	"os"
	"testing"

	"github.com/zemlyak/hypetrain/pkg/api"
)

func TestStatus(t *testing.T) {
	hypetrain := api.New(os.Getenv("HYPIXEL_KEY"));
	res, err := hypetrain.Status("1c6825bf2ae44169aa0fde74d1cf007f")
	t.Logf("%+v", res)
	if err != nil {
		t.Errorf("Error while testing: %s", err)
	}
}
