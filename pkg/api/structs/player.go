package structs

type PlayerResponse struct {
	Success bool `json:"success"`
	Cause    string `json:"cause"`
	Throttle bool   `json:"throttle"`
	Global   bool   `json:"global"`
	Player  struct {
		ID           string `json:"_id"`
		Achievements map[string]int  `json:"achievements"`
		AchievementsOneTime []string `json:"achievementsOneTime"`
		Displayname         string   `json:"displayname"`
		FirstLogin          int64    `json:"firstLogin"`
		FriendRequests      []string `json:"friendRequests"`
		Karma               int      `json:"karma"`
		KnownAliases        []string `json:"knownAliases"`
		KnownAliasesLower   []string `json:"knownAliasesLower"`
		LastLogin           int64    `json:"lastLogin"`
		NetworkExp          float64      `json:"networkExp"`
		Playername          string   `json:"playername"`
		Stats               struct {
			Arcade struct {
				BountyKillsOneinthequiver                     int     `json:"bounty_kills_oneinthequiver"`
				Coins                                         float64 `json:"coins"`
				DeathsOneinthequiver                          int     `json:"deaths_oneinthequiver"`
				DeathsThrowOut                                int     `json:"deaths_throw_out"`
				KillsOneinthequiver                           int     `json:"kills_oneinthequiver"`
				Dec2016Achievements                           bool    `json:"dec2016_achievements"`
				Dec2016Achievements2                          bool    `json:"dec2016_achievements2"`
				MonthlyCoinsB                                 int     `json:"monthly_coins_b"`
				HeadshotsDayone                               int     `json:"headshots_dayone"`
				WeeklyCoinsB                                  int     `json:"weekly_coins_b"`
				KillsDayone                                   int     `json:"kills_dayone"`
				MaxWave                                       int     `json:"max_wave"`
				WinsFarmHunt                                  int     `json:"wins_farm_hunt"`
				MonthlyCoinsA                                 int     `json:"monthly_coins_a"`
				MiniwallsActiveKit                            string  `json:"miniwalls_activeKit"`
				WeeklyCoinsA                                  int     `json:"weekly_coins_a"`
				ArrowsHitMiniWalls                            int     `json:"arrows_hit_mini_walls"`
				KillsMiniWalls                                int     `json:"kills_mini_walls"`
				DeathsMiniWalls                               int     `json:"deaths_mini_walls"`
				ArrowsShotMiniWalls                           int     `json:"arrows_shot_mini_walls"`
				WinsMiniWalls                                 int     `json:"wins_mini_walls"`
				WitherDamageMiniWalls                         int     `json:"wither_damage_mini_walls"`
				FinalKillsMiniWalls                           int     `json:"final_kills_mini_walls"`
				GoalsSoccer                                   int     `json:"goals_soccer"`
				WitherKillsMiniWalls                          int     `json:"wither_kills_mini_walls"`
				WinsSoccer                                    int     `json:"wins_soccer"`
				PowerkicksSoccer                              int     `json:"powerkicks_soccer"`
				RoundsSimonSays                               int     `json:"rounds_simon_says"`
				WinsSimonSays                                 int     `json:"wins_simon_says"`
				PropHuntHiderWinsHideAndSeek                  int     `json:"prop_hunt_hider_wins_hide_and_seek"`
				HiderWinsHideAndSeek                          int     `json:"hider_wins_hide_and_seek"`
				Showinfobook                                  bool    `json:"showinfobook"`
				BasicZombieKillsZombies                       int     `json:"basic_zombie_kills_zombies"`
				BestRoundZombies                              int     `json:"best_round_zombies"`
				BestRoundZombiesDeadend                       int     `json:"best_round_zombies_deadend"`
				BestRoundZombiesDeadendNormal                 int     `json:"best_round_zombies_deadend_normal"`
				BulletsHitZombies                             int     `json:"bullets_hit_zombies"`
				BulletsShotZombies                            int     `json:"bullets_shot_zombies"`
				DeathsZombies                                 int     `json:"deaths_zombies"`
				DeathsZombiesDeadend                          int     `json:"deaths_zombies_deadend"`
				DeathsZombiesDeadendNormal                    int     `json:"deaths_zombies_deadend_normal"`
				HeadshotsZombies                              int     `json:"headshots_zombies"`
				PigZombieZombieKillsZombies                   int     `json:"pig_zombie_zombie_kills_zombies"`
				TimesKnockedDownZombies                       int     `json:"times_knocked_down_zombies"`
				TimesKnockedDownZombiesDeadend                int     `json:"times_knocked_down_zombies_deadend"`
				TimesKnockedDownZombiesDeadendNormal          int     `json:"times_knocked_down_zombies_deadend_normal"`
				TotalRoundsSurvivedZombies                    int     `json:"total_rounds_survived_zombies"`
				TotalRoundsSurvivedZombiesDeadend             int     `json:"total_rounds_survived_zombies_deadend"`
				TotalRoundsSurvivedZombiesDeadendNormal       int     `json:"total_rounds_survived_zombies_deadend_normal"`
				WindowsRepairedZombies                        int     `json:"windows_repaired_zombies"`
				WindowsRepairedZombiesDeadend                 int     `json:"windows_repaired_zombies_deadend"`
				WindowsRepairedZombiesDeadendNormal           int     `json:"windows_repaired_zombies_deadend_normal"`
				WolfZombieKillsZombies                        int     `json:"wolf_zombie_kills_zombies"`
				ZombieKillsZombies                            int     `json:"zombie_kills_zombies"`
				ZombieKillsZombiesDeadend                     int     `json:"zombie_kills_zombies_deadend"`
				ZombieKillsZombiesDeadendNormal               int     `json:"zombie_kills_zombies_deadend_normal"`
				EmpoweredZombieKillsZombies                   int     `json:"empowered_zombie_kills_zombies"`
				PlayersRevivedZombies                         int     `json:"players_revived_zombies"`
				PlayersRevivedZombiesDeadend                  int     `json:"players_revived_zombies_deadend"`
				PlayersRevivedZombiesDeadendNormal            int     `json:"players_revived_zombies_deadend_normal"`
				TntBabyZombieKillsZombies                     int     `json:"tnt_baby_zombie_kills_zombies"`
				DoorsOpenedZombies                            int     `json:"doors_opened_zombies"`
				DoorsOpenedZombiesDeadend                     int     `json:"doors_opened_zombies_deadend"`
				DoorsOpenedZombiesDeadendNormal               int     `json:"doors_opened_zombies_deadend_normal"`
				FastestTime10Zombies                          int     `json:"fastest_time_10_zombies"`
				FastestTime10ZombiesDeadendNormal             int     `json:"fastest_time_10_zombies_deadend_normal"`
				FireZombieKillsZombies                        int     `json:"fire_zombie_kills_zombies"`
				MagmaCubeZombieKillsZombies                   int     `json:"magma_cube_zombie_kills_zombies"`
				MagmaZombieKillsZombies                       int     `json:"magma_zombie_kills_zombies"`
				BlazeZombieKillsZombies                       int     `json:"blaze_zombie_kills_zombies"`
				BestRoundZombiesAlienarcadium                 int     `json:"best_round_zombies_alienarcadium"`
				BestRoundZombiesAlienarcadiumNormal           int     `json:"best_round_zombies_alienarcadium_normal"`
				BlobZombieKillsZombies                        int     `json:"blob_zombie_kills_zombies"`
				DeathsZombiesAlienarcadium                    int     `json:"deaths_zombies_alienarcadium"`
				DeathsZombiesAlienarcadiumNormal              int     `json:"deaths_zombies_alienarcadium_normal"`
				FastestTime10ZombiesAlienarcadiumNormal       int     `json:"fastest_time_10_zombies_alienarcadium_normal"`
				RainbowZombieKillsZombies                     int     `json:"rainbow_zombie_kills_zombies"`
				SentinelZombieKillsZombies                    int     `json:"sentinel_zombie_kills_zombies"`
				SkeletonZombieKillsZombies                    int     `json:"skeleton_zombie_kills_zombies"`
				SpaceBlasterZombieKillsZombies                int     `json:"space_blaster_zombie_kills_zombies"`
				SpaceGruntZombieKillsZombies                  int     `json:"space_grunt_zombie_kills_zombies"`
				TimesKnockedDownZombiesAlienarcadium          int     `json:"times_knocked_down_zombies_alienarcadium"`
				TimesKnockedDownZombiesAlienarcadiumNormal    int     `json:"times_knocked_down_zombies_alienarcadium_normal"`
				TotalRoundsSurvivedZombiesAlienarcadium       int     `json:"total_rounds_survived_zombies_alienarcadium"`
				TotalRoundsSurvivedZombiesAlienarcadiumNormal int     `json:"total_rounds_survived_zombies_alienarcadium_normal"`
				WindowsRepairedZombiesAlienarcadium           int     `json:"windows_repaired_zombies_alienarcadium"`
				WindowsRepairedZombiesAlienarcadiumNormal     int     `json:"windows_repaired_zombies_alienarcadium_normal"`
				WormSmallZombieKillsZombies                   int     `json:"worm_small_zombie_kills_zombies"`
				WormZombieKillsZombies                        int     `json:"worm_zombie_kills_zombies"`
				ZombieKillsZombiesAlienarcadium               int     `json:"zombie_kills_zombies_alienarcadium"`
				ZombieKillsZombiesAlienarcadiumNormal         int     `json:"zombie_kills_zombies_alienarcadium_normal"`
				BestRoundZombiesBadblood                      int     `json:"best_round_zombies_badblood"`
				BestRoundZombiesBadbloodNormal                int     `json:"best_round_zombies_badblood_normal"`
				DeathsZombiesBadblood                         int     `json:"deaths_zombies_badblood"`
				DeathsZombiesBadbloodNormal                   int     `json:"deaths_zombies_badblood_normal"`
				DoorsOpenedZombiesBadblood                    int     `json:"doors_opened_zombies_badblood"`
				DoorsOpenedZombiesBadbloodNormal              int     `json:"doors_opened_zombies_badblood_normal"`
				FamilyTwinBlueZombieKillsZombies              int     `json:"family_twin_blue_zombie_kills_zombies"`
				PlayersRevivedZombiesBadblood                 int     `json:"players_revived_zombies_badblood"`
				PlayersRevivedZombiesBadbloodNormal           int     `json:"players_revived_zombies_badblood_normal"`
				SlimeZombieKillsZombies                       int     `json:"slime_zombie_kills_zombies"`
				SlimeZombieZombieKillsZombies                 int     `json:"slime_zombie_zombie_kills_zombies"`
				TimesKnockedDownZombiesBadblood               int     `json:"times_knocked_down_zombies_badblood"`
				TimesKnockedDownZombiesBadbloodNormal         int     `json:"times_knocked_down_zombies_badblood_normal"`
				TotalRoundsSurvivedZombiesBadblood            int     `json:"total_rounds_survived_zombies_badblood"`
				TotalRoundsSurvivedZombiesBadbloodNormal      int     `json:"total_rounds_survived_zombies_badblood_normal"`
				WerewolfZombieKillsZombies                    int     `json:"werewolf_zombie_kills_zombies"`
				WindowsRepairedZombiesBadblood                int     `json:"windows_repaired_zombies_badblood"`
				WindowsRepairedZombiesBadbloodNormal          int     `json:"windows_repaired_zombies_badblood_normal"`
				WitchZombieKillsZombies                       int     `json:"witch_zombie_kills_zombies"`
				ZombieKillsZombiesBadblood                    int     `json:"zombie_kills_zombies_badblood"`
				ZombieKillsZombiesBadbloodNormal              int     `json:"zombie_kills_zombies_badblood_normal"`
				ZombiesHideTutorials                          bool    `json:"zombies_hideTutorials"`
				StampLevel                                    int     `json:"stamp_level"`
				EggsFoundEasterSimulator                      int     `json:"eggs_found_easter_simulator"`
				FastestTime10ZombiesBadbloodNormal            int     `json:"fastest_time_10_zombies_badblood_normal"`
				WitherSkeletonZombieKillsZombies              int     `json:"wither_skeleton_zombie_kills_zombies"`
				CaveSpiderZombieKillsZombies                  int     `json:"cave_spider_zombie_kills_zombies"`
				WitherZombieZombieKillsZombies                int     `json:"wither_zombie_zombie_kills_zombies"`
				ClownZombieKillsZombies                       int     `json:"clown_zombie_kills_zombies"`
				PlayersRevivedZombiesAlienarcadium            int     `json:"players_revived_zombies_alienarcadium"`
				PlayersRevivedZombiesAlienarcadiumNormal      int     `json:"players_revived_zombies_alienarcadium_normal"`
				ItemsFoundScubaSimulator                      int     `json:"items_found_scuba_simulator"`
				TotalPointsScubaSimulator                     int     `json:"total_points_scuba_simulator"`
				WinsScubaSimulator                            int     `json:"wins_scuba_simulator"`
				PoopCollected                                 int     `json:"poop_collected"`
				CandyFoundHalloweenSimulator                  int     `json:"candy_found_halloween_simulator"`
				WinsHalloweenSimulator                        int     `json:"wins_halloween_simulator"`
				WinsGrinchSimulatorV2                         int     `json:"wins_grinch_simulator_v2"`
				LastTourneyAd                                 int64   `json:"lastTourneyAd"`
				GiftsGrinchSimulatorV2                        int     `json:"gifts_grinch_simulator_v2"`
				EnderZombieKillsZombies                       int     `json:"ender_zombie_kills_zombies"`
				EndermiteZombieKillsZombies                   int     `json:"endermite_zombie_kills_zombies"`
				FastestTime20Zombies                          int     `json:"fastest_time_20_zombies"`
				FastestTime20ZombiesDeadendNormal             int     `json:"fastest_time_20_zombies_deadend_normal"`
				GuardianZombieKillsZombies                    int     `json:"guardian_zombie_kills_zombies"`
				SilverfishZombieKillsZombies                  int     `json:"silverfish_zombie_kills_zombies"`
				SkelefishZombieKillsZombies                   int     `json:"skelefish_zombie_kills_zombies"`
				HitwRecordQ                                   int     `json:"hitw_record_q"`
				HitwRecordF                                   int     `json:"hitw_record_f"`
				RoundsHoleInTheWall                           int     `json:"rounds_hole_in_the_wall"`
			} `json:"Arcade"`
			HungerGames struct {
				Coins                   int      `json:"coins"`
				Deaths                  int      `json:"deaths"`
				Kills                   int      `json:"kills"`
				Knight                  int      `json:"knight"`
				Scout                   int      `json:"scout"`
				Wins                    int      `json:"wins"`
				MonthlyKillsA           int      `json:"monthly_kills_a"`
				WeeklyKillsB            int      `json:"weekly_kills_b"`
				MonthlyKillsB           int      `json:"monthly_kills_b"`
				Packages                []string `json:"packages"`
				InGamePresentsCap201927 int      `json:"inGamePresentsCap_2019_27"`
				ChestsOpened            int      `json:"chests_opened"`
				ChestsOpenedKnight      int      `json:"chests_opened_knight"`
				Damage                  int      `json:"damage"`
				DamageKnight            int      `json:"damage_knight"`
				DamageTaken             int      `json:"damage_taken"`
				DamageTakenKnight       int      `json:"damage_taken_knight"`
				GamesPlayed             int      `json:"games_played"`
				GamesPlayedKnight       int      `json:"games_played_knight"`
				TimePlayed              int      `json:"time_played"`
				TimePlayedKnight        int      `json:"time_played_knight"`
				ExpKnight               int      `json:"exp_knight"`
				KillsKnight             int      `json:"kills_knight"`
				KillsTeamsNormal        int      `json:"kills_teams_normal"`
				PotionsDrunk            int      `json:"potions_drunk"`
				PotionsDrunkKnight      int      `json:"potions_drunk_knight"`
				WinsSoloNormal          int      `json:"wins_solo_normal"`
				WinsBackup              int      `json:"wins_backup"`
				Autoarmor               bool     `json:"autoarmor"`
				WinsTeamsNormal         int      `json:"wins_teams_normal"`
				LastTourneyAd           int64    `json:"lastTourneyAd"`
				Afterkill               string   `json:"afterkill"`
				ChestsOpenedScout       int      `json:"chests_opened_scout"`
				DamageScout             int      `json:"damage_scout"`
				DamageTakenScout        int      `json:"damage_taken_scout"`
				ExpScout                int      `json:"exp_scout"`
				GamesPlayedScout        int      `json:"games_played_scout"`
				PotionsDrunkScout       int      `json:"potions_drunk_scout"`
				TimePlayedScout         int      `json:"time_played_scout"`
				ArrowsHit               int      `json:"arrows_hit"`
				ArrowsHitKnight         int      `json:"arrows_hit_knight"`
				KillsSoloNormal         int      `json:"kills_solo_normal"`
				ChosenTaunt             string   `json:"chosen_taunt"`
				Togglekillcounter       int      `json:"togglekillcounter"`
			} `json:"HungerGames"`
			Paintball struct {
				Coins          int      `json:"coins"`
				Deaths         int      `json:"deaths"`
				Kills          int      `json:"kills"`
				ShotsFired     int      `json:"shots_fired"`
				Wins           int      `json:"wins"`
				Packages       []string `json:"packages"`
				ShowKillPrefix bool     `json:"showKillPrefix"`
				Transfusion    int      `json:"transfusion"`
				FavoriteSlots  string   `json:"favorite_slots"`
				Killstreaks    int      `json:"killstreaks"`
				VotesGladiator int      `json:"votes_Gladiator"`
				Hat            string   `json:"hat"`
				VotesJuice     int      `json:"votes_Juice"`
				Fortune        int      `json:"fortune"`
			} `json:"Paintball"`
			Quake struct {
				Barrel                          string   `json:"barrel"`
				Case                            string   `json:"case"`
				Coins                           int      `json:"coins"`
				Deaths                          int      `json:"deaths"`
				Kills                           int      `json:"kills"`
				Killstreaks                     int      `json:"killstreaks"`
				Muzzle                          string   `json:"muzzle"`
				Packages                        []string `json:"packages"`
				Sight                           string   `json:"sight"`
				Trigger                         string   `json:"trigger"`
				Wins                            int      `json:"wins"`
				AlternativeGunCooldownIndicator bool     `json:"alternative_gun_cooldown_indicator"`
				CompassSelected                 bool     `json:"compass_selected"`
				EnableSound                     bool     `json:"enable_sound"`
				MessageYourDeaths               bool     `json:"messageYour Deaths"`
				InstantRespawn                  bool     `json:"instantRespawn"`
				MessageOthersKillsDeaths        bool     `json:"messageOthers' Kills/deaths"`
				ShowDashCooldown                bool     `json:"showDashCooldown"`
				MessageYourKills                bool     `json:"messageYour Kills"`
				MessageCoinMessages             bool     `json:"messageCoin Messages"`
				HighestKillstreak               int      `json:"highest_killstreak"`
				MessageKillstreaks              bool     `json:"messageKillstreaks"`
				MessageMultiKills               bool     `json:"messageMulti-kills"`
				DistanceTravelled               int      `json:"distance_travelled"`
				ShotsFired                      int      `json:"shots_fired"`
				KillsSinceUpdateFeb2017         int      `json:"kills_since_update_feb_2017"`
				VotesForgotten                  int      `json:"votes_Forgotten"`
				DashCooldown                    string   `json:"dash_cooldown"`
				KillsTeams                      int      `json:"kills_teams"`
				KillsDm                         int      `json:"kills_dm"`
				KillsDmTeams                    int      `json:"kills_dm_teams"`
				KillsTimeattack                 int      `json:"kills_timeattack"`
				MessageCoin                     bool     `json:"messageCoin"`
				MessagePowerupCollections       bool     `json:"messagePowerup Collections"`
				Headshots                       int      `json:"headshots"`
				ShowKillPrefix                  bool     `json:"showKillPrefix"`
				KillsSoloTourney                int      `json:"kills_solo_tourney"`
			} `json:"Quake"`
			TNTGames struct {
				Coins                       int      `json:"coins"`
				DeathsBowspleef             int      `json:"deaths_bowspleef"`
				DeathsCapture               int      `json:"deaths_capture"`
				KillsCapture                int      `json:"kills_capture"`
				KineticwizardRegen          int      `json:"kineticwizard_regen"`
				TagsBowspleef               int      `json:"tags_bowspleef"`
				WinsCapture                 int      `json:"wins_capture"`
				SpleefTriple                int      `json:"spleef_triple"`
				SpleefDoublejump            int      `json:"spleef_doublejump"`
				SpleefRepulse               int      `json:"spleef_repulse"`
				CaptureClass                string   `json:"capture_class"`
				AssistsCapture              int      `json:"assists_capture"`
				RecordTntrun                int      `json:"record_tntrun"`
				DoublejumpTntrun            int      `json:"doublejump_tntrun"`
				NewWitherwizardExplode      int      `json:"new_witherwizard_explode"`
				Packages                    []string `json:"packages"`
				NewSpleefDoubleJumps        int      `json:"new_spleef_double_jumps"`
				NewTntagSpeedy              int      `json:"new_tntag_speedy"`
				NewPvprunDoubleJumps        int      `json:"new_pvprun_double_jumps"`
				NewIcewizardExplode         int      `json:"new_icewizard_explode"`
				NewIcewizardRegen           int      `json:"new_icewizard_regen"`
				NewFirewizardRegen          int      `json:"new_firewizard_regen"`
				NewBloodwizardRegen         int      `json:"new_bloodwizard_regen"`
				NewKineticwizardRegen       int      `json:"new_kineticwizard_regen"`
				NewFirewizardExplode        int      `json:"new_firewizard_explode"`
				NewSpleefRepulsor           int      `json:"new_spleef_repulsor"`
				NewBloodwizardExplode       int      `json:"new_bloodwizard_explode"`
				NewSpleefTripleshot         int      `json:"new_spleef_tripleshot"`
				NewTntrunDoubleJumps        int      `json:"new_tntrun_double_jumps"`
				NewKineticwizardExplode     int      `json:"new_kineticwizard_explode"`
				NewWitherwizardRegen        int      `json:"new_witherwizard_regen"`
				RunPotionsSplashedOnPlayers int      `json:"run_potions_splashed_on_players"`
				DeathsTntrun                int      `json:"deaths_tntrun"`
				Winstreak                   int      `json:"winstreak"`
				Wins                        int      `json:"wins"`
				NewSpleefArrowrain          int      `json:"new_spleef_arrowrain"`
				Flags                       struct {
					EnableExplosiveDash     bool `json:"enable_explosive_dash"`
					ShowTipHolograms        bool `json:"show_tip_holograms"`
					ShowTntrunActionbarInfo bool `json:"show_tntrun_actionbar_info"`
				} `json:"flags"`
				KillsTntag    int   `json:"kills_tntag"`
				RecordPvprun  int   `json:"record_pvprun"`
				KillsPvprun   int   `json:"kills_pvprun"`
				DeathsPvprun  int   `json:"deaths_pvprun"`
				LastTourneyAd int64 `json:"lastTourneyAd"`
			} `json:"TNTGames"`
			VampireZ struct {
				Coins         int  `json:"coins"`
				HumanDeaths   int  `json:"human_deaths"`
				HumanKills    int  `json:"human_kills"`
				VampireDeaths int  `json:"vampire_deaths"`
				VampireKills  int  `json:"vampire_kills"`
				UpdatedStats  bool `json:"updated_stats"`
			} `json:"VampireZ"`
			Walls struct {
				Coins  int `json:"coins"`
				Deaths int `json:"deaths"`
				Kills  int `json:"kills"`
				Losses int `json:"losses"`
				Wins   int `json:"wins"`
			} `json:"Walls"`
			SkyWars struct {
				Packages                                                []string `json:"packages"`
				GamesPlayedSkywars                                      int      `json:"games_played_skywars"`
				WinStreak                                               int      `json:"win_streak"`
				LastMode                                                string   `json:"lastMode"`
				SurvivedPlayersSolo                                     int      `json:"survived_players_solo"`
				MeleeKillsSolo                                          int      `json:"melee_kills_solo"`
				KillsWeeklyA                                            int      `json:"kills_weekly_a"`
				KillsSoloNormal                                         int      `json:"kills_solo_normal"`
				Kills                                                   int      `json:"kills"`
				MostKillsGameKitBasicSoloDefault                        int      `json:"most_kills_game_kit_basic_solo_default"`
				KillsKitBasicSoloDefault                                int      `json:"kills_kit_basic_solo_default"`
				DeathsSoloNormal                                        int      `json:"deaths_solo_normal"`
				ChestsOpenedKitBasicSoloDefault                         int      `json:"chests_opened_kit_basic_solo_default"`
				SurvivedPlayers                                         int      `json:"survived_players"`
				ChestsOpened                                            int      `json:"chests_opened"`
				DeathsKitBasicSoloDefault                               int      `json:"deaths_kit_basic_solo_default"`
				MeleeKills                                              int      `json:"melee_kills"`
				KillsSolo                                               int      `json:"kills_solo"`
				Deaths                                                  int      `json:"deaths"`
				DeathsSolo                                              int      `json:"deaths_solo"`
				TimePlayedKitBasicSoloDefault                           int      `json:"time_played_kit_basic_solo_default"`
				BlocksPlaced                                            int      `json:"blocks_placed"`
				MostKillsGameSolo                                       int      `json:"most_kills_game_solo"`
				Coins                                                   int      `json:"coins"`
				Losses                                                  int      `json:"losses"`
				ChestsOpenedSolo                                        int      `json:"chests_opened_solo"`
				MeleeKillsKitBasicSoloDefault                           int      `json:"melee_kills_kit_basic_solo_default"`
				TimePlayedSolo                                          int      `json:"time_played_solo"`
				LossesSolo                                              int      `json:"losses_solo"`
				Quits                                                   int      `json:"quits"`
				SoulsGathered                                           int      `json:"souls_gathered"`
				TimePlayed                                              int      `json:"time_played"`
				SurvivedPlayersKitBasicSoloDefault                      int      `json:"survived_players_kit_basic_solo_default"`
				MostKillsGame                                           int      `json:"most_kills_game"`
				LossesKitBasicSoloDefault                               int      `json:"losses_kit_basic_solo_default"`
				Souls                                                   int      `json:"souls"`
				LossesSoloNormal                                        int      `json:"losses_solo_normal"`
				KillsMonthlyB                                           int      `json:"kills_monthly_b"`
				VoidKillsKitBasicSoloDefault                            int      `json:"void_kills_kit_basic_solo_default"`
				VoidKills                                               int      `json:"void_kills"`
				VoidKillsSolo                                           int      `json:"void_kills_solo"`
				BlocksBroken                                            int      `json:"blocks_broken"`
				AssistsKitBasicSoloDefault                              int      `json:"assists_kit_basic_solo_default"`
				AssistsSolo                                             int      `json:"assists_solo"`
				Assists                                                 int      `json:"assists"`
				EggThrown                                               int      `json:"egg_thrown"`
				GamesSolo                                               int      `json:"games_solo"`
				Games                                                   int      `json:"games"`
				GamesKitBasicSoloDefault                                int      `json:"games_kit_basic_solo_default"`
				EnderpearlsThrown                                       int      `json:"enderpearls_thrown"`
				LongestBowShotKitBasicSoloDefault                       int      `json:"longest_bow_shot_kit_basic_solo_default"`
				LongestBowShot                                          int      `json:"longest_bow_shot"`
				LongestBowShotSolo                                      int      `json:"longest_bow_shot_solo"`
				ArrowsHit                                               int      `json:"arrows_hit"`
				ArrowsShotKitBasicSoloDefault                           int      `json:"arrows_shot_kit_basic_solo_default"`
				ArrowsHitSolo                                           int      `json:"arrows_hit_solo"`
				ArrowsHitKitBasicSoloDefault                            int      `json:"arrows_hit_kit_basic_solo_default"`
				ArrowsShotSolo                                          int      `json:"arrows_shot_solo"`
				ArrowsShot                                              int      `json:"arrows_shot"`
				LossesKitMiningTeamDefault                              int      `json:"losses_kit_mining_team_default"`
				LossesTeamNormal                                        int      `json:"losses_team_normal"`
				AssistsTeam                                             int      `json:"assists_team"`
				ChestsOpenedTeam                                        int      `json:"chests_opened_team"`
				SurvivedPlayersKitMiningTeamDefault                     int      `json:"survived_players_kit_mining_team_default"`
				TimePlayedTeam                                          int      `json:"time_played_team"`
				LossesTeam                                              int      `json:"losses_team"`
				SurvivedPlayersTeam                                     int      `json:"survived_players_team"`
				DeathsTeamNormal                                        int      `json:"deaths_team_normal"`
				TimePlayedKitMiningTeamDefault                          int      `json:"time_played_kit_mining_team_default"`
				DeathsTeam                                              int      `json:"deaths_team"`
				DeathsKitMiningTeamDefault                              int      `json:"deaths_kit_mining_team_default"`
				ChestsOpenedKitMiningTeamDefault                        int      `json:"chests_opened_kit_mining_team_default"`
				AssistsKitMiningTeamDefault                             int      `json:"assists_kit_mining_team_default"`
				MeleeKillsTeam                                          int      `json:"melee_kills_team"`
				KillsTeamNormal                                         int      `json:"kills_team_normal"`
				KillsTeam                                               int      `json:"kills_team"`
				KillsKitMiningTeamDefault                               int      `json:"kills_kit_mining_team_default"`
				MostKillsGameTeam                                       int      `json:"most_kills_game_team"`
				MostKillsGameKitMiningTeamDefault                       int      `json:"most_kills_game_kit_mining_team_default"`
				MeleeKillsKitMiningTeamDefault                          int      `json:"melee_kills_kit_mining_team_default"`
				VoidKillsKitMiningTeamDefault                           int      `json:"void_kills_kit_mining_team_default"`
				VoidKillsTeam                                           int      `json:"void_kills_team"`
				LongestBowShotTeam                                      int      `json:"longest_bow_shot_team"`
				LongestBowShotKitMiningTeamDefault                      int      `json:"longest_bow_shot_kit_mining_team_default"`
				ArrowsShotTeam                                          int      `json:"arrows_shot_team"`
				ArrowsHitTeam                                           int      `json:"arrows_hit_team"`
				ArrowsHitKitMiningTeamDefault                           int      `json:"arrows_hit_kit_mining_team_default"`
				ArrowsShotKitMiningTeamDefault                          int      `json:"arrows_shot_kit_mining_team_default"`
				DeathsTeamInsane                                        int      `json:"deaths_team_insane"`
				LossesTeamInsane                                        int      `json:"losses_team_insane"`
				FastestWin                                              int      `json:"fastest_win"`
				FastestWinTeam                                          int      `json:"fastest_win_team"`
				FastestWinKitMiningTeamDefault                          int      `json:"fastest_win_kit_mining_team_default"`
				WinsKitMiningTeamDefault                                int      `json:"wins_kit_mining_team_default"`
				Killstreak                                              int      `json:"killstreak"`
				WinsTeamInsane                                          int      `json:"wins_team_insane"`
				Wins                                                    int      `json:"wins"`
				WinstreakKitMiningTeamDefault                           int      `json:"winstreak_kit_mining_team_default"`
				KillsTeamInsane                                         int      `json:"kills_team_insane"`
				KillstreakTeam                                          int      `json:"killstreak_team"`
				WinstreakTeam                                           int      `json:"winstreak_team"`
				Winstreak                                               int      `json:"winstreak"`
				KillstreakKitMiningTeamDefault                          int      `json:"killstreak_kit_mining_team_default"`
				WinsTeam                                                int      `json:"wins_team"`
				LossesSoloInsane                                        int      `json:"losses_solo_insane"`
				DeathsSoloInsane                                        int      `json:"deaths_solo_insane"`
				KillsSoloInsane                                         int      `json:"kills_solo_insane"`
				FastestWinKitBasicSoloDefault                           int      `json:"fastest_win_kit_basic_solo_default"`
				FastestWinSolo                                          int      `json:"fastest_win_solo"`
				KillstreakSolo                                          int      `json:"killstreak_solo"`
				WinsSoloNormal                                          int      `json:"wins_solo_normal"`
				KillstreakKitBasicSoloDefault                           int      `json:"killstreak_kit_basic_solo_default"`
				WinstreakSolo                                           int      `json:"winstreak_solo"`
				WinstreakKitBasicSoloDefault                            int      `json:"winstreak_kit_basic_solo_default"`
				WinsKitBasicSoloDefault                                 int      `json:"wins_kit_basic_solo_default"`
				WinsSolo                                                int      `json:"wins_solo"`
				ActiveKitTEAM                                           string   `json:"activeKit_TEAM"`
				LossesKitDefendingTeamArmorer                           int      `json:"losses_kit_defending_team_armorer"`
				TimePlayedKitDefendingTeamArmorer                       int      `json:"time_played_kit_defending_team_armorer"`
				ChestsOpenedKitDefendingTeamArmorer                     int      `json:"chests_opened_kit_defending_team_armorer"`
				DeathsKitDefendingTeamArmorer                           int      `json:"deaths_kit_defending_team_armorer"`
				SurvivedPlayersKitDefendingTeamArmorer                  int      `json:"survived_players_kit_defending_team_armorer"`
				AssistsKitDefendingTeamArmorer                          int      `json:"assists_kit_defending_team_armorer"`
				MeleeKillsKitDefendingTeamArmorer                       int      `json:"melee_kills_kit_defending_team_armorer"`
				KillsKitDefendingTeamArmorer                            int      `json:"kills_kit_defending_team_armorer"`
				MostKillsGameKitDefendingTeamArmorer                    int      `json:"most_kills_game_kit_defending_team_armorer"`
				ArrowsShotKitDefendingTeamArmorer                       int      `json:"arrows_shot_kit_defending_team_armorer"`
				VoidKillsKitDefendingTeamArmorer                        int      `json:"void_kills_kit_defending_team_armorer"`
				LongestBowShotKitDefendingTeamArmorer                   int      `json:"longest_bow_shot_kit_defending_team_armorer"`
				ArrowsHitKitDefendingTeamArmorer                        int      `json:"arrows_hit_kit_defending_team_armorer"`
				WinsSoloInsane                                          int      `json:"wins_solo_insane"`
				RefillChestDestroy                                      int      `json:"refill_chest_destroy"`
				SoulWell                                                int      `json:"soul_well"`
				UsedSoulWell                                            bool     `json:"usedSoulWell"`
				SoloMiningExpertise                                     int      `json:"solo_mining_expertise"`
				SoloResistanceBoost                                     int      `json:"solo_resistance_boost"`
				SoulWellRares                                           int      `json:"soul_well_rares"`
				SoloBlazingArrows                                       int      `json:"solo_blazing_arrows"`
				FastestWinKitDefendingTeamArmorer                       int      `json:"fastest_win_kit_defending_team_armorer"`
				GamesKitDefendingTeamArmorer                            int      `json:"games_kit_defending_team_armorer"`
				GamesTeam                                               int      `json:"games_team"`
				WinsTeamNormal                                          int      `json:"wins_team_normal"`
				WinstreakKitDefendingTeamArmorer                        int      `json:"winstreak_kit_defending_team_armorer"`
				WinsKitDefendingTeamArmorer                             int      `json:"wins_kit_defending_team_armorer"`
				KillstreakKitDefendingTeamArmorer                       int      `json:"killstreak_kit_defending_team_armorer"`
				LossesKitSupportingTeamRookie                           int      `json:"losses_kit_supporting_team_rookie"`
				ChestsOpenedKitSupportingTeamRookie                     int      `json:"chests_opened_kit_supporting_team_rookie"`
				TimePlayedKitSupportingTeamRookie                       int      `json:"time_played_kit_supporting_team_rookie"`
				SurvivedPlayersKitSupportingTeamRookie                  int      `json:"survived_players_kit_supporting_team_rookie"`
				DeathsKitSupportingTeamRookie                           int      `json:"deaths_kit_supporting_team_rookie"`
				SoloKnowledge                                           int      `json:"solo_knowledge"`
				FreeLootChestNpc                                        int64    `json:"freeLootChestNpc"`
				SkywarsChests                                           int      `json:"skywars_chests"`
				ActiveKitSOLO                                           string   `json:"activeKit_SOLO"`
				ActiveKitSOLORandom                                     bool     `json:"activeKit_SOLO_random"`
				SkyWarsOpenedChests                                     int      `json:"SkyWars_openedChests"`
				SkyWarsOpenedCommons                                    int      `json:"SkyWars_openedCommons"`
				SkywarsChestHistory                                     []string `json:"skywars_chest_history"`
				SkyWarsOpenedRares                                      int      `json:"SkyWars_openedRares"`
				ActiveVictorydance                                      string   `json:"active_victorydance"`
				ActiveDeathcry                                          string   `json:"active_deathcry"`
				ActiveSprays                                            string   `json:"active_sprays"`
				ActiveKitMEGADOUBLESRandom                              bool     `json:"activeKit_MEGA_DOUBLES_random"`
				ActiveKitMEGADOUBLES                                    string   `json:"activeKit_MEGA_DOUBLES"`
				ActiveKitTEAMS                                          string   `json:"activeKit_TEAMS"`
				ActiveKitTEAMSRandom                                    bool     `json:"activeKit_TEAMS_random"`
				DeathsKitAttackingTeamDefault                           int      `json:"deaths_kit_attacking_team_default"`
				ChestsOpenedKitAttackingTeamDefault                     int      `json:"chests_opened_kit_attacking_team_default"`
				SurvivedPlayersKitAttackingTeamDefault                  int      `json:"survived_players_kit_attacking_team_default"`
				TimePlayedKitAttackingTeamDefault                       int      `json:"time_played_kit_attacking_team_default"`
				LossesKitAttackingTeamDefault                           int      `json:"losses_kit_attacking_team_default"`
				AssistsKitAttackingTeamDefault                          int      `json:"assists_kit_attacking_team_default"`
				ArrowsShotKitAttackingTeamDefault                       int      `json:"arrows_shot_kit_attacking_team_default"`
				MostKillsGameKitAttackingTeamDefault                    int      `json:"most_kills_game_kit_attacking_team_default"`
				LongestBowKillKitAttackingTeamDefault                   int      `json:"longest_bow_kill_kit_attacking_team_default"`
				LongestBowKillTeam                                      int      `json:"longest_bow_kill_team"`
				LongestBowKill                                          int      `json:"longest_bow_kill"`
				MeleeKillsKitAttackingTeamDefault                       int      `json:"melee_kills_kit_attacking_team_default"`
				KillsKitAttackingTeamDefault                            int      `json:"kills_kit_attacking_team_default"`
				LongestBowKillKitBasicSoloDefault                       int      `json:"longest_bow_kill_kit_basic_solo_default"`
				LongestBowKillSolo                                      int      `json:"longest_bow_kill_solo"`
				BowKills                                                int      `json:"bow_kills"`
				BowKillsSolo                                            int      `json:"bow_kills_solo"`
				BowKillsKitBasicSoloDefault                             int      `json:"bow_kills_kit_basic_solo_default"`
				LevelFormatted                                          string   `json:"levelFormatted"`
				TeamKnowledge                                           int      `json:"team_knowledge"`
				TeamMiningExpertise                                     int      `json:"team_mining_expertise"`
				SkywarsExperience                                       int      `json:"skywars_experience"`
				TeamResistanceBoost                                     int      `json:"team_resistance_boost"`
				TeamBlazingArrows                                       int      `json:"team_blazing_arrows"`
				LossesKitAdvancedSoloArmorer                            int      `json:"losses_kit_advanced_solo_armorer"`
				ChestsOpenedKitAdvancedSoloArmorer                      int      `json:"chests_opened_kit_advanced_solo_armorer"`
				SurvivedPlayersKitAdvancedSoloArmorer                   int      `json:"survived_players_kit_advanced_solo_armorer"`
				TimePlayedKitAdvancedSoloArmorer                        int      `json:"time_played_kit_advanced_solo_armorer"`
				DeathsKitAdvancedSoloArmorer                            int      `json:"deaths_kit_advanced_solo_armorer"`
				LossesKitBasicSoloRookie                                int      `json:"losses_kit_basic_solo_rookie"`
				SurvivedPlayersKitBasicSoloRookie                       int      `json:"survived_players_kit_basic_solo_rookie"`
				TimePlayedKitBasicSoloRookie                            int      `json:"time_played_kit_basic_solo_rookie"`
				DeathsKitBasicSoloRookie                                int      `json:"deaths_kit_basic_solo_rookie"`
				MostKillsGameKitAdvancedSoloArmorer                     int      `json:"most_kills_game_kit_advanced_solo_armorer"`
				KillsWeeklyB                                            int      `json:"kills_weekly_b"`
				KillsKitAdvancedSoloArmorer                             int      `json:"kills_kit_advanced_solo_armorer"`
				VoidKillsKitAdvancedSoloArmorer                         int      `json:"void_kills_kit_advanced_solo_armorer"`
				KillsMonthlyA                                           int      `json:"kills_monthly_a"`
				ArrowsHitKitSupportingTeamRookie                        int      `json:"arrows_hit_kit_supporting_team_rookie"`
				ArrowsShotKitSupportingTeamRookie                       int      `json:"arrows_shot_kit_supporting_team_rookie"`
				KillsKitSupportingTeamRookie                            int      `json:"kills_kit_supporting_team_rookie"`
				LongestBowShotKitSupportingTeamRookie                   int      `json:"longest_bow_shot_kit_supporting_team_rookie"`
				MostKillsGameKitSupportingTeamRookie                    int      `json:"most_kills_game_kit_supporting_team_rookie"`
				VoidKillsKitSupportingTeamRookie                        int      `json:"void_kills_kit_supporting_team_rookie"`
				ChestsOpenedKitBasicSoloRookie                          int      `json:"chests_opened_kit_basic_solo_rookie"`
				KillsKitBasicSoloRookie                                 int      `json:"kills_kit_basic_solo_rookie"`
				MostKillsGameKitBasicSoloRookie                         int      `json:"most_kills_game_kit_basic_solo_rookie"`
				VoidKillsKitBasicSoloRookie                             int      `json:"void_kills_kit_basic_solo_rookie"`
				ArrowsHitKitBasicSoloRookie                             int      `json:"arrows_hit_kit_basic_solo_rookie"`
				ArrowsShotKitBasicSoloRookie                            int      `json:"arrows_shot_kit_basic_solo_rookie"`
				GamesKitBasicSoloRookie                                 int      `json:"games_kit_basic_solo_rookie"`
				LongestBowShotKitBasicSoloRookie                        int      `json:"longest_bow_shot_kit_basic_solo_rookie"`
				LongestBowKillKitBasicSoloRookie                        int      `json:"longest_bow_kill_kit_basic_solo_rookie"`
				MeleeKillsKitBasicSoloRookie                            int      `json:"melee_kills_kit_basic_solo_rookie"`
				AssistsKitBasicSoloRookie                               int      `json:"assists_kit_basic_solo_rookie"`
				PaidSouls                                               int      `json:"paid_souls"`
				TeamSavior                                              int      `json:"team_savior"`
				TeamInstantSmelting                                     int      `json:"team_instant_smelting"`
				ChestsOpenedKitBasicSoloFrog                            int      `json:"chests_opened_kit_basic_solo_frog"`
				DeathsKitBasicSoloFrog                                  int      `json:"deaths_kit_basic_solo_frog"`
				KillsKitBasicSoloFrog                                   int      `json:"kills_kit_basic_solo_frog"`
				LossesKitBasicSoloFrog                                  int      `json:"losses_kit_basic_solo_frog"`
				MostKillsGameKitBasicSoloFrog                           int      `json:"most_kills_game_kit_basic_solo_frog"`
				SurvivedPlayersKitBasicSoloFrog                         int      `json:"survived_players_kit_basic_solo_frog"`
				TimePlayedKitBasicSoloFrog                              int      `json:"time_played_kit_basic_solo_frog"`
				VoidKillsKitBasicSoloFrog                               int      `json:"void_kills_kit_basic_solo_frog"`
				ArrowsHitKitBasicSoloFrog                               int      `json:"arrows_hit_kit_basic_solo_frog"`
				ArrowsShotKitBasicSoloFrog                              int      `json:"arrows_shot_kit_basic_solo_frog"`
				GamesKitBasicSoloFrog                                   int      `json:"games_kit_basic_solo_frog"`
				LongestBowKillKitBasicSoloFrog                          int      `json:"longest_bow_kill_kit_basic_solo_frog"`
				LongestBowShotKitBasicSoloFrog                          int      `json:"longest_bow_shot_kit_basic_solo_frog"`
				MeleeKillsKitBasicSoloFrog                              int      `json:"melee_kills_kit_basic_solo_frog"`
				ArrowsShotKitBasicSoloFisherman                         int      `json:"arrows_shot_kit_basic_solo_fisherman"`
				ChestsOpenedKitBasicSoloFisherman                       int      `json:"chests_opened_kit_basic_solo_fisherman"`
				DeathsKitBasicSoloFisherman                             int      `json:"deaths_kit_basic_solo_fisherman"`
				LossesKitBasicSoloFisherman                             int      `json:"losses_kit_basic_solo_fisherman"`
				SurvivedPlayersKitBasicSoloFisherman                    int      `json:"survived_players_kit_basic_solo_fisherman"`
				TimePlayedKitBasicSoloFisherman                         int      `json:"time_played_kit_basic_solo_fisherman"`
				ArrowsHitKitBasicSoloFisherman                          int      `json:"arrows_hit_kit_basic_solo_fisherman"`
				GamesKitBasicSoloFisherman                              int      `json:"games_kit_basic_solo_fisherman"`
				LongestBowShotKitBasicSoloFisherman                     int      `json:"longest_bow_shot_kit_basic_solo_fisherman"`
				KillsKitBasicSoloFisherman                              int      `json:"kills_kit_basic_solo_fisherman"`
				MostKillsGameKitBasicSoloFisherman                      int      `json:"most_kills_game_kit_basic_solo_fisherman"`
				VoidKillsKitBasicSoloFisherman                          int      `json:"void_kills_kit_basic_solo_fisherman"`
				AssistsKitBasicSoloFrog                                 int      `json:"assists_kit_basic_solo_frog"`
				SoulWellLegendaries                                     int      `json:"soul_well_legendaries"`
				AssistsKitSupportingTeamRookie                          int      `json:"assists_kit_supporting_team_rookie"`
				LongestBowKillKitSupportingTeamRookie                   int      `json:"longest_bow_kill_kit_supporting_team_rookie"`
				MeleeKillsKitSupportingTeamRookie                       int      `json:"melee_kills_kit_supporting_team_rookie"`
				DeathsKitDefendingTeamDisco                             int      `json:"deaths_kit_defending_team_disco"`
				GamesKitDefendingTeamDisco                              int      `json:"games_kit_defending_team_disco"`
				LossesKitDefendingTeamDisco                             int      `json:"losses_kit_defending_team_disco"`
				SurvivedPlayersKitDefendingTeamDisco                    int      `json:"survived_players_kit_defending_team_disco"`
				TimePlayedKitDefendingTeamDisco                         int      `json:"time_played_kit_defending_team_disco"`
				FastestWinKitDefendingTeamDisco                         int      `json:"fastest_win_kit_defending_team_disco"`
				WinsKitDefendingTeamDisco                               int      `json:"wins_kit_defending_team_disco"`
				ChestsOpenedKitDefendingTeamDisco                       int      `json:"chests_opened_kit_defending_team_disco"`
				KillsKitDefendingTeamDisco                              int      `json:"kills_kit_defending_team_disco"`
				MostKillsGameKitDefendingTeamDisco                      int      `json:"most_kills_game_kit_defending_team_disco"`
				VoidKillsKitDefendingTeamDisco                          int      `json:"void_kills_kit_defending_team_disco"`
				FastestWinKitSupportingTeamRookie                       int      `json:"fastest_win_kit_supporting_team_rookie"`
				GamesKitSupportingTeamRookie                            int      `json:"games_kit_supporting_team_rookie"`
				WinsKitSupportingTeamRookie                             int      `json:"wins_kit_supporting_team_rookie"`
				TeamJuggernaut                                          int      `json:"team_juggernaut"`
				ChallengeAttempts                                       int      `json:"challenge_attempts"`
				ChallengeAttempts8                                      int      `json:"challenge_attempts_8"`
				ChallengeAttempts8KitSupportingTeamRookie               int      `json:"challenge_attempts_8_kit_supporting_team_rookie"`
				ChallengeAttempts8Solo                                  int      `json:"challenge_attempts_8_solo"`
				ChallengeAttemptsArcher                                 int      `json:"challenge_attempts_archer"`
				ChallengeAttemptsArcherKitSupportingTeamRookie          int      `json:"challenge_attempts_archer_kit_supporting_team_rookie"`
				ChallengeAttemptsArcherSolo                             int      `json:"challenge_attempts_archer_solo"`
				ChallengeAttemptsHalfHealth                             int      `json:"challenge_attempts_half_health"`
				ChallengeAttemptsHalfHealthKitSupportingTeamRookie      int      `json:"challenge_attempts_half_health_kit_supporting_team_rookie"`
				ChallengeAttemptsHalfHealthSolo                         int      `json:"challenge_attempts_half_health_solo"`
				ChallengeAttemptsKitSupportingTeamRookie                int      `json:"challenge_attempts_kit_supporting_team_rookie"`
				ChallengeAttemptsNoBlock                                int      `json:"challenge_attempts_no_block"`
				ChallengeAttemptsNoBlockKitSupportingTeamRookie         int      `json:"challenge_attempts_no_block_kit_supporting_team_rookie"`
				ChallengeAttemptsNoBlockSolo                            int      `json:"challenge_attempts_no_block_solo"`
				ChallengeAttemptsNoChest                                int      `json:"challenge_attempts_no_chest"`
				ChallengeAttemptsNoChestKitSupportingTeamRookie         int      `json:"challenge_attempts_no_chest_kit_supporting_team_rookie"`
				ChallengeAttemptsNoChestSolo                            int      `json:"challenge_attempts_no_chest_solo"`
				ChallengeAttemptsPaper                                  int      `json:"challenge_attempts_paper"`
				ChallengeAttemptsPaperKitSupportingTeamRookie           int      `json:"challenge_attempts_paper_kit_supporting_team_rookie"`
				ChallengeAttemptsPaperSolo                              int      `json:"challenge_attempts_paper_solo"`
				ChallengeAttemptsRookie                                 int      `json:"challenge_attempts_rookie"`
				ChallengeAttemptsRookieKitSupportingTeamRookie          int      `json:"challenge_attempts_rookie_kit_supporting_team_rookie"`
				ChallengeAttemptsRookieSolo                             int      `json:"challenge_attempts_rookie_solo"`
				ChallengeAttemptsSolo                                   int      `json:"challenge_attempts_solo"`
				ChallengeAttemptsUhc                                    int      `json:"challenge_attempts_uhc"`
				ChallengeAttemptsUhcKitSupportingTeamRookie             int      `json:"challenge_attempts_uhc_kit_supporting_team_rookie"`
				ChallengeAttemptsUhcSolo                                int      `json:"challenge_attempts_uhc_solo"`
				ChallengeAttemptsUltimateWarrior                        int      `json:"challenge_attempts_ultimate_warrior"`
				ChallengeAttemptsUltimateWarriorKitSupportingTeamRookie int      `json:"challenge_attempts_ultimate_warrior_kit_supporting_team_rookie"`
				ChallengeAttemptsUltimateWarriorSolo                    int      `json:"challenge_attempts_ultimate_warrior_solo"`
				ChallengeAttempts7                                      int      `json:"challenge_attempts_7"`
				ChallengeAttempts7KitSupportingTeamRookie               int      `json:"challenge_attempts_7_kit_supporting_team_rookie"`
				ChallengeAttempts7Solo                                  int      `json:"challenge_attempts_7_solo"`
				ChallengeAttempts5                                      int      `json:"challenge_attempts_5"`
				ChallengeAttempts5KitSupportingTeamRookie               int      `json:"challenge_attempts_5_kit_supporting_team_rookie"`
				ChallengeAttempts5Solo                                  int      `json:"challenge_attempts_5_solo"`
				ChallengeAttempts7KitBasicSoloFrog                      int      `json:"challenge_attempts_7_kit_basic_solo_frog"`
				ChallengeAttemptsArcherKitBasicSoloFrog                 int      `json:"challenge_attempts_archer_kit_basic_solo_frog"`
				ChallengeAttemptsHalfHealthKitBasicSoloFrog             int      `json:"challenge_attempts_half_health_kit_basic_solo_frog"`
				ChallengeAttemptsKitBasicSoloFrog                       int      `json:"challenge_attempts_kit_basic_solo_frog"`
				ChallengeAttemptsNoBlockKitBasicSoloFrog                int      `json:"challenge_attempts_no_block_kit_basic_solo_frog"`
				ChallengeAttemptsPaperKitBasicSoloFrog                  int      `json:"challenge_attempts_paper_kit_basic_solo_frog"`
				ChallengeAttemptsRookieKitBasicSoloFrog                 int      `json:"challenge_attempts_rookie_kit_basic_solo_frog"`
				ChallengeAttemptsUhcKitBasicSoloFrog                    int      `json:"challenge_attempts_uhc_kit_basic_solo_frog"`
				ChallengeAttemptsUltimateWarriorKitBasicSoloFrog        int      `json:"challenge_attempts_ultimate_warrior_kit_basic_solo_frog"`
				ChallengeAttempts8KitBasicSoloRookie                    int      `json:"challenge_attempts_8_kit_basic_solo_rookie"`
				ChallengeAttemptsArcherKitBasicSoloRookie               int      `json:"challenge_attempts_archer_kit_basic_solo_rookie"`
				ChallengeAttemptsHalfHealthKitBasicSoloRookie           int      `json:"challenge_attempts_half_health_kit_basic_solo_rookie"`
				ChallengeAttemptsKitBasicSoloRookie                     int      `json:"challenge_attempts_kit_basic_solo_rookie"`
				ChallengeAttemptsNoBlockKitBasicSoloRookie              int      `json:"challenge_attempts_no_block_kit_basic_solo_rookie"`
				ChallengeAttemptsNoChestKitBasicSoloRookie              int      `json:"challenge_attempts_no_chest_kit_basic_solo_rookie"`
				ChallengeAttemptsPaperKitBasicSoloRookie                int      `json:"challenge_attempts_paper_kit_basic_solo_rookie"`
				ChallengeAttemptsRookieKitBasicSoloRookie               int      `json:"challenge_attempts_rookie_kit_basic_solo_rookie"`
				ChallengeAttemptsUhcKitBasicSoloRookie                  int      `json:"challenge_attempts_uhc_kit_basic_solo_rookie"`
				ChallengeAttemptsUltimateWarriorKitBasicSoloRookie      int      `json:"challenge_attempts_ultimate_warrior_kit_basic_solo_rookie"`
				ChallengeAttempts8KitBasicSoloFisherman                 int      `json:"challenge_attempts_8_kit_basic_solo_fisherman"`
				ChallengeAttemptsArcherKitBasicSoloFisherman            int      `json:"challenge_attempts_archer_kit_basic_solo_fisherman"`
				ChallengeAttemptsHalfHealthKitBasicSoloFisherman        int      `json:"challenge_attempts_half_health_kit_basic_solo_fisherman"`
				ChallengeAttemptsKitBasicSoloFisherman                  int      `json:"challenge_attempts_kit_basic_solo_fisherman"`
				ChallengeAttemptsNoBlockKitBasicSoloFisherman           int      `json:"challenge_attempts_no_block_kit_basic_solo_fisherman"`
				ChallengeAttemptsNoChestKitBasicSoloFisherman           int      `json:"challenge_attempts_no_chest_kit_basic_solo_fisherman"`
				ChallengeAttemptsPaperKitBasicSoloFisherman             int      `json:"challenge_attempts_paper_kit_basic_solo_fisherman"`
				ChallengeAttemptsRookieKitBasicSoloFisherman            int      `json:"challenge_attempts_rookie_kit_basic_solo_fisherman"`
				ChallengeAttemptsUhcKitBasicSoloFisherman               int      `json:"challenge_attempts_uhc_kit_basic_solo_fisherman"`
				ChallengeAttemptsUltimateWarriorKitBasicSoloFisherman   int      `json:"challenge_attempts_ultimate_warrior_kit_basic_solo_fisherman"`
				ChallengeAttempts6                                      int      `json:"challenge_attempts_6"`
				ChallengeAttempts6KitBasicSoloFrog                      int      `json:"challenge_attempts_6_kit_basic_solo_frog"`
				ChallengeAttempts6Solo                                  int      `json:"challenge_attempts_6_solo"`
				ChallengeAttemptsNoChestKitBasicSoloFrog                int      `json:"challenge_attempts_no_chest_kit_basic_solo_frog"`
				ChallengeAttempts4                                      int      `json:"challenge_attempts_4"`
				ChallengeAttempts4KitBasicSoloFrog                      int      `json:"challenge_attempts_4_kit_basic_solo_frog"`
				ChallengeAttempts4Solo                                  int      `json:"challenge_attempts_4_solo"`
				ChallengeAttempts5KitBasicSoloRookie                    int      `json:"challenge_attempts_5_kit_basic_solo_rookie"`
				ChallengeAttempts3                                      int      `json:"challenge_attempts_3"`
				ChallengeAttempts3KitBasicSoloFisherman                 int      `json:"challenge_attempts_3_kit_basic_solo_fisherman"`
				ChallengeAttempts3Solo                                  int      `json:"challenge_attempts_3_solo"`
				ChallengeAttempts4KitBasicSoloRookie                    int      `json:"challenge_attempts_4_kit_basic_solo_rookie"`
				ChallengeAttempts2                                      int      `json:"challenge_attempts_2"`
				ChallengeAttempts2KitBasicSoloRookie                    int      `json:"challenge_attempts_2_kit_basic_solo_rookie"`
				ChallengeAttempts2Solo                                  int      `json:"challenge_attempts_2_solo"`
				ChallengeAttempts1                                      int      `json:"challenge_attempts_1"`
				ChallengeAttempts1KitBasicSoloRookie                    int      `json:"challenge_attempts_1_kit_basic_solo_rookie"`
				ChallengeAttempts1Solo                                  int      `json:"challenge_attempts_1_solo"`
				ChallengeWins                                           int      `json:"challenge_wins"`
				ChallengeWins1                                          int      `json:"challenge_wins_1"`
				ChallengeWins1KitBasicSoloRookie                        int      `json:"challenge_wins_1_kit_basic_solo_rookie"`
				ChallengeWins1Solo                                      int      `json:"challenge_wins_1_solo"`
				ChallengeWinsKitBasicSoloRookie                         int      `json:"challenge_wins_kit_basic_solo_rookie"`
				ChallengeWinsSolo                                       int      `json:"challenge_wins_solo"`
				ChallengeWinsUltimateWarrior                            int      `json:"challenge_wins_ultimate_warrior"`
				ChallengeWinsUltimateWarriorKitBasicSoloRookie          int      `json:"challenge_wins_ultimate_warrior_kit_basic_solo_rookie"`
				ChallengeWinsUltimateWarriorSolo                        int      `json:"challenge_wins_ultimate_warrior_solo"`
				FastestWinKitBasicSoloRookie                            int      `json:"fastest_win_kit_basic_solo_rookie"`
				KillstreakKitBasicSoloRookie                            int      `json:"killstreak_kit_basic_solo_rookie"`
				WinsKitBasicSoloRookie                                  int      `json:"wins_kit_basic_solo_rookie"`
				ChallengeAttempts5KitBasicSoloFrog                      int      `json:"challenge_attempts_5_kit_basic_solo_frog"`
				ChallengeAttempts8KitBasicSoloFrog                      int      `json:"challenge_attempts_8_kit_basic_solo_frog"`
				ChallengeWins8                                          int      `json:"challenge_wins_8"`
				ChallengeWins8KitBasicSoloFrog                          int      `json:"challenge_wins_8_kit_basic_solo_frog"`
				ChallengeWins8Solo                                      int      `json:"challenge_wins_8_solo"`
				ChallengeWinsArcher                                     int      `json:"challenge_wins_archer"`
				ChallengeWinsArcherKitBasicSoloFrog                     int      `json:"challenge_wins_archer_kit_basic_solo_frog"`
				ChallengeWinsArcherSolo                                 int      `json:"challenge_wins_archer_solo"`
				ChallengeWinsHalfHealth                                 int      `json:"challenge_wins_half_health"`
				ChallengeWinsHalfHealthKitBasicSoloFrog                 int      `json:"challenge_wins_half_health_kit_basic_solo_frog"`
				ChallengeWinsHalfHealthSolo                             int      `json:"challenge_wins_half_health_solo"`
				ChallengeWinsKitBasicSoloFrog                           int      `json:"challenge_wins_kit_basic_solo_frog"`
				ChallengeWinsNoBlock                                    int      `json:"challenge_wins_no_block"`
				ChallengeWinsNoBlockKitBasicSoloFrog                    int      `json:"challenge_wins_no_block_kit_basic_solo_frog"`
				ChallengeWinsNoBlockSolo                                int      `json:"challenge_wins_no_block_solo"`
				ChallengeWinsNoChest                                    int      `json:"challenge_wins_no_chest"`
				ChallengeWinsNoChestKitBasicSoloFrog                    int      `json:"challenge_wins_no_chest_kit_basic_solo_frog"`
				ChallengeWinsNoChestSolo                                int      `json:"challenge_wins_no_chest_solo"`
				ChallengeWinsPaper                                      int      `json:"challenge_wins_paper"`
				ChallengeWinsPaperKitBasicSoloFrog                      int      `json:"challenge_wins_paper_kit_basic_solo_frog"`
				ChallengeWinsPaperSolo                                  int      `json:"challenge_wins_paper_solo"`
				ChallengeWinsRookie                                     int      `json:"challenge_wins_rookie"`
				ChallengeWinsRookieKitBasicSoloFrog                     int      `json:"challenge_wins_rookie_kit_basic_solo_frog"`
				ChallengeWinsRookieSolo                                 int      `json:"challenge_wins_rookie_solo"`
				ChallengeWinsUhc                                        int      `json:"challenge_wins_uhc"`
				ChallengeWinsUhcKitBasicSoloFrog                        int      `json:"challenge_wins_uhc_kit_basic_solo_frog"`
				ChallengeWinsUhcSolo                                    int      `json:"challenge_wins_uhc_solo"`
				ChallengeWinsUltimateWarriorKitBasicSoloFrog            int      `json:"challenge_wins_ultimate_warrior_kit_basic_solo_frog"`
				FastestWinKitBasicSoloFrog                              int      `json:"fastest_win_kit_basic_solo_frog"`
				WinsKitBasicSoloFrog                                    int      `json:"wins_kit_basic_solo_frog"`
				SkywarsHalloweenBoxes                                   int      `json:"skywars_halloween_boxes"`
				FreeEventKeySkywarsHalloweenBoxes20192                  bool     `json:"free_event_key_skywars_halloween_boxes_2019_2"`
				ChestsOpenedKitMegaMegaDefault                          int      `json:"chests_opened_kit_mega_mega_default"`
				ChestsOpenedMegaDoubles                                 int      `json:"chests_opened_mega_doubles"`
				DeathsKitMegaMegaDefault                                int      `json:"deaths_kit_mega_mega_default"`
				DeathsMegaDoubles                                       int      `json:"deaths_mega_doubles"`
				DeathsMegaDoublesNormal                                 int      `json:"deaths_mega_doubles_normal"`
				KillsKitMegaMegaDefault                                 int      `json:"kills_kit_mega_mega_default"`
				KillsMegaDoubles                                        int      `json:"kills_mega_doubles"`
				KillsMegaDoublesNormal                                  int      `json:"kills_mega_doubles_normal"`
				LongestBowKillKitMegaMegaDefault                        int      `json:"longest_bow_kill_kit_mega_mega_default"`
				LongestBowKillMegaDoubles                               int      `json:"longest_bow_kill_mega_doubles"`
				LossesKitMegaMegaDefault                                int      `json:"losses_kit_mega_mega_default"`
				LossesMegaDoubles                                       int      `json:"losses_mega_doubles"`
				LossesMegaDoublesNormal                                 int      `json:"losses_mega_doubles_normal"`
				MeleeKillsKitMegaMegaDefault                            int      `json:"melee_kills_kit_mega_mega_default"`
				MeleeKillsMegaDoubles                                   int      `json:"melee_kills_mega_doubles"`
				MostKillsGameKitMegaMegaDefault                         int      `json:"most_kills_game_kit_mega_mega_default"`
				MostKillsGameMegaDoubles                                int      `json:"most_kills_game_mega_doubles"`
				SurvivedPlayersKitMegaMegaDefault                       int      `json:"survived_players_kit_mega_mega_default"`
				SurvivedPlayersMegaDoubles                              int      `json:"survived_players_mega_doubles"`
				TimePlayedKitMegaMegaDefault                            int      `json:"time_played_kit_mega_mega_default"`
				TimePlayedMegaDoubles                                   int      `json:"time_played_mega_doubles"`
				FastestWinKitMegaMegaDefault                            int      `json:"fastest_win_kit_mega_mega_default"`
				FastestWinMegaDoubles                                   int      `json:"fastest_win_mega_doubles"`
				WinsKitMegaMegaDefault                                  int      `json:"wins_kit_mega_mega_default"`
				WinsMegaDoubles                                         int      `json:"wins_mega_doubles"`
				WinsMegaDoublesNormal                                   int      `json:"wins_mega_doubles_normal"`
				ArrowsShotKitMegaMegaDefault                            int      `json:"arrows_shot_kit_mega_mega_default"`
				ArrowsShotMegaDoubles                                   int      `json:"arrows_shot_mega_doubles"`
				GamesKitMegaMegaDefault                                 int      `json:"games_kit_mega_mega_default"`
				GamesMegaDoubles                                        int      `json:"games_mega_doubles"`
				VoidKillsKitMegaMegaDefault                             int      `json:"void_kills_kit_mega_mega_default"`
				VoidKillsMegaDoubles                                    int      `json:"void_kills_mega_doubles"`
				RushExplained                                           int      `json:"rush_explained"`
				RushExplainedLast                                       int64    `json:"rush_explained_last"`
				ChestsOpenedLab                                         int      `json:"chests_opened_lab"`
				ChestsOpenedLabKitSupportingTeamRookie                  int      `json:"chests_opened_lab_kit_supporting_team_rookie"`
				ChestsOpenedLabSolo                                     int      `json:"chests_opened_lab_solo"`
				CoinsGainedLab                                          int      `json:"coins_gained_lab"`
				DeathsLab                                               int      `json:"deaths_lab"`
				DeathsLabKitSupportingTeamRookie                        int      `json:"deaths_lab_kit_supporting_team_rookie"`
				DeathsLabSolo                                           int      `json:"deaths_lab_solo"`
				EnderpearlsThrownLab                                    int      `json:"enderpearls_thrown_lab"`
				KillsLab                                                int      `json:"kills_lab"`
				KillsLabKitSupportingTeamRookie                         int      `json:"kills_lab_kit_supporting_team_rookie"`
				KillsLabSolo                                            int      `json:"kills_lab_solo"`
				LossesLab                                               int      `json:"losses_lab"`
				LossesLabKitSupportingTeamRookie                        int      `json:"losses_lab_kit_supporting_team_rookie"`
				LossesLabSolo                                           int      `json:"losses_lab_solo"`
				MeleeKillsLab                                           int      `json:"melee_kills_lab"`
				MeleeKillsLabKitSupportingTeamRookie                    int      `json:"melee_kills_lab_kit_supporting_team_rookie"`
				MeleeKillsLabSolo                                       int      `json:"melee_kills_lab_solo"`
				QuitsLab                                                int      `json:"quits_lab"`
				SoulsGatheredLab                                        int      `json:"souls_gathered_lab"`
				SurvivedPlayersLab                                      int      `json:"survived_players_lab"`
				SurvivedPlayersLabKitSupportingTeamRookie               int      `json:"survived_players_lab_kit_supporting_team_rookie"`
				SurvivedPlayersLabSolo                                  int      `json:"survived_players_lab_solo"`
				TimePlayedLab                                           int      `json:"time_played_lab"`
				TimePlayedLabKitSupportingTeamRookie                    int      `json:"time_played_lab_kit_supporting_team_rookie"`
				TimePlayedLabSolo                                       int      `json:"time_played_lab_solo"`
				VoidKillsLab                                            int      `json:"void_kills_lab"`
				VoidKillsLabKitSupportingTeamRookie                     int      `json:"void_kills_lab_kit_supporting_team_rookie"`
				VoidKillsLabSolo                                        int      `json:"void_kills_lab_solo"`
				WinStreakLab                                            int      `json:"win_streak_lab"`
				BlocksBrokenLab                                         int      `json:"blocks_broken_lab"`
				GamesLab                                                int      `json:"games_lab"`
				GamesLabKitSupportingTeamRookie                         int      `json:"games_lab_kit_supporting_team_rookie"`
				GamesLabSolo                                            int      `json:"games_lab_solo"`
				SlimeExplained                                          int      `json:"slime_explained"`
				SlimeExplainedLast                                      int64    `json:"slime_explained_last"`
				BlocksPlacedLab                                         int      `json:"blocks_placed_lab"`
				ChestsOpenedLabTeam                                     int      `json:"chests_opened_lab_team"`
				DeathsLabTeam                                           int      `json:"deaths_lab_team"`
				LossesLabTeam                                           int      `json:"losses_lab_team"`
				SurvivedPlayersLabTeam                                  int      `json:"survived_players_lab_team"`
				TimePlayedLabTeam                                       int      `json:"time_played_lab_team"`
				FastestWinLab                                           int      `json:"fastest_win_lab"`
				FastestWinLabKitSupportingTeamRookie                    int      `json:"fastest_win_lab_kit_supporting_team_rookie"`
				FastestWinLabSolo                                       int      `json:"fastest_win_lab_solo"`
				KillstreakLab                                           int      `json:"killstreak_lab"`
				KillstreakLabKitSupportingTeamRookie                    int      `json:"killstreak_lab_kit_supporting_team_rookie"`
				KillstreakLabSolo                                       int      `json:"killstreak_lab_solo"`
				LabWinRushLab                                           int      `json:"lab_win_rush_lab"`
				LabWinRushLabKitSupportingTeamRookie                    int      `json:"lab_win_rush_lab_kit_supporting_team_rookie"`
				LabWinRushLabSolo                                       int      `json:"lab_win_rush_lab_solo"`
				MostKillsGameLab                                        int      `json:"most_kills_game_lab"`
				MostKillsGameLabKitSupportingTeamRookie                 int      `json:"most_kills_game_lab_kit_supporting_team_rookie"`
				MostKillsGameLabSolo                                    int      `json:"most_kills_game_lab_solo"`
				WinsLab                                                 int      `json:"wins_lab"`
				WinsLabKitSupportingTeamRookie                          int      `json:"wins_lab_kit_supporting_team_rookie"`
				WinsLabSolo                                             int      `json:"wins_lab_solo"`
				RefillChestDestroyLab                                   int      `json:"refill_chest_destroy_lab"`
				MegaNourishment                                         int      `json:"mega_nourishment"`
				ChestsOpenedKitDefendingTeamBatguy                      int      `json:"chests_opened_kit_defending_team_batguy"`
				DeathsKitDefendingTeamBatguy                            int      `json:"deaths_kit_defending_team_batguy"`
				FastestWinKitDefendingTeamBatguy                        int      `json:"fastest_win_kit_defending_team_batguy"`
				GamesKitDefendingTeamBatguy                             int      `json:"games_kit_defending_team_batguy"`
				SurvivedPlayersKitDefendingTeamBatguy                   int      `json:"survived_players_kit_defending_team_batguy"`
				TimePlayedKitDefendingTeamBatguy                        int      `json:"time_played_kit_defending_team_batguy"`
				WinsKitDefendingTeamBatguy                              int      `json:"wins_kit_defending_team_batguy"`
				LossesKitDefendingTeamBatguy                            int      `json:"losses_kit_defending_team_batguy"`
				ArrowsShotKitDefendingTeamBatguy                        int      `json:"arrows_shot_kit_defending_team_batguy"`
				KillsKitDefendingTeamBatguy                             int      `json:"kills_kit_defending_team_batguy"`
				MostKillsGameKitDefendingTeamBatguy                     int      `json:"most_kills_game_kit_defending_team_batguy"`
				VoidKillsKitDefendingTeamBatguy                         int      `json:"void_kills_kit_defending_team_batguy"`
				LongestBowKillKitDefendingTeamBatguy                    int      `json:"longest_bow_kill_kit_defending_team_batguy"`
				MeleeKillsKitDefendingTeamBatguy                        int      `json:"melee_kills_kit_defending_team_batguy"`
				Heads                                                   int      `json:"heads"`
				HeadsEww                                                int      `json:"heads_eww"`
				HeadsEwwKitBasicSoloFrog                                int      `json:"heads_eww_kit_basic_solo_frog"`
				HeadsEwwTeam                                            int      `json:"heads_eww_team"`
				HeadsKitBasicSoloFrog                                   int      `json:"heads_kit_basic_solo_frog"`
				HeadsMeh                                                int      `json:"heads_meh"`
				HeadsMehKitBasicSoloFrog                                int      `json:"heads_meh_kit_basic_solo_frog"`
				HeadsMehTeam                                            int      `json:"heads_meh_team"`
				HeadsSucculent                                          int      `json:"heads_succulent"`
				HeadsSucculentKitBasicSoloFrog                          int      `json:"heads_succulent_kit_basic_solo_frog"`
				HeadsSucculentTeam                                      int      `json:"heads_succulent_team"`
				HeadsTeam                                               int      `json:"heads_team"`
				HeadsYucky                                              int      `json:"heads_yucky"`
				HeadsYuckyKitBasicSoloFrog                              int      `json:"heads_yucky_kit_basic_solo_frog"`
				HeadsYuckyTeam                                          int      `json:"heads_yucky_team"`
				HeadCollection                                          struct {
					Recent []struct {
						UUID      string `json:"uuid"`
						Timestamp int64  `json:"timestamp"`
						Mode      string `json:"mode"`
						Sacrifice string `json:"sacrifice"`
					} `json:"recent"`
					Prestigious []struct {
						UUID      string `json:"uuid"`
						Timestamp int64  `json:"timestamp"`
						Mode      string `json:"mode"`
						Sacrifice string `json:"sacrifice"`
					} `json:"prestigious"`
				} `json:"head_collection"`
				CosmeticTokens                           int    `json:"cosmetic_tokens"`
				ActiveKillmessages                       string `json:"active_killmessages"`
				SkyWarsOpenedLegendaries                 int    `json:"SkyWars_openedLegendaries"`
				ActiveCage                               string `json:"active_cage"`
				ActiveKilleffect                         string `json:"active_killeffect"`
				SelectedPrestigeIcon                     string `json:"selected_prestige_icon"`
				HeadsEwwKitSupportingTeamRookie          int    `json:"heads_eww_kit_supporting_team_rookie"`
				HeadsKitSupportingTeamRookie             int    `json:"heads_kit_supporting_team_rookie"`
				HeadsTasty                               int    `json:"heads_tasty"`
				HeadsTastyKitSupportingTeamRookie        int    `json:"heads_tasty_kit_supporting_team_rookie"`
				HeadsTastyTeam                           int    `json:"heads_tasty_team"`
				TntMadnessExplained                      int    `json:"tnt_madness_explained"`
				TntMadnessExplainedLast                  int64  `json:"tnt_madness_explained_last"`
				LuckyExplained                           int    `json:"lucky_explained"`
				LuckyExplainedLast                       int64  `json:"lucky_explained_last"`
				ArrowsHitLab                             int    `json:"arrows_hit_lab"`
				ArrowsHitLabKitSupportingTeamRookie      int    `json:"arrows_hit_lab_kit_supporting_team_rookie"`
				ArrowsHitLabTeam                         int    `json:"arrows_hit_lab_team"`
				ArrowsShotLab                            int    `json:"arrows_shot_lab"`
				ArrowsShotLabKitSupportingTeamRookie     int    `json:"arrows_shot_lab_kit_supporting_team_rookie"`
				ArrowsShotLabTeam                        int    `json:"arrows_shot_lab_team"`
				KillsLabTeam                             int    `json:"kills_lab_team"`
				VoidKillsLabTeam                         int    `json:"void_kills_lab_team"`
				GamesLabTeam                             int    `json:"games_lab_team"`
				MeleeKillsLabTeam                        int    `json:"melee_kills_lab_team"`
				LongestBowShotLab                        int    `json:"longest_bow_shot_lab"`
				LongestBowShotLabKitSupportingTeamRookie int    `json:"longest_bow_shot_lab_kit_supporting_team_rookie"`
				LongestBowShotLabTeam                    int    `json:"longest_bow_shot_lab_team"`
				AssistsLab                               int    `json:"assists_lab"`
				AssistsLabKitSupportingTeamRookie        int    `json:"assists_lab_kit_supporting_team_rookie"`
				AssistsLabTeam                           int    `json:"assists_lab_team"`
				LongestBowKillLab                        int    `json:"longest_bow_kill_lab"`
				LongestBowKillLabKitSupportingTeamRookie int    `json:"longest_bow_kill_lab_kit_supporting_team_rookie"`
				LongestBowKillLabTeam                    int    `json:"longest_bow_kill_lab_team"`
				LuckyBlockResourcePackEnabled            bool   `json:"luckyBlockResourcePackEnabled"`
				BowKillsLab                              int    `json:"bow_kills_lab"`
				BowKillsLabKitSupportingTeamRookie       int    `json:"bow_kills_lab_kit_supporting_team_rookie"`
				BowKillsLabTeam                          int    `json:"bow_kills_lab_team"`
				EggThrownLab                             int    `json:"egg_thrown_lab"`
				KitSupportingTeamRookieInventory         struct {
					GLASS0          string `json:"GLASS:0"`
					IRONHELMET0     string `json:"IRON_HELMET:0"`
					IRONCHESTPLATE0 string `json:"IRON_CHESTPLATE:0"`
					STONESWORD0     string `json:"STONE_SWORD:0"`
					GOLDENAPPLE0    string `json:"GOLDEN_APPLE:0"`
				} `json:"kit_supporting_team_rookie_inventory"`
				ArrowsHitLabSolo                      int  `json:"arrows_hit_lab_solo"`
				ArrowsShotLabSolo                     int  `json:"arrows_shot_lab_solo"`
				SkywarsChristmasBoxes                 int  `json:"skywars_christmas_boxes"`
				SkyWarsOpenedEpics                    int  `json:"SkyWars_openedEpics"`
				BowKillsKitSupportingTeamRookie       int  `json:"bow_kills_kit_supporting_team_rookie"`
				BowKillsTeam                          int  `json:"bow_kills_team"`
				ItemsEnchanted                        int  `json:"items_enchanted"`
				FreeEventKeySkywarsChristmasBoxes2020 bool `json:"free_event_key_skywars_christmas_boxes_2020"`
				TeamNourishment                       int  `json:"team_nourishment"`
				KitAttackingTeamEnergixInventory      struct {
					POTION9 string `json:"POTION:9"`
				} `json:"kit_attacking_team_energix_inventory"`
				ChestsOpenedKitAttackingTeamEnergix    int    `json:"chests_opened_kit_attacking_team_energix"`
				DeathsKitAttackingTeamEnergix          int    `json:"deaths_kit_attacking_team_energix"`
				LossesKitAttackingTeamEnergix          int    `json:"losses_kit_attacking_team_energix"`
				SurvivedPlayersKitAttackingTeamEnergix int    `json:"survived_players_kit_attacking_team_energix"`
				TimePlayedKitAttackingTeamEnergix      int    `json:"time_played_kit_attacking_team_energix"`
				ArrowsShotKitAttackingTeamEnergix      int    `json:"arrows_shot_kit_attacking_team_energix"`
				KillsKitAttackingTeamEnergix           int    `json:"kills_kit_attacking_team_energix"`
				LongestBowKillKitAttackingTeamEnergix  int    `json:"longest_bow_kill_kit_attacking_team_energix"`
				MeleeKillsKitAttackingTeamEnergix      int    `json:"melee_kills_kit_attacking_team_energix"`
				MostKillsGameKitAttackingTeamEnergix   int    `json:"most_kills_game_kit_attacking_team_energix"`
				InGamePresentsCap20201                 int    `json:"inGamePresentsCap_2020_1"`
				KillstreakKitSupportingTeamRookie      int    `json:"killstreak_kit_supporting_team_rookie"`
				TeamFat                                int    `json:"team_fat"`
				TeamLuckyCharm                         int    `json:"team_lucky_charm"`
				TeamBridger                            int    `json:"team_bridger"`
				InGamePresentsCap20209                 int    `json:"inGamePresentsCap_2020_9"`
				AssistsLabSolo                         int    `json:"assists_lab_solo"`
				LongestBowKillKitDefendingTeamArmorer  int    `json:"longest_bow_kill_kit_defending_team_armorer"`
				HeadsHeavenly                          int    `json:"heads_heavenly"`
				HeadsHeavenlyKitDefendingTeamArmorer   int    `json:"heads_heavenly_kit_defending_team_armorer"`
				HeadsHeavenlySolo                      int    `json:"heads_heavenly_solo"`
				HeadsKitDefendingTeamArmorer           int    `json:"heads_kit_defending_team_armorer"`
				HeadsSolo                              int    `json:"heads_solo"`
				SoloBridger                            int    `json:"solo_bridger"`
				SoloEnderMastery                       int    `json:"solo_ender_mastery"`
				ExtraWheels                            int    `json:"extra_wheels"`
				SoloFat                                int    `json:"solo_fat"`
				SoloLuckyCharm                         int    `json:"solo_lucky_charm"`
				MegaEnderMastery                       int    `json:"mega_ender_mastery"`
				MegaInstantSmelting                    int    `json:"mega_instant_smelting"`
				TeamRobbery                            int    `json:"team_robbery"`
				HeadsTastyKitDefendingTeamArmorer      int    `json:"heads_tasty_kit_defending_team_armorer"`
				HeadsTastySolo                         int    `json:"heads_tasty_solo"`
				ActiveProjectiletrail                  string `json:"active_projectiletrail"`
				TeamBulldozer                          int    `json:"team_bulldozer"`
				MegaRusher                             int    `json:"mega_rusher"`
				HeadsDecent                            int    `json:"heads_decent"`
				HeadsDecentKitDefendingTeamArmorer     int    `json:"heads_decent_kit_defending_team_armorer"`
				HeadsDecentTeam                        int    `json:"heads_decent_team"`
				HeadsEwwKitDefendingTeamArmorer        int    `json:"heads_eww_kit_defending_team_armorer"`
				HeadsYuckyKitDefendingTeamArmorer      int    `json:"heads_yucky_kit_defending_team_armorer"`
				BowKillsKitDefendingTeamArmorer        int    `json:"bow_kills_kit_defending_team_armorer"`
				KitDefendingTeamFrogInventory          struct {
					POTION2            string `json:"POTION:2"`
					LEATHERCHESTPLATE0 string `json:"LEATHER_CHESTPLATE:0"`
					LEATHERLEGGINGS0   string `json:"LEATHER_LEGGINGS:0"`
					SKULLITEM3         string `json:"SKULL_ITEM:3"`
					LEATHERBOOTS0      string `json:"LEATHER_BOOTS:0"`
				} `json:"kit_defending_team_frog_inventory"`
				ChestsOpenedKitDefendingTeamFrog                     int  `json:"chests_opened_kit_defending_team_frog"`
				DeathsKitDefendingTeamFrog                           int  `json:"deaths_kit_defending_team_frog"`
				LossesKitDefendingTeamFrog                           int  `json:"losses_kit_defending_team_frog"`
				SurvivedPlayersKitDefendingTeamFrog                  int  `json:"survived_players_kit_defending_team_frog"`
				TimePlayedKitDefendingTeamFrog                       int  `json:"time_played_kit_defending_team_frog"`
				KillsKitDefendingTeamFrog                            int  `json:"kills_kit_defending_team_frog"`
				MostKillsGameKitDefendingTeamFrog                    int  `json:"most_kills_game_kit_defending_team_frog"`
				VoidKillsKitDefendingTeamFrog                        int  `json:"void_kills_kit_defending_team_frog"`
				LongestBowKillKitDefendingTeamFrog                   int  `json:"longest_bow_kill_kit_defending_team_frog"`
				MeleeKillsKitDefendingTeamFrog                       int  `json:"melee_kills_kit_defending_team_frog"`
				AssistsKitDefendingTeamFrog                          int  `json:"assists_kit_defending_team_frog"`
				HeadsKitDefendingTeamFrog                            int  `json:"heads_kit_defending_team_frog"`
				HeadsTastyKitDefendingTeamFrog                       int  `json:"heads_tasty_kit_defending_team_frog"`
				TeamEnderMastery                                     int  `json:"team_ender_mastery"`
				HeadsYuckySolo                                       int  `json:"heads_yucky_solo"`
				GamesKitDefendingTeamFrog                            int  `json:"games_kit_defending_team_frog"`
				HeadsDecentKitDefendingTeamFrog                      int  `json:"heads_decent_kit_defending_team_frog"`
				HeadsDecentSolo                                      int  `json:"heads_decent_solo"`
				HeadsMehKitDefendingTeamFrog                         int  `json:"heads_meh_kit_defending_team_frog"`
				HeadsMehSolo                                         int  `json:"heads_meh_solo"`
				HeadsSalty                                           int  `json:"heads_salty"`
				HeadsSaltyKitDefendingTeamFrog                       int  `json:"heads_salty_kit_defending_team_frog"`
				HeadsSaltySolo                                       int  `json:"heads_salty_solo"`
				HeadsYuckyKitDefendingTeamFrog                       int  `json:"heads_yucky_kit_defending_team_frog"`
				ArrowsShotKitDefendingTeamFrog                       int  `json:"arrows_shot_kit_defending_team_frog"`
				ArrowsHitKitDefendingTeamFrog                        int  `json:"arrows_hit_kit_defending_team_frog"`
				LongestBowShotKitDefendingTeamFrog                   int  `json:"longest_bow_shot_kit_defending_team_frog"`
				TeamEnvironmentalExpert                              int  `json:"team_environmental_expert"`
				HeadsEwwKitDefendingTeamFrog                         int  `json:"heads_eww_kit_defending_team_frog"`
				HeadsEwwSolo                                         int  `json:"heads_eww_solo"`
				FastestWinKitDefendingTeamFrog                       int  `json:"fastest_win_kit_defending_team_frog"`
				KillstreakKitDefendingTeamFrog                       int  `json:"killstreak_kit_defending_team_frog"`
				WinsKitDefendingTeamFrog                             int  `json:"wins_kit_defending_team_frog"`
				ChestsOpenedLabKitDefendingTeamFrog                  int  `json:"chests_opened_lab_kit_defending_team_frog"`
				DeathsLabKitDefendingTeamFrog                        int  `json:"deaths_lab_kit_defending_team_frog"`
				LossesLabKitDefendingTeamFrog                        int  `json:"losses_lab_kit_defending_team_frog"`
				SurvivedPlayersLabKitDefendingTeamFrog               int  `json:"survived_players_lab_kit_defending_team_frog"`
				TimePlayedLabKitDefendingTeamFrog                    int  `json:"time_played_lab_kit_defending_team_frog"`
				GamesLabKitDefendingTeamFrog                         int  `json:"games_lab_kit_defending_team_frog"`
				KillsLabKitDefendingTeamFrog                         int  `json:"kills_lab_kit_defending_team_frog"`
				KillstreakLabKitDefendingTeamFrog                    int  `json:"killstreak_lab_kit_defending_team_frog"`
				LabWinTntMadnessLab                                  int  `json:"lab_win_tnt_madness_lab"`
				LabWinTntMadnessLabKitDefendingTeamFrog              int  `json:"lab_win_tnt_madness_lab_kit_defending_team_frog"`
				LabWinTntMadnessLabSolo                              int  `json:"lab_win_tnt_madness_lab_solo"`
				VoidKillsLabKitDefendingTeamFrog                     int  `json:"void_kills_lab_kit_defending_team_frog"`
				WinsLabKitDefendingTeamFrog                          int  `json:"wins_lab_kit_defending_team_frog"`
				ArrowsShotLabKitDefendingTeamFrog                    int  `json:"arrows_shot_lab_kit_defending_team_frog"`
				MobKillsLab                                          int  `json:"mob_kills_lab"`
				MobKillsLabKitDefendingTeamFrog                      int  `json:"mob_kills_lab_kit_defending_team_frog"`
				MobKillsLabSolo                                      int  `json:"mob_kills_lab_solo"`
				AssistsLabKitDefendingTeamFrog                       int  `json:"assists_lab_kit_defending_team_frog"`
				MeleeKillsLabKitDefendingTeamFrog                    int  `json:"melee_kills_lab_kit_defending_team_frog"`
				HeadsSucculentKitDefendingTeamFrog                   int  `json:"heads_succulent_kit_defending_team_frog"`
				HeadsSucculentSolo                                   int  `json:"heads_succulent_solo"`
				XezbethLuck                                          int  `json:"xezbeth_luck"`
				ChestsOpenedKitBasicSoloArmorsmith                   int  `json:"chests_opened_kit_basic_solo_armorsmith"`
				DeathsKitBasicSoloArmorsmith                         int  `json:"deaths_kit_basic_solo_armorsmith"`
				LossesKitBasicSoloArmorsmith                         int  `json:"losses_kit_basic_solo_armorsmith"`
				SurvivedPlayersKitBasicSoloArmorsmith                int  `json:"survived_players_kit_basic_solo_armorsmith"`
				TimePlayedKitBasicSoloArmorsmith                     int  `json:"time_played_kit_basic_solo_armorsmith"`
				KillsKitBasicSoloArmorsmith                          int  `json:"kills_kit_basic_solo_armorsmith"`
				LongestBowKillKitBasicSoloArmorsmith                 int  `json:"longest_bow_kill_kit_basic_solo_armorsmith"`
				MeleeKillsKitBasicSoloArmorsmith                     int  `json:"melee_kills_kit_basic_solo_armorsmith"`
				MostKillsGameKitBasicSoloArmorsmith                  int  `json:"most_kills_game_kit_basic_solo_armorsmith"`
				BlocksPlacedTourney                                  int  `json:"blocks_placed_tourney"`
				ChestsOpenedTourney                                  int  `json:"chests_opened_tourney"`
				ChestsOpenedTourneyKitMiningTeamDefault              int  `json:"chests_opened_tourney_kit_mining_team_default"`
				ChestsOpenedTourneyTeamsTourney                      int  `json:"chests_opened_tourney_teams_tourney"`
				CoinsGainedTourney                                   int  `json:"coins_gained_tourney"`
				DeathsTourney                                        int  `json:"deaths_tourney"`
				DeathsTourneyKitMiningTeamDefault                    int  `json:"deaths_tourney_kit_mining_team_default"`
				DeathsTourneyTeamsTourney                            int  `json:"deaths_tourney_teams_tourney"`
				LossesTourney                                        int  `json:"losses_tourney"`
				LossesTourneyKitMiningTeamDefault                    int  `json:"losses_tourney_kit_mining_team_default"`
				LossesTourneyTeamsTourney                            int  `json:"losses_tourney_teams_tourney"`
				QuitsTourney                                         int  `json:"quits_tourney"`
				SurvivedPlayersTourney                               int  `json:"survived_players_tourney"`
				SurvivedPlayersTourneyKitMiningTeamDefault           int  `json:"survived_players_tourney_kit_mining_team_default"`
				SurvivedPlayersTourneyTeamsTourney                   int  `json:"survived_players_tourney_teams_tourney"`
				TimePlayedTourney                                    int  `json:"time_played_tourney"`
				TimePlayedTourneyKitMiningTeamDefault                int  `json:"time_played_tourney_kit_mining_team_default"`
				TimePlayedTourneyTeamsTourney                        int  `json:"time_played_tourney_teams_tourney"`
				TourneySwInsaneDoubles0BlocksPlaced                  int  `json:"tourney_sw_insane_doubles_0_blocks_placed"`
				TourneySwInsaneDoubles0ChestsOpened                  int  `json:"tourney_sw_insane_doubles_0_chests_opened"`
				TourneySwInsaneDoubles0Coins                         int  `json:"tourney_sw_insane_doubles_0_coins"`
				TourneySwInsaneDoubles0CoinsGained                   int  `json:"tourney_sw_insane_doubles_0_coins_gained"`
				TourneySwInsaneDoubles0Deaths                        int  `json:"tourney_sw_insane_doubles_0_deaths"`
				TourneySwInsaneDoubles0Losses                        int  `json:"tourney_sw_insane_doubles_0_losses"`
				TourneySwInsaneDoubles0Quits                         int  `json:"tourney_sw_insane_doubles_0_quits"`
				TourneySwInsaneDoubles0SurvivedPlayers               int  `json:"tourney_sw_insane_doubles_0_survived_players"`
				TourneySwInsaneDoubles0TimePlayed                    int  `json:"tourney_sw_insane_doubles_0_time_played"`
				TourneySwInsaneDoubles0WinStreak                     int  `json:"tourney_sw_insane_doubles_0_win_streak"`
				WinStreakTourney                                     int  `json:"win_streak_tourney"`
				ChestsOpenedTourneyKitSupportingTeamArmorsmith       int  `json:"chests_opened_tourney_kit_supporting_team_armorsmith"`
				DeathsTourneyKitSupportingTeamArmorsmith             int  `json:"deaths_tourney_kit_supporting_team_armorsmith"`
				KillsTourney                                         int  `json:"kills_tourney"`
				KillsTourneyKitSupportingTeamArmorsmith              int  `json:"kills_tourney_kit_supporting_team_armorsmith"`
				KillsTourneyTeamsTourney                             int  `json:"kills_tourney_teams_tourney"`
				LossesTourneyKitSupportingTeamArmorsmith             int  `json:"losses_tourney_kit_supporting_team_armorsmith"`
				MostKillsGameTourney                                 int  `json:"most_kills_game_tourney"`
				MostKillsGameTourneyKitSupportingTeamArmorsmith      int  `json:"most_kills_game_tourney_kit_supporting_team_armorsmith"`
				MostKillsGameTourneyTeamsTourney                     int  `json:"most_kills_game_tourney_teams_tourney"`
				SoulsGatheredTourney                                 int  `json:"souls_gathered_tourney"`
				SurvivedPlayersTourneyKitSupportingTeamArmorsmith    int  `json:"survived_players_tourney_kit_supporting_team_armorsmith"`
				TimePlayedTourneyKitSupportingTeamArmorsmith         int  `json:"time_played_tourney_kit_supporting_team_armorsmith"`
				TourneySwInsaneDoubles0Kills                         int  `json:"tourney_sw_insane_doubles_0_kills"`
				TourneySwInsaneDoubles0MostKillsGame                 int  `json:"tourney_sw_insane_doubles_0_most_kills_game"`
				TourneySwInsaneDoubles0Souls                         int  `json:"tourney_sw_insane_doubles_0_souls"`
				TourneySwInsaneDoubles0SoulsGathered                 int  `json:"tourney_sw_insane_doubles_0_souls_gathered"`
				TourneySwInsaneDoubles0VoidKills                     int  `json:"tourney_sw_insane_doubles_0_void_kills"`
				VoidKillsTourney                                     int  `json:"void_kills_tourney"`
				VoidKillsTourneyKitSupportingTeamArmorsmith          int  `json:"void_kills_tourney_kit_supporting_team_armorsmith"`
				VoidKillsTourneyTeamsTourney                         int  `json:"void_kills_tourney_teams_tourney"`
				LongestBowKillTourney                                int  `json:"longest_bow_kill_tourney"`
				LongestBowKillTourneyKitSupportingTeamArmorsmith     int  `json:"longest_bow_kill_tourney_kit_supporting_team_armorsmith"`
				LongestBowKillTourneyTeamsTourney                    int  `json:"longest_bow_kill_tourney_teams_tourney"`
				MeleeKillsTourney                                    int  `json:"melee_kills_tourney"`
				MeleeKillsTourneyKitSupportingTeamArmorsmith         int  `json:"melee_kills_tourney_kit_supporting_team_armorsmith"`
				MeleeKillsTourneyTeamsTourney                        int  `json:"melee_kills_tourney_teams_tourney"`
				TourneySwInsaneDoubles0LongestBowKill                int  `json:"tourney_sw_insane_doubles_0_longest_bow_kill"`
				TourneySwInsaneDoubles0MeleeKills                    int  `json:"tourney_sw_insane_doubles_0_melee_kills"`
				BlocksBrokenTourney                                  int  `json:"blocks_broken_tourney"`
				TourneySwInsaneDoubles0BlocksBroken                  int  `json:"tourney_sw_insane_doubles_0_blocks_broken"`
				ArrowsHitTourney                                     int  `json:"arrows_hit_tourney"`
				ArrowsHitTourneyKitSupportingTeamArmorsmith          int  `json:"arrows_hit_tourney_kit_supporting_team_armorsmith"`
				ArrowsHitTourneyTeamsTourney                         int  `json:"arrows_hit_tourney_teams_tourney"`
				ArrowsShotTourney                                    int  `json:"arrows_shot_tourney"`
				ArrowsShotTourneyKitSupportingTeamArmorsmith         int  `json:"arrows_shot_tourney_kit_supporting_team_armorsmith"`
				ArrowsShotTourneyTeamsTourney                        int  `json:"arrows_shot_tourney_teams_tourney"`
				GamesTourney                                         int  `json:"games_tourney"`
				GamesTourneyKitSupportingTeamArmorsmith              int  `json:"games_tourney_kit_supporting_team_armorsmith"`
				GamesTourneyTeamsTourney                             int  `json:"games_tourney_teams_tourney"`
				LongestBowShotTourney                                int  `json:"longest_bow_shot_tourney"`
				LongestBowShotTourneyKitSupportingTeamArmorsmith     int  `json:"longest_bow_shot_tourney_kit_supporting_team_armorsmith"`
				LongestBowShotTourneyTeamsTourney                    int  `json:"longest_bow_shot_tourney_teams_tourney"`
				TourneySwInsaneDoubles0ArrowsHit                     int  `json:"tourney_sw_insane_doubles_0_arrows_hit"`
				TourneySwInsaneDoubles0ArrowsShot                    int  `json:"tourney_sw_insane_doubles_0_arrows_shot"`
				TourneySwInsaneDoubles0Games                         int  `json:"tourney_sw_insane_doubles_0_games"`
				TourneySwInsaneDoubles0LongestBowShot                int  `json:"tourney_sw_insane_doubles_0_longest_bow_shot"`
				DeathsTourneyKitDefendingTeamBaseballPlayer          int  `json:"deaths_tourney_kit_defending_team_baseball-player"`
				LossesTourneyKitDefendingTeamBaseballPlayer          int  `json:"losses_tourney_kit_defending_team_baseball-player"`
				SurvivedPlayersTourneyKitDefendingTeamBaseballPlayer int  `json:"survived_players_tourney_kit_defending_team_baseball-player"`
				TimePlayedTourneyKitDefendingTeamBaseballPlayer      int  `json:"time_played_tourney_kit_defending_team_baseball-player"`
				ChestsOpenedTourneyKitDefendingTeamBaseballPlayer    int  `json:"chests_opened_tourney_kit_defending_team_baseball-player"`
				KillsTourneyKitDefendingTeamBaseballPlayer           int  `json:"kills_tourney_kit_defending_team_baseball-player"`
				LongestBowKillTourneyKitDefendingTeamBaseballPlayer  int  `json:"longest_bow_kill_tourney_kit_defending_team_baseball-player"`
				MeleeKillsTourneyKitDefendingTeamBaseballPlayer      int  `json:"melee_kills_tourney_kit_defending_team_baseball-player"`
				MostKillsGameTourneyKitDefendingTeamBaseballPlayer   int  `json:"most_kills_game_tourney_kit_defending_team_baseball-player"`
				EggThrownTourney                                     int  `json:"egg_thrown_tourney"`
				TourneySwInsaneDoubles0EggThrown                     int  `json:"tourney_sw_insane_doubles_0_egg_thrown"`
				VoidKillsTourneyKitDefendingTeamBaseballPlayer       int  `json:"void_kills_tourney_kit_defending_team_baseball-player"`
				ArrowsShotTourneyKitDefendingTeamBaseballPlayer      int  `json:"arrows_shot_tourney_kit_defending_team_baseball-player"`
				EnderpearlsThrownTourney                             int  `json:"enderpearls_thrown_tourney"`
				ItemsEnchantedTourney                                int  `json:"items_enchanted_tourney"`
				TourneySwInsaneDoubles0EnderpearlsThrown             int  `json:"tourney_sw_insane_doubles_0_enderpearls_thrown"`
				TourneySwInsaneDoubles0ItemsEnchanted                int  `json:"tourney_sw_insane_doubles_0_items_enchanted"`
				FastestWinTourney                                    int  `json:"fastest_win_tourney"`
				FastestWinTourneyKitSupportingTeamArmorsmith         int  `json:"fastest_win_tourney_kit_supporting_team_armorsmith"`
				FastestWinTourneyTeamsTourney                        int  `json:"fastest_win_tourney_teams_tourney"`
				TourneySwInsaneDoubles0FastestWin                    int  `json:"tourney_sw_insane_doubles_0_fastest_win"`
				TourneySwInsaneDoubles0Wins                          int  `json:"tourney_sw_insane_doubles_0_wins"`
				WinsTourney                                          int  `json:"wins_tourney"`
				WinsTourneyKitSupportingTeamArmorsmith               int  `json:"wins_tourney_kit_supporting_team_armorsmith"`
				WinsTourneyTeamsTourney                              int  `json:"wins_tourney_teams_tourney"`
				ChestsOpenedTourneyKitMythicalEndLord                int  `json:"chests_opened_tourney_kit_mythical_end-lord"`
				DeathsTourneyKitMythicalEndLord                      int  `json:"deaths_tourney_kit_mythical_end-lord"`
				LossesTourneyKitMythicalEndLord                      int  `json:"losses_tourney_kit_mythical_end-lord"`
				SurvivedPlayersTourneyKitMythicalEndLord             int  `json:"survived_players_tourney_kit_mythical_end-lord"`
				TimePlayedTourneyKitMythicalEndLord                  int  `json:"time_played_tourney_kit_mythical_end-lord"`
				MegaMiningExpertise                                  int  `json:"mega_mining_expertise"`
				MegaBlackMagic                                       int  `json:"mega_black_magic"`
				SoloInstantSmelting                                  int  `json:"solo_instant_smelting"`
				SkywarsEasterBoxes                                   int  `json:"skywars_easter_boxes"`
				FreeEventKeySkywarsEasterBoxes2020                   bool `json:"free_event_key_skywars_easter_boxes_2020"`
				FastestWinKitAttackingTeamEnergix                    int  `json:"fastest_win_kit_attacking_team_energix"`
				GamesKitAttackingTeamEnergix                         int  `json:"games_kit_attacking_team_energix"`
				KillstreakKitAttackingTeamEnergix                    int  `json:"killstreak_kit_attacking_team_energix"`
				WinsKitAttackingTeamEnergix                          int  `json:"wins_kit_attacking_team_energix"`
				VoidKillsKitAttackingTeamEnergix                     int  `json:"void_kills_kit_attacking_team_energix"`
				ArrowsHitKitAttackingTeamEnergix                     int  `json:"arrows_hit_kit_attacking_team_energix"`
				LongestBowShotKitAttackingTeamEnergix                int  `json:"longest_bow_shot_kit_attacking_team_energix"`
				HeadsKitAttackingTeamEnergix                         int  `json:"heads_kit_attacking_team_energix"`
				HeadsSucculentKitAttackingTeamEnergix                int  `json:"heads_succulent_kit_attacking_team_energix"`
				HeadsYuckyKitAttackingTeamEnergix                    int  `json:"heads_yucky_kit_attacking_team_energix"`
				BowKillsKitAttackingTeamEnergix                      int  `json:"bow_kills_kit_attacking_team_energix"`
				ArrowsHitKitMythicalNetherLord                       int  `json:"arrows_hit_kit_mythical_nether-lord"`
				ArrowsShotKitMythicalNetherLord                      int  `json:"arrows_shot_kit_mythical_nether-lord"`
				ChestsOpenedKitMythicalNetherLord                    int  `json:"chests_opened_kit_mythical_nether-lord"`
				DeathsKitMythicalNetherLord                          int  `json:"deaths_kit_mythical_nether-lord"`
				LongestBowShotKitMythicalNetherLord                  int  `json:"longest_bow_shot_kit_mythical_nether-lord"`
				LossesKitMythicalNetherLord                          int  `json:"losses_kit_mythical_nether-lord"`
				SurvivedPlayersKitMythicalNetherLord                 int  `json:"survived_players_kit_mythical_nether-lord"`
				TimePlayedKitMythicalNetherLord                      int  `json:"time_played_kit_mythical_nether-lord"`
				FastestWinKitBasicSoloArmorsmith                     int  `json:"fastest_win_kit_basic_solo_armorsmith"`
				GamesKitBasicSoloArmorsmith                          int  `json:"games_kit_basic_solo_armorsmith"`
				KillstreakKitBasicSoloArmorsmith                     int  `json:"killstreak_kit_basic_solo_armorsmith"`
				WinsKitBasicSoloArmorsmith                           int  `json:"wins_kit_basic_solo_armorsmith"`
				ArrowsHitKitBasicSoloArmorsmith                      int  `json:"arrows_hit_kit_basic_solo_armorsmith"`
				ArrowsShotKitBasicSoloArmorsmith                     int  `json:"arrows_shot_kit_basic_solo_armorsmith"`
				LongestBowShotKitBasicSoloArmorsmith                 int  `json:"longest_bow_shot_kit_basic_solo_armorsmith"`
				DeathsKitDefendingTeamFarmer                         int  `json:"deaths_kit_defending_team_farmer"`
				LossesKitDefendingTeamFarmer                         int  `json:"losses_kit_defending_team_farmer"`
				TimePlayedKitDefendingTeamFarmer                     int  `json:"time_played_kit_defending_team_farmer"`
				ArrowsHitKitDefendingTeamFarmer                      int  `json:"arrows_hit_kit_defending_team_farmer"`
				ArrowsShotKitDefendingTeamFarmer                     int  `json:"arrows_shot_kit_defending_team_farmer"`
				ChestsOpenedKitDefendingTeamFarmer                   int  `json:"chests_opened_kit_defending_team_farmer"`
				KillsKitDefendingTeamFarmer                          int  `json:"kills_kit_defending_team_farmer"`
				LongestBowShotKitDefendingTeamFarmer                 int  `json:"longest_bow_shot_kit_defending_team_farmer"`
				MostKillsGameKitDefendingTeamFarmer                  int  `json:"most_kills_game_kit_defending_team_farmer"`
				SurvivedPlayersKitDefendingTeamFarmer                int  `json:"survived_players_kit_defending_team_farmer"`
				VoidKillsKitDefendingTeamFarmer                      int  `json:"void_kills_kit_defending_team_farmer"`
				LongestBowKillKitDefendingTeamFarmer                 int  `json:"longest_bow_kill_kit_defending_team_farmer"`
				MeleeKillsKitDefendingTeamFarmer                     int  `json:"melee_kills_kit_defending_team_farmer"`
				FastestWinKitDefendingTeamFarmer                     int  `json:"fastest_win_kit_defending_team_farmer"`
				GamesKitDefendingTeamFarmer                          int  `json:"games_kit_defending_team_farmer"`
				KillstreakKitDefendingTeamFarmer                     int  `json:"killstreak_kit_defending_team_farmer"`
				WinsKitDefendingTeamFarmer                           int  `json:"wins_kit_defending_team_farmer"`
				HeadsEwwKitDefendingTeamFarmer                       int  `json:"heads_eww_kit_defending_team_farmer"`
				HeadsKitDefendingTeamFarmer                          int  `json:"heads_kit_defending_team_farmer"`
				AssistsKitDefendingTeamFarmer                        int  `json:"assists_kit_defending_team_farmer"`
				ChestsOpenedKitSupportingTeamZookeeper               int  `json:"chests_opened_kit_supporting_team_zookeeper"`
				DeathsKitSupportingTeamZookeeper                     int  `json:"deaths_kit_supporting_team_zookeeper"`
				LossesKitSupportingTeamZookeeper                     int  `json:"losses_kit_supporting_team_zookeeper"`
				SurvivedPlayersKitSupportingTeamZookeeper            int  `json:"survived_players_kit_supporting_team_zookeeper"`
				TimePlayedKitSupportingTeamZookeeper                 int  `json:"time_played_kit_supporting_team_zookeeper"`
				KillsKitSupportingTeamZookeeper                      int  `json:"kills_kit_supporting_team_zookeeper"`
				LongestBowKillKitSupportingTeamZookeeper             int  `json:"longest_bow_kill_kit_supporting_team_zookeeper"`
				MeleeKillsKitSupportingTeamZookeeper                 int  `json:"melee_kills_kit_supporting_team_zookeeper"`
				MostKillsGameKitSupportingTeamZookeeper              int  `json:"most_kills_game_kit_supporting_team_zookeeper"`
				ArrowsShotKitAttackingTeamScout                      int  `json:"arrows_shot_kit_attacking_team_scout"`
				ChestsOpenedKitAttackingTeamScout                    int  `json:"chests_opened_kit_attacking_team_scout"`
				DeathsKitAttackingTeamScout                          int  `json:"deaths_kit_attacking_team_scout"`
				LossesKitAttackingTeamScout                          int  `json:"losses_kit_attacking_team_scout"`
				SurvivedPlayersKitAttackingTeamScout                 int  `json:"survived_players_kit_attacking_team_scout"`
				TimePlayedKitAttackingTeamScout                      int  `json:"time_played_kit_attacking_team_scout"`
				BowKillsKitDefendingTeamFrog                         int  `json:"bow_kills_kit_defending_team_frog"`
				DeathsKitSupportingTeamEcologist                     int  `json:"deaths_kit_supporting_team_ecologist"`
				KillsKitSupportingTeamEcologist                      int  `json:"kills_kit_supporting_team_ecologist"`
				LongestBowKillKitSupportingTeamEcologist             int  `json:"longest_bow_kill_kit_supporting_team_ecologist"`
				LossesKitSupportingTeamEcologist                     int  `json:"losses_kit_supporting_team_ecologist"`
				MeleeKillsKitSupportingTeamEcologist                 int  `json:"melee_kills_kit_supporting_team_ecologist"`
				MostKillsGameKitSupportingTeamEcologist              int  `json:"most_kills_game_kit_supporting_team_ecologist"`
				SurvivedPlayersKitSupportingTeamEcologist            int  `json:"survived_players_kit_supporting_team_ecologist"`
				TimePlayedKitSupportingTeamEcologist                 int  `json:"time_played_kit_supporting_team_ecologist"`
				ArrowsShotKitSupportingTeamEcologist                 int  `json:"arrows_shot_kit_supporting_team_ecologist"`
				GamesKitSupportingTeamEcologist                      int  `json:"games_kit_supporting_team_ecologist"`
				VoidKillsKitSupportingTeamEcologist                  int  `json:"void_kills_kit_supporting_team_ecologist"`
				ArrowsHitKitSupportingTeamEcologist                  int  `json:"arrows_hit_kit_supporting_team_ecologist"`
				LongestBowShotKitSupportingTeamEcologist             int  `json:"longest_bow_shot_kit_supporting_team_ecologist"`
				ChestsOpenedKitSupportingTeamEcologist               int  `json:"chests_opened_kit_supporting_team_ecologist"`
				AssistsKitSupportingTeamEcologist                    int  `json:"assists_kit_supporting_team_ecologist"`
				HeadsKitSupportingTeamEcologist                      int  `json:"heads_kit_supporting_team_ecologist"`
				HeadsMehKitSupportingTeamEcologist                   int  `json:"heads_meh_kit_supporting_team_ecologist"`
				HeadsYuckyKitSupportingTeamEcologist                 int  `json:"heads_yucky_kit_supporting_team_ecologist"`
				ChestsOpenedKitAttackingTeamEnderman                 int  `json:"chests_opened_kit_attacking_team_enderman"`
				DeathsKitAttackingTeamEnderman                       int  `json:"deaths_kit_attacking_team_enderman"`
				LossesKitAttackingTeamEnderman                       int  `json:"losses_kit_attacking_team_enderman"`
				TimePlayedKitAttackingTeamEnderman                   int  `json:"time_played_kit_attacking_team_enderman"`
				SurvivedPlayersKitAttackingTeamEnderman              int  `json:"survived_players_kit_attacking_team_enderman"`
				AssistsKitAttackingTeamEnderman                      int  `json:"assists_kit_attacking_team_enderman"`
				ArrowsHitKitAttackingTeamEnderman                    int  `json:"arrows_hit_kit_attacking_team_enderman"`
				ArrowsShotKitAttackingTeamEnderman                   int  `json:"arrows_shot_kit_attacking_team_enderman"`
				KillsKitAttackingTeamEnderman                        int  `json:"kills_kit_attacking_team_enderman"`
				LongestBowShotKitAttackingTeamEnderman               int  `json:"longest_bow_shot_kit_attacking_team_enderman"`
				MostKillsGameKitAttackingTeamEnderman                int  `json:"most_kills_game_kit_attacking_team_enderman"`
				VoidKillsKitAttackingTeamEnderman                    int  `json:"void_kills_kit_attacking_team_enderman"`
				ChestsOpenedKitDefendingTeamBaseballPlayer           int  `json:"chests_opened_kit_defending_team_baseball-player"`
				DeathsKitDefendingTeamBaseballPlayer                 int  `json:"deaths_kit_defending_team_baseball-player"`
				GamesKitDefendingTeamBaseballPlayer                  int  `json:"games_kit_defending_team_baseball-player"`
				KillsKitDefendingTeamBaseballPlayer                  int  `json:"kills_kit_defending_team_baseball-player"`
				LossesKitDefendingTeamBaseballPlayer                 int  `json:"losses_kit_defending_team_baseball-player"`
				MostKillsGameKitDefendingTeamBaseballPlayer          int  `json:"most_kills_game_kit_defending_team_baseball-player"`
				SurvivedPlayersKitDefendingTeamBaseballPlayer        int  `json:"survived_players_kit_defending_team_baseball-player"`
				TimePlayedKitDefendingTeamBaseballPlayer             int  `json:"time_played_kit_defending_team_baseball-player"`
				VoidKillsKitDefendingTeamBaseballPlayer              int  `json:"void_kills_kit_defending_team_baseball-player"`
				ChestsOpenedKitEnderchestTeamEnderchest              int  `json:"chests_opened_kit_enderchest_team_enderchest"`
				DeathsKitEnderchestTeamEnderchest                    int  `json:"deaths_kit_enderchest_team_enderchest"`
				LossesKitEnderchestTeamEnderchest                    int  `json:"losses_kit_enderchest_team_enderchest"`
				SurvivedPlayersKitEnderchestTeamEnderchest           int  `json:"survived_players_kit_enderchest_team_enderchest"`
				TimePlayedKitEnderchestTeamEnderchest                int  `json:"time_played_kit_enderchest_team_enderchest"`
				AssistsKitEnderchestTeamEnderchest                   int  `json:"assists_kit_enderchest_team_enderchest"`
				ArrowsShotKitEnderchestTeamEnderchest                int  `json:"arrows_shot_kit_enderchest_team_enderchest"`
				HeadsDecentKitEnderchestTeamEnderchest               int  `json:"heads_decent_kit_enderchest_team_enderchest"`
				HeadsKitEnderchestTeamEnderchest                     int  `json:"heads_kit_enderchest_team_enderchest"`
				KillsKitEnderchestTeamEnderchest                     int  `json:"kills_kit_enderchest_team_enderchest"`
				MostKillsGameKitEnderchestTeamEnderchest             int  `json:"most_kills_game_kit_enderchest_team_enderchest"`
				VoidKillsKitEnderchestTeamEnderchest                 int  `json:"void_kills_kit_enderchest_team_enderchest"`
				ArrowsHitKitEnderchestTeamEnderchest                 int  `json:"arrows_hit_kit_enderchest_team_enderchest"`
				LongestBowShotKitEnderchestTeamEnderchest            int  `json:"longest_bow_shot_kit_enderchest_team_enderchest"`
				FastestWinKitEnderchestTeamEnderchest                int  `json:"fastest_win_kit_enderchest_team_enderchest"`
				GamesKitEnderchestTeamEnderchest                     int  `json:"games_kit_enderchest_team_enderchest"`
				KillstreakKitEnderchestTeamEnderchest                int  `json:"killstreak_kit_enderchest_team_enderchest"`
				LongestBowKillKitEnderchestTeamEnderchest            int  `json:"longest_bow_kill_kit_enderchest_team_enderchest"`
				MeleeKillsKitEnderchestTeamEnderchest                int  `json:"melee_kills_kit_enderchest_team_enderchest"`
				WinsKitEnderchestTeamEnderchest                      int  `json:"wins_kit_enderchest_team_enderchest"`
				KitSupportingTeamPyroInventory                       struct {
					FLINTANDSTEEL0     string `json:"FLINT_AND_STEEL:0"`
					POTION3            string `json:"POTION:3"`
					LAVABUCKET0        string `json:"LAVA_BUCKET:0"`
					DIAMONDCHESTPLATE0 string `json:"DIAMOND_CHESTPLATE:0"`
				} `json:"kit_supporting_team_pyro_inventory"`
				ChestsOpenedKitSupportingTeamPyro          int    `json:"chests_opened_kit_supporting_team_pyro"`
				DeathsKitSupportingTeamPyro                int    `json:"deaths_kit_supporting_team_pyro"`
				LossesKitSupportingTeamPyro                int    `json:"losses_kit_supporting_team_pyro"`
				SurvivedPlayersKitSupportingTeamPyro       int    `json:"survived_players_kit_supporting_team_pyro"`
				TimePlayedKitSupportingTeamPyro            int    `json:"time_played_kit_supporting_team_pyro"`
				KillsKitSupportingTeamPyro                 int    `json:"kills_kit_supporting_team_pyro"`
				MostKillsGameKitSupportingTeamPyro         int    `json:"most_kills_game_kit_supporting_team_pyro"`
				VoidKillsKitSupportingTeamPyro             int    `json:"void_kills_kit_supporting_team_pyro"`
				ArrowsHitKitSupportingTeamPyro             int    `json:"arrows_hit_kit_supporting_team_pyro"`
				ArrowsShotKitSupportingTeamPyro            int    `json:"arrows_shot_kit_supporting_team_pyro"`
				LongestBowShotKitSupportingTeamPyro        int    `json:"longest_bow_shot_kit_supporting_team_pyro"`
				GamesKitSupportingTeamPyro                 int    `json:"games_kit_supporting_team_pyro"`
				AssistsKitSupportingTeamPyro               int    `json:"assists_kit_supporting_team_pyro"`
				FastestWinKitSupportingTeamPyro            int    `json:"fastest_win_kit_supporting_team_pyro"`
				KillstreakKitSupportingTeamPyro            int    `json:"killstreak_kit_supporting_team_pyro"`
				WinsKitSupportingTeamPyro                  int    `json:"wins_kit_supporting_team_pyro"`
				ChestsOpenedKitAttackingTeamJester         int    `json:"chests_opened_kit_attacking_team_jester"`
				DeathsKitAttackingTeamJester               int    `json:"deaths_kit_attacking_team_jester"`
				KillsKitAttackingTeamJester                int    `json:"kills_kit_attacking_team_jester"`
				LongestBowKillKitAttackingTeamJester       int    `json:"longest_bow_kill_kit_attacking_team_jester"`
				LossesKitAttackingTeamJester               int    `json:"losses_kit_attacking_team_jester"`
				MeleeKillsKitAttackingTeamJester           int    `json:"melee_kills_kit_attacking_team_jester"`
				MostKillsGameKitAttackingTeamJester        int    `json:"most_kills_game_kit_attacking_team_jester"`
				SurvivedPlayersKitAttackingTeamJester      int    `json:"survived_players_kit_attacking_team_jester"`
				TimePlayedKitAttackingTeamJester           int    `json:"time_played_kit_attacking_team_jester"`
				HeadsKitSupportingTeamPyro                 int    `json:"heads_kit_supporting_team_pyro"`
				HeadsYuckyKitSupportingTeamPyro            int    `json:"heads_yucky_kit_supporting_team_pyro"`
				LongestBowKillKitSupportingTeamPyro        int    `json:"longest_bow_kill_kit_supporting_team_pyro"`
				MeleeKillsKitSupportingTeamPyro            int    `json:"melee_kills_kit_supporting_team_pyro"`
				GamesKitAttackingTeamJester                int    `json:"games_kit_attacking_team_jester"`
				VoidKillsKitAttackingTeamJester            int    `json:"void_kills_kit_attacking_team_jester"`
				ArrowsShotKitAttackingTeamJester           int    `json:"arrows_shot_kit_attacking_team_jester"`
				ArrowsHitKitAttackingTeamJester            int    `json:"arrows_hit_kit_attacking_team_jester"`
				LongestBowShotKitAttackingTeamJester       int    `json:"longest_bow_shot_kit_attacking_team_jester"`
				AssistsKitAttackingTeamJester              int    `json:"assists_kit_attacking_team_jester"`
				HeadsKitAttackingTeamJester                int    `json:"heads_kit_attacking_team_jester"`
				HeadsTastyKitAttackingTeamJester           int    `json:"heads_tasty_kit_attacking_team_jester"`
				ChestsOpenedKitSupportingTeamArmorsmith    int    `json:"chests_opened_kit_supporting_team_armorsmith"`
				DeathsKitSupportingTeamArmorsmith          int    `json:"deaths_kit_supporting_team_armorsmith"`
				GamesKitSupportingTeamArmorsmith           int    `json:"games_kit_supporting_team_armorsmith"`
				KillsKitSupportingTeamArmorsmith           int    `json:"kills_kit_supporting_team_armorsmith"`
				LongestBowKillKitSupportingTeamArmorsmith  int    `json:"longest_bow_kill_kit_supporting_team_armorsmith"`
				LossesKitSupportingTeamArmorsmith          int    `json:"losses_kit_supporting_team_armorsmith"`
				MeleeKillsKitSupportingTeamArmorsmith      int    `json:"melee_kills_kit_supporting_team_armorsmith"`
				MostKillsGameKitSupportingTeamArmorsmith   int    `json:"most_kills_game_kit_supporting_team_armorsmith"`
				SurvivedPlayersKitSupportingTeamArmorsmith int    `json:"survived_players_kit_supporting_team_armorsmith"`
				TimePlayedKitSupportingTeamArmorsmith      int    `json:"time_played_kit_supporting_team_armorsmith"`
				ArrowsHitKitSupportingTeamArmorsmith       int    `json:"arrows_hit_kit_supporting_team_armorsmith"`
				ArrowsShotKitSupportingTeamArmorsmith      int    `json:"arrows_shot_kit_supporting_team_armorsmith"`
				LongestBowShotKitSupportingTeamArmorsmith  int    `json:"longest_bow_shot_kit_supporting_team_armorsmith"`
				FastestWinKitSupportingTeamArmorsmith      int    `json:"fastest_win_kit_supporting_team_armorsmith"`
				KillstreakKitSupportingTeamArmorsmith      int    `json:"killstreak_kit_supporting_team_armorsmith"`
				WinsKitSupportingTeamArmorsmith            int    `json:"wins_kit_supporting_team_armorsmith"`
				AssistsKitSupportingTeamArmorsmith         int    `json:"assists_kit_supporting_team_armorsmith"`
				VoidKillsKitSupportingTeamArmorsmith       int    `json:"void_kills_kit_supporting_team_armorsmith"`
				MegaBridger                                int    `json:"mega_bridger"`
				ActiveKitMEGARandom                        bool   `json:"activeKit_MEGA_random"`
				ActiveKitMEGA                              string `json:"activeKit_MEGA"`
				HeadsKitSupportingTeamArmorsmith           int    `json:"heads_kit_supporting_team_armorsmith"`
				HeadsMehKitSupportingTeamArmorsmith        int    `json:"heads_meh_kit_supporting_team_armorsmith"`
				HeadsSucculentKitSupportingTeamArmorsmith  int    `json:"heads_succulent_kit_supporting_team_armorsmith"`
				HeadsTastyKitSupportingTeamArmorsmith      int    `json:"heads_tasty_kit_supporting_team_armorsmith"`
				TeamBlackMagic                             int    `json:"team_black_magic"`
				SoloBlackMagic                             int    `json:"solo_black_magic"`
				SoloJuggernaut                             int    `json:"solo_juggernaut"`
				SoloNourishment                            int    `json:"solo_nourishment"`
				SoloEnvironmentalExpert                    int    `json:"solo_environmental_expert"`
				ChestsOpenedMega                           int    `json:"chests_opened_mega"`
				DeathsMega                                 int    `json:"deaths_mega"`
				DeathsMegaNormal                           int    `json:"deaths_mega_normal"`
				LossesMega                                 int    `json:"losses_mega"`
				LossesMegaNormal                           int    `json:"losses_mega_normal"`
				SurvivedPlayersMega                        int    `json:"survived_players_mega"`
				TimePlayedMega                             int    `json:"time_played_mega"`
				ArrowsHitKitAttackingTeamGrenade           int    `json:"arrows_hit_kit_attacking_team_grenade"`
				ArrowsShotKitAttackingTeamGrenade          int    `json:"arrows_shot_kit_attacking_team_grenade"`
				ChestsOpenedKitAttackingTeamGrenade        int    `json:"chests_opened_kit_attacking_team_grenade"`
				DeathsKitAttackingTeamGrenade              int    `json:"deaths_kit_attacking_team_grenade"`
				LongestBowShotKitAttackingTeamGrenade      int    `json:"longest_bow_shot_kit_attacking_team_grenade"`
				LossesKitAttackingTeamGrenade              int    `json:"losses_kit_attacking_team_grenade"`
				SurvivedPlayersKitAttackingTeamGrenade     int    `json:"survived_players_kit_attacking_team_grenade"`
				TimePlayedKitAttackingTeamGrenade          int    `json:"time_played_kit_attacking_team_grenade"`
				HeadsYuckyKitSupportingTeamArmorsmith      int    `json:"heads_yucky_kit_supporting_team_armorsmith"`
				KillsKitAttackingTeamGrenade               int    `json:"kills_kit_attacking_team_grenade"`
				MostKillsGameKitAttackingTeamGrenade       int    `json:"most_kills_game_kit_attacking_team_grenade"`
				BowKillsKitSupportingTeamArmorsmith        int    `json:"bow_kills_kit_supporting_team_armorsmith"`
				AssistsKitAttackingTeamEnergix             int    `json:"assists_kit_attacking_team_energix"`
				HeadsDecentKitSupportingTeamArmorsmith     int    `json:"heads_decent_kit_supporting_team_armorsmith"`
				MobKills                                   int    `json:"mob_kills"`
				MobKillsKitSupportingTeamArmorsmith        int    `json:"mob_kills_kit_supporting_team_armorsmith"`
				MobKillsSolo                               int    `json:"mob_kills_solo"`
				MegaArrowRecovery                          int    `json:"mega_arrow_recovery"`
				TeamAnnoyOMite                             int    `json:"team_annoy-o-mite"`
				HeadsEwwKitSupportingTeamArmorsmith        int    `json:"heads_eww_kit_supporting_team_armorsmith"`
				HeadsSaltyKitSupportingTeamArmorsmith      int    `json:"heads_salty_kit_supporting_team_armorsmith"`
				FastestWinKitSupportingTeamEcologist       int    `json:"fastest_win_kit_supporting_team_ecologist"`
				KillstreakKitSupportingTeamEcologist       int    `json:"killstreak_kit_supporting_team_ecologist"`
				WinsKitSupportingTeamEcologist             int    `json:"wins_kit_supporting_team_ecologist"`
				GamesKitAttackingTeamEnderman              int    `json:"games_kit_attacking_team_enderman"`
				LongestBowKillKitAttackingTeamEnderman     int    `json:"longest_bow_kill_kit_attacking_team_enderman"`
				MeleeKillsKitAttackingTeamEnderman         int    `json:"melee_kills_kit_attacking_team_enderman"`
				SoloFrost                                  int    `json:"solo_frost"`
				AngelOfDeathLevel                          int    `json:"angel_of_death_level"`
				ChestsOpenedKitAttackingTeamFisherman      int    `json:"chests_opened_kit_attacking_team_fisherman"`
				DeathsKitAttackingTeamFisherman            int    `json:"deaths_kit_attacking_team_fisherman"`
				KillsKitAttackingTeamFisherman             int    `json:"kills_kit_attacking_team_fisherman"`
				LossesKitAttackingTeamFisherman            int    `json:"losses_kit_attacking_team_fisherman"`
				MostKillsGameKitAttackingTeamFisherman     int    `json:"most_kills_game_kit_attacking_team_fisherman"`
				SurvivedPlayersKitAttackingTeamFisherman   int    `json:"survived_players_kit_attacking_team_fisherman"`
				TimePlayedKitAttackingTeamFisherman        int    `json:"time_played_kit_attacking_team_fisherman"`
				VoidKillsKitAttackingTeamFisherman         int    `json:"void_kills_kit_attacking_team_fisherman"`
				FastestWinKitAttackingTeamJester           int    `json:"fastest_win_kit_attacking_team_jester"`
				KillstreakKitAttackingTeamJester           int    `json:"killstreak_kit_attacking_team_jester"`
				WinsKitAttackingTeamJester                 int    `json:"wins_kit_attacking_team_jester"`
				TeamArrowRecovery                          int    `json:"team_arrow_recovery"`
				ActiveBalloon                              string `json:"active_balloon"`
				VoidKillsKitBasicSoloArmorsmith            int    `json:"void_kills_kit_basic_solo_armorsmith"`
				GamesKitAdvancedSoloArmorer                int    `json:"games_kit_advanced_solo_armorer"`
				ShopSort                                   string `json:"shop_sort"`
				LongestBowKillKitAdvancedSoloArmorer       int    `json:"longest_bow_kill_kit_advanced_solo_armorer"`
				MeleeKillsKitAdvancedSoloArmorer           int    `json:"melee_kills_kit_advanced_solo_armorer"`
				DeathsKitBasicSoloEcologist                int    `json:"deaths_kit_basic_solo_ecologist"`
				LossesKitBasicSoloEcologist                int    `json:"losses_kit_basic_solo_ecologist"`
				SurvivedPlayersKitBasicSoloEcologist       int    `json:"survived_players_kit_basic_solo_ecologist"`
				TimePlayedKitBasicSoloEcologist            int    `json:"time_played_kit_basic_solo_ecologist"`
				BowKillsKitSupportingTeamEcologist         int    `json:"bow_kills_kit_supporting_team_ecologist"`
				HeadsDivine                                int    `json:"heads_divine"`
				HeadsDivineKitSupportingTeamEcologist      int    `json:"heads_divine_kit_supporting_team_ecologist"`
				HeadsDivineSolo                            int    `json:"heads_divine_solo"`
				HeadsSucculentKitSupportingTeamEcologist   int    `json:"heads_succulent_kit_supporting_team_ecologist"`
				HeadsDecentKitSupportingTeamEcologist      int    `json:"heads_decent_kit_supporting_team_ecologist"`
				HeadsEwwKitSupportingTeamEcologist         int    `json:"heads_eww_kit_supporting_team_ecologist"`
				HeadsTastyKitSupportingTeamEcologist       int    `json:"heads_tasty_kit_supporting_team_ecologist"`
				HeadsHeavenlyKitSupportingTeamEcologist    int    `json:"heads_heavenly_kit_supporting_team_ecologist"`
				ShopSortEnableOwnedFirst                   bool   `json:"shop_sort_enable_owned_first"`
				KitMegaMegaArmorer                         int    `json:"kit_mega_mega_armorer"`
				KitMegaMegaArmorerInventory                struct {
					DIAMONDBOOTS0      string `json:"DIAMOND_BOOTS:0"`
					POTION8            string `json:"POTION:8"`
					DIAMONDCHESTPLATE0 string `json:"DIAMOND_CHESTPLATE:0"`
				} `json:"kit_mega_mega_armorer_inventory"`
				TeamSpeedBoost                                  int  `json:"team_speed_boost"`
				ArrowsHitKitAdvancedSoloArmorer                 int  `json:"arrows_hit_kit_advanced_solo_armorer"`
				ArrowsShotKitAdvancedSoloArmorer                int  `json:"arrows_shot_kit_advanced_solo_armorer"`
				LongestBowShotKitAdvancedSoloArmorer            int  `json:"longest_bow_shot_kit_advanced_solo_armorer"`
				ArrowsShotKitDefendingTeamBaseballPlayer        int  `json:"arrows_shot_kit_defending_team_baseball-player"`
				ArrowsHitKitDefendingTeamBaseballPlayer         int  `json:"arrows_hit_kit_defending_team_baseball-player"`
				LongestBowKillKitDefendingTeamBaseballPlayer    int  `json:"longest_bow_kill_kit_defending_team_baseball-player"`
				LongestBowShotKitDefendingTeamBaseballPlayer    int  `json:"longest_bow_shot_kit_defending_team_baseball-player"`
				MeleeKillsKitDefendingTeamBaseballPlayer        int  `json:"melee_kills_kit_defending_team_baseball-player"`
				KillsKitAttackingTeamScout                      int  `json:"kills_kit_attacking_team_scout"`
				MostKillsGameKitAttackingTeamScout              int  `json:"most_kills_game_kit_attacking_team_scout"`
				VoidKillsKitAttackingTeamScout                  int  `json:"void_kills_kit_attacking_team_scout"`
				AssistsKitAdvancedSoloArmorer                   int  `json:"assists_kit_advanced_solo_armorer"`
				HeadsKitAdvancedSoloArmorer                     int  `json:"heads_kit_advanced_solo_armorer"`
				HeadsTastyKitAdvancedSoloArmorer                int  `json:"heads_tasty_kit_advanced_solo_armorer"`
				ArrowsHitLabKitSupportingTeamEcologist          int  `json:"arrows_hit_lab_kit_supporting_team_ecologist"`
				ArrowsShotLabKitSupportingTeamEcologist         int  `json:"arrows_shot_lab_kit_supporting_team_ecologist"`
				BowKillsLabKitSupportingTeamEcologist           int  `json:"bow_kills_lab_kit_supporting_team_ecologist"`
				BowKillsLabSolo                                 int  `json:"bow_kills_lab_solo"`
				ChestsOpenedLabKitSupportingTeamEcologist       int  `json:"chests_opened_lab_kit_supporting_team_ecologist"`
				DeathsLabKitSupportingTeamEcologist             int  `json:"deaths_lab_kit_supporting_team_ecologist"`
				KillsLabKitSupportingTeamEcologist              int  `json:"kills_lab_kit_supporting_team_ecologist"`
				LossesLabKitSupportingTeamEcologist             int  `json:"losses_lab_kit_supporting_team_ecologist"`
				SurvivedPlayersLabKitSupportingTeamEcologist    int  `json:"survived_players_lab_kit_supporting_team_ecologist"`
				TimePlayedLabKitSupportingTeamEcologist         int  `json:"time_played_lab_kit_supporting_team_ecologist"`
				GamesLabKitSupportingTeamEcologist              int  `json:"games_lab_kit_supporting_team_ecologist"`
				HarvestingSeason                                int  `json:"harvesting_season"`
				TeamBarbarian                                   int  `json:"team_barbarian"`
				MobKillsKitSupportingTeamEcologist              int  `json:"mob_kills_kit_supporting_team_ecologist"`
				MegaEnvironmentalExpert                         int  `json:"mega_environmental_expert"`
				ToggleTeamBarbarian                             bool `json:"toggle_team_barbarian"`
				AssistsKitBasicSoloEcologist                    int  `json:"assists_kit_basic_solo_ecologist"`
				GamesKitBasicSoloEcologist                      int  `json:"games_kit_basic_solo_ecologist"`
				KillsKitBasicSoloEcologist                      int  `json:"kills_kit_basic_solo_ecologist"`
				LongestBowKillKitBasicSoloEcologist             int  `json:"longest_bow_kill_kit_basic_solo_ecologist"`
				MeleeKillsKitBasicSoloEcologist                 int  `json:"melee_kills_kit_basic_solo_ecologist"`
				MostKillsGameKitBasicSoloEcologist              int  `json:"most_kills_game_kit_basic_solo_ecologist"`
				VoidKillsKitBasicSoloEcologist                  int  `json:"void_kills_kit_basic_solo_ecologist"`
				FallKills                                       int  `json:"fall_kills"`
				FallKillsKitSupportingTeamEcologist             int  `json:"fall_kills_kit_supporting_team_ecologist"`
				FallKillsSolo                                   int  `json:"fall_kills_solo"`
				HeadsSaltyKitSupportingTeamEcologist            int  `json:"heads_salty_kit_supporting_team_ecologist"`
				HeadsSaltyTeam                                  int  `json:"heads_salty_team"`
				SoloAnnoyOMite                                  int  `json:"solo_annoy-o-mite"`
				TeamNecromancer                                 int  `json:"team_necromancer"`
				SoloSavior                                      int  `json:"solo_savior"`
				ChestsOpenedKitSupportingTeamPharaoh            int  `json:"chests_opened_kit_supporting_team_pharaoh"`
				DeathsKitSupportingTeamPharaoh                  int  `json:"deaths_kit_supporting_team_pharaoh"`
				KillsKitSupportingTeamPharaoh                   int  `json:"kills_kit_supporting_team_pharaoh"`
				LongestBowKillKitSupportingTeamPharaoh          int  `json:"longest_bow_kill_kit_supporting_team_pharaoh"`
				LossesKitSupportingTeamPharaoh                  int  `json:"losses_kit_supporting_team_pharaoh"`
				MeleeKillsKitSupportingTeamPharaoh              int  `json:"melee_kills_kit_supporting_team_pharaoh"`
				MostKillsGameKitSupportingTeamPharaoh           int  `json:"most_kills_game_kit_supporting_team_pharaoh"`
				SurvivedPlayersKitSupportingTeamPharaoh         int  `json:"survived_players_kit_supporting_team_pharaoh"`
				TimePlayedKitSupportingTeamPharaoh              int  `json:"time_played_kit_supporting_team_pharaoh"`
				ChestsOpenedKitMythicalThundermeister           int  `json:"chests_opened_kit_mythical_thundermeister"`
				DeathsKitMythicalThundermeister                 int  `json:"deaths_kit_mythical_thundermeister"`
				LossesKitMythicalThundermeister                 int  `json:"losses_kit_mythical_thundermeister"`
				SurvivedPlayersKitMythicalThundermeister        int  `json:"survived_players_kit_mythical_thundermeister"`
				TimePlayedKitMythicalThundermeister             int  `json:"time_played_kit_mythical_thundermeister"`
				ArrowsHitKitMythicalThundermeister              int  `json:"arrows_hit_kit_mythical_thundermeister"`
				ArrowsShotKitMythicalThundermeister             int  `json:"arrows_shot_kit_mythical_thundermeister"`
				LongestBowShotKitMythicalThundermeister         int  `json:"longest_bow_shot_kit_mythical_thundermeister"`
				MegaLuckyCharm                                  int  `json:"mega_lucky_charm"`
				FreeEventKeySkywarsHalloweenBoxes20202          bool `json:"free_event_key_skywars_halloween_boxes_2020_2"`
				ChestsOpenedKitMegaMegaArmorer                  int  `json:"chests_opened_kit_mega_mega_armorer"`
				DeathsKitMegaMegaArmorer                        int  `json:"deaths_kit_mega_mega_armorer"`
				LossesKitMegaMegaArmorer                        int  `json:"losses_kit_mega_mega_armorer"`
				SurvivedPlayersKitMegaMegaArmorer               int  `json:"survived_players_kit_mega_mega_armorer"`
				TimePlayedKitMegaMegaArmorer                    int  `json:"time_played_kit_mega_mega_armorer"`
				ArrowsHitKitMegaMegaArmorer                     int  `json:"arrows_hit_kit_mega_mega_armorer"`
				ArrowsHitMega                                   int  `json:"arrows_hit_mega"`
				ArrowsShotKitMegaMegaArmorer                    int  `json:"arrows_shot_kit_mega_mega_armorer"`
				ArrowsShotMega                                  int  `json:"arrows_shot_mega"`
				LongestBowShotKitMegaMegaArmorer                int  `json:"longest_bow_shot_kit_mega_mega_armorer"`
				LongestBowShotMega                              int  `json:"longest_bow_shot_mega"`
				KillsKitMegaMegaArmorer                         int  `json:"kills_kit_mega_mega_armorer"`
				KillsMega                                       int  `json:"kills_mega"`
				KillsMegaNormal                                 int  `json:"kills_mega_normal"`
				LongestBowKillKitMegaMegaArmorer                int  `json:"longest_bow_kill_kit_mega_mega_armorer"`
				LongestBowKillMega                              int  `json:"longest_bow_kill_mega"`
				MeleeKillsKitMegaMegaArmorer                    int  `json:"melee_kills_kit_mega_mega_armorer"`
				MeleeKillsMega                                  int  `json:"melee_kills_mega"`
				MostKillsGameKitMegaMegaArmorer                 int  `json:"most_kills_game_kit_mega_mega_armorer"`
				MostKillsGameMega                               int  `json:"most_kills_game_mega"`
				AssistsKitMegaMegaArmorer                       int  `json:"assists_kit_mega_mega_armorer"`
				AssistsMega                                     int  `json:"assists_mega"`
				VoidKillsKitMegaMegaArmorer                     int  `json:"void_kills_kit_mega_mega_armorer"`
				VoidKillsMega                                   int  `json:"void_kills_mega"`
				ArrowsHitMegaDoubles                            int  `json:"arrows_hit_mega_doubles"`
				LongestBowShotMegaDoubles                       int  `json:"longest_bow_shot_mega_doubles"`
				GamesKitMegaMegaArmorer                         int  `json:"games_kit_mega_mega_armorer"`
				GamesMega                                       int  `json:"games_mega"`
				TeamMarksmanship                                int  `json:"team_marksmanship"`
				GamesKitSupportingTeamPharaoh                   int  `json:"games_kit_supporting_team_pharaoh"`
				HeadsDivineKitSupportingTeamPharaoh             int  `json:"heads_divine_kit_supporting_team_pharaoh"`
				HeadsKitSupportingTeamPharaoh                   int  `json:"heads_kit_supporting_team_pharaoh"`
				HeadsTastyKitSupportingTeamPharaoh              int  `json:"heads_tasty_kit_supporting_team_pharaoh"`
				VoidKillsKitSupportingTeamPharaoh               int  `json:"void_kills_kit_supporting_team_pharaoh"`
				ArrowsHitKitSupportingTeamPharaoh               int  `json:"arrows_hit_kit_supporting_team_pharaoh"`
				ArrowsShotKitSupportingTeamPharaoh              int  `json:"arrows_shot_kit_supporting_team_pharaoh"`
				LongestBowShotKitSupportingTeamPharaoh          int  `json:"longest_bow_shot_kit_supporting_team_pharaoh"`
				KitSupportingTeamPharaohInventoryAutoEquipArmor bool `json:"kit_supporting_team_pharaoh_inventory_auto_equip_armor"`
				KitSupportingTeamPharaohInventory               struct {
					BEACON0            string `json:"BEACON:0"`
					LEATHERCHESTPLATE0 string `json:"LEATHER_CHESTPLATE:0"`
					GOLDBLOCK0         string `json:"GOLD_BLOCK:0"`
					LEATHERLEGGINGS0   string `json:"LEATHER_LEGGINGS:0"`
					GOLDBOOTS0         string `json:"GOLD_BOOTS:0"`
					DIAMONDBLOCK0      string `json:"DIAMOND_BLOCK:0"`
					GOLDHELMET0        string `json:"GOLD_HELMET:0"`
					EMERALDBLOCK0      string `json:"EMERALD_BLOCK:0"`
				} `json:"kit_supporting_team_pharaoh_inventory"`
				FastestWinKitSupportingTeamPharaoh     int  `json:"fastest_win_kit_supporting_team_pharaoh"`
				KillstreakKitSupportingTeamPharaoh     int  `json:"killstreak_kit_supporting_team_pharaoh"`
				WinsKitSupportingTeamPharaoh           int  `json:"wins_kit_supporting_team_pharaoh"`
				AssistsKitSupportingTeamPharaoh        int  `json:"assists_kit_supporting_team_pharaoh"`
				FallKillsKitMegaMegaArmorer            int  `json:"fall_kills_kit_mega_mega_armorer"`
				FallKillsMegaDoubles                   int  `json:"fall_kills_mega_doubles"`
				AssistsMegaDoubles                     int  `json:"assists_mega_doubles"`
				ChestsOpenedKitBasicSoloEcologist      int  `json:"chests_opened_kit_basic_solo_ecologist"`
				AssistsKitBasicSoloArmorsmith          int  `json:"assists_kit_basic_solo_armorsmith"`
				SoloBulldozer                          int  `json:"solo_bulldozer"`
				HeadsDecentKitSupportingTeamPharaoh    int  `json:"heads_decent_kit_supporting_team_pharaoh"`
				HeadsSucculentKitSupportingTeamPharaoh int  `json:"heads_succulent_kit_supporting_team_pharaoh"`
				BowKillsKitSupportingTeamPharaoh       int  `json:"bow_kills_kit_supporting_team_pharaoh"`
				MobKillsKitSupportingTeamPharaoh       int  `json:"mob_kills_kit_supporting_team_pharaoh"`
				FallKillsKitSupportingTeamPharaoh      int  `json:"fall_kills_kit_supporting_team_pharaoh"`
				HeadsMehKitSupportingTeamPharaoh       int  `json:"heads_meh_kit_supporting_team_pharaoh"`
				HeadsEwwKitSupportingTeamPharaoh       int  `json:"heads_eww_kit_supporting_team_pharaoh"`
				HeadsSaltyKitSupportingTeamPharaoh     int  `json:"heads_salty_kit_supporting_team_pharaoh"`
				HeadsYuckyKitSupportingTeamPharaoh     int  `json:"heads_yucky_kit_supporting_team_pharaoh"`
				AssistsKitAdvancedSoloFarmer           int  `json:"assists_kit_advanced_solo_farmer"`
				DeathsKitAdvancedSoloFarmer            int  `json:"deaths_kit_advanced_solo_farmer"`
				FastestWinKitAdvancedSoloFarmer        int  `json:"fastest_win_kit_advanced_solo_farmer"`
				GamesKitAdvancedSoloFarmer             int  `json:"games_kit_advanced_solo_farmer"`
				SurvivedPlayersKitAdvancedSoloFarmer   int  `json:"survived_players_kit_advanced_solo_farmer"`
				TimePlayedKitAdvancedSoloFarmer        int  `json:"time_played_kit_advanced_solo_farmer"`
				WinsKitAdvancedSoloFarmer              int  `json:"wins_kit_advanced_solo_farmer"`
				ChestsOpenedKitAdvancedSoloFarmer      int  `json:"chests_opened_kit_advanced_solo_farmer"`
				LossesKitAdvancedSoloFarmer            int  `json:"losses_kit_advanced_solo_farmer"`
				AssistsKitBasicSoloFisherman           int  `json:"assists_kit_basic_solo_fisherman"`
				LongestBowKillKitBasicSoloFisherman    int  `json:"longest_bow_kill_kit_basic_solo_fisherman"`
				MeleeKillsKitBasicSoloFisherman        int  `json:"melee_kills_kit_basic_solo_fisherman"`
				FallKillsKitAdvancedSoloArmorer        int  `json:"fall_kills_kit_advanced_solo_armorer"`
				FallKillsTeam                          int  `json:"fall_kills_team"`
				FastestWinKitAdvancedSoloArmorer       int  `json:"fastest_win_kit_advanced_solo_armorer"`
				KillstreakKitAdvancedSoloArmorer       int  `json:"killstreak_kit_advanced_solo_armorer"`
				WinsKitAdvancedSoloArmorer             int  `json:"wins_kit_advanced_solo_armorer"`
				BowKillsKitAdvancedSoloArmorer         int  `json:"bow_kills_kit_advanced_solo_armorer"`
				TeamFrost                              int  `json:"team_frost"`
				MobKillsTeam                           int  `json:"mob_kills_team"`
				HeadsEwwKitAdvancedSoloArmorer         int  `json:"heads_eww_kit_advanced_solo_armorer"`
				HeadsYuckyKitAdvancedSoloArmorer       int  `json:"heads_yucky_kit_advanced_solo_armorer"`
				FreeEventKeySkywarsEasterBoxes20212    bool `json:"free_event_key_skywars_easter_boxes_2021_2"`
			} `json:"SkyWars"`
			Bedwars struct {
				Packages                                          []string `json:"packages"`
				FirstJoin7                                        bool     `json:"first_join_7"`
				Experience                                        int      `json:"Experience"`
				BedwarsBoxes                                      int      `json:"bedwars_boxes"`
				GamesPlayedBedwars1                               int      `json:"games_played_bedwars_1"`
				Winstreak                                         int      `json:"winstreak"`
				EightOneBedsLostBedwars                           int      `json:"eight_one_beds_lost_bedwars"`
				FinalDeathsBedwars                                int      `json:"final_deaths_bedwars"`
				GoldResourcesCollectedBedwars                     int      `json:"gold_resources_collected_bedwars"`
				VoidDeathsBedwars                                 int      `json:"void_deaths_bedwars"`
				EightOneFinalDeathsBedwars                        int      `json:"eight_one_final_deaths_bedwars"`
				EightOneDeathsBedwars                             int      `json:"eight_one_deaths_bedwars"`
				EightOneVoidDeathsBedwars                         int      `json:"eight_one_void_deaths_bedwars"`
				VoidKillsBedwars                                  int      `json:"void_kills_bedwars"`
				EightOneGamesPlayedBedwars                        int      `json:"eight_one_games_played_bedwars"`
				DiamondResourcesCollectedBedwars                  int      `json:"diamond_resources_collected_bedwars"`
				DeathsBedwars                                     int      `json:"deaths_bedwars"`
				EightOneEntityAttackFinalDeathsBedwars            int      `json:"eight_one_entity_attack_final_deaths_bedwars"`
				EntityAttackFinalDeathsBedwars                    int      `json:"entity_attack_final_deaths_bedwars"`
				ResourcesCollectedBedwars                         int      `json:"resources_collected_bedwars"`
				EightOneLossesBedwars                             int      `json:"eight_one_losses_bedwars"`
				Coins                                             int      `json:"coins"`
				GamesPlayedBedwars                                int      `json:"games_played_bedwars"`
				EightOneIronResourcesCollectedBedwars             int      `json:"eight_one_iron_resources_collected_bedwars"`
				EightOneItemsPurchasedBedwars                     int      `json:"eight_one__items_purchased_bedwars"`
				BedsLostBedwars                                   int      `json:"beds_lost_bedwars"`
				KillsBedwars                                      int      `json:"kills_bedwars"`
				EntityAttackKillsBedwars                          int      `json:"entity_attack_kills_bedwars"`
				EightOneDiamondResourcesCollectedBedwars          int      `json:"eight_one_diamond_resources_collected_bedwars"`
				EightOneEntityAttackKillsBedwars                  int      `json:"eight_one_entity_attack_kills_bedwars"`
				EightOneVoidKillsBedwars                          int      `json:"eight_one_void_kills_bedwars"`
				EightOneKillsBedwars                              int      `json:"eight_one_kills_bedwars"`
				LossesBedwars                                     int      `json:"losses_bedwars"`
				EightOneResourcesCollectedBedwars                 int      `json:"eight_one_resources_collected_bedwars"`
				IronResourcesCollectedBedwars                     int      `json:"iron_resources_collected_bedwars"`
				EightOneGoldResourcesCollectedBedwars             int      `json:"eight_one_gold_resources_collected_bedwars"`
				ItemsPurchasedBedwars                             int      `json:"_items_purchased_bedwars"`
				EightOneBedsBrokenBedwars                         int      `json:"eight_one_beds_broken_bedwars"`
				EmeraldResourcesCollectedBedwars                  int      `json:"emerald_resources_collected_bedwars"`
				EightOneEmeraldResourcesCollectedBedwars          int      `json:"eight_one_emerald_resources_collected_bedwars"`
				VoidFinalDeathsBedwars                            int      `json:"void_final_deaths_bedwars"`
				EightOneVoidFinalDeathsBedwars                    int      `json:"eight_one_void_final_deaths_bedwars"`
				BedsBrokenBedwars                                 int      `json:"beds_broken_bedwars"`
				FourFourIronResourcesCollectedBedwars             int      `json:"four_four_iron_resources_collected_bedwars"`
				FourFourLossesBedwars                             int      `json:"four_four_losses_bedwars"`
				FourFourGamesPlayedBedwars                        int      `json:"four_four_games_played_bedwars"`
				EntityAttackDeathsBedwars                         int      `json:"entity_attack_deaths_bedwars"`
				FourFourResourcesCollectedBedwars                 int      `json:"four_four_resources_collected_bedwars"`
				FourFourVoidDeathsBedwars                         int      `json:"four_four_void_deaths_bedwars"`
				FourFourDeathsBedwars                             int      `json:"four_four_deaths_bedwars"`
				FourFourGoldResourcesCollectedBedwars             int      `json:"four_four_gold_resources_collected_bedwars"`
				FourFourEntityAttackDeathsBedwars                 int      `json:"four_four_entity_attack_deaths_bedwars"`
				EightTwoEmeraldResourcesCollectedBedwars          int      `json:"eight_two_emerald_resources_collected_bedwars"`
				EightTwoResourcesCollectedBedwars                 int      `json:"eight_two_resources_collected_bedwars"`
				EightTwoDiamondResourcesCollectedBedwars          int      `json:"eight_two_diamond_resources_collected_bedwars"`
				EightTwoKillsBedwars                              int      `json:"eight_two_kills_bedwars"`
				EightTwoEntityAttackKillsBedwars                  int      `json:"eight_two_entity_attack_kills_bedwars"`
				EightTwoIronResourcesCollectedBedwars             int      `json:"eight_two_iron_resources_collected_bedwars"`
				EightTwoFinalDeathsBedwars                        int      `json:"eight_two_final_deaths_bedwars"`
				EightTwoVoidFinalDeathsBedwars                    int      `json:"eight_two_void_final_deaths_bedwars"`
				EightTwoDeathsBedwars                             int      `json:"eight_two_deaths_bedwars"`
				EightTwoGamesPlayedBedwars                        int      `json:"eight_two_games_played_bedwars"`
				EightTwoItemsPurchasedBedwars                     int      `json:"eight_two_items_purchased_bedwars"`
				EightTwoBedsLostBedwars                           int      `json:"eight_two_beds_lost_bedwars"`
				EightTwoEntityAttackDeathsBedwars                 int      `json:"eight_two_entity_attack_deaths_bedwars"`
				EightTwoGoldResourcesCollectedBedwars             int      `json:"eight_two_gold_resources_collected_bedwars"`
				EightTwoVoidFinalKillsBedwars                     int      `json:"eight_two_void_final_kills_bedwars"`
				FinalKillsBedwars                                 int      `json:"final_kills_bedwars"`
				VoidFinalKillsBedwars                             int      `json:"void_final_kills_bedwars"`
				EightTwoLossesBedwars                             int      `json:"eight_two_losses_bedwars"`
				EightTwoFinalKillsBedwars                         int      `json:"eight_two_final_kills_bedwars"`
				EightTwoVoidKillsBedwars                          int      `json:"eight_two_void_kills_bedwars"`
				EightOneWinstreak                                 int      `json:"eight_one_winstreak"`
				EightOneEntityAttackDeathsBedwars                 int      `json:"eight_one_entity_attack_deaths_bedwars"`
				EightOneEntityAttackFinalKillsBedwars             int      `json:"eight_one_entity_attack_final_kills_bedwars"`
				EightOneFinalKillsBedwars                         int      `json:"eight_one_final_kills_bedwars"`
				EntityAttackFinalKillsBedwars                     int      `json:"entity_attack_final_kills_bedwars"`
				EightTwoWinstreak                                 int      `json:"eight_two_winstreak"`
				EightTwoEntityAttackFinalDeathsBedwars            int      `json:"eight_two_entity_attack_final_deaths_bedwars"`
				EightTwoVoidDeathsBedwars                         int      `json:"eight_two_void_deaths_bedwars"`
				EightTwoEntityAttackFinalKillsBedwars             int      `json:"eight_two_entity_attack_final_kills_bedwars"`
				BedwarsOpenedCommons                              int      `json:"Bedwars_openedCommons"`
				BedwarsOpenedChests                               int      `json:"Bedwars_openedChests"`
				ChestHistoryNew                                   []string `json:"chest_history_new"`
				BedwarsOpenedRares                                int      `json:"Bedwars_openedRares"`
				BedwarsOpenedEpics                                int      `json:"Bedwars_openedEpics"`
				ShopSort                                          string   `json:"shop_sort"`
				ActiveProjectileTrail                             string   `json:"activeProjectileTrail"`
				ActiveKillEffect                                  string   `json:"activeKillEffect"`
				ActiveSprays                                      string   `json:"activeSprays"`
				ActiveIslandTopper                                string   `json:"activeIslandTopper"`
				ShopSortEnableOwnedFirst                          bool     `json:"shop_sort_enable_owned_first"`
				ActiveGlyph                                       string   `json:"activeGlyph"`
				ActiveNPCSkin                                     string   `json:"activeNPCSkin"`
				ActiveDeathCry                                    string   `json:"activeDeathCry"`
				UnderstandsResourceBank                           bool     `json:"understands_resource_bank"`
				UnderstandsStreaks                                bool     `json:"understands_streaks"`
				CastleItemsPurchasedBedwars                       int      `json:"castle__items_purchased_bedwars"`
				CastleBedsLostBedwars                             int      `json:"castle_beds_lost_bedwars"`
				CastleDeathsBedwars                               int      `json:"castle_deaths_bedwars"`
				CastleDiamondResourcesCollectedBedwars            int      `json:"castle_diamond_resources_collected_bedwars"`
				CastleEmeraldResourcesCollectedBedwars            int      `json:"castle_emerald_resources_collected_bedwars"`
				CastleEntityAttackDeathsBedwars                   int      `json:"castle_entity_attack_deaths_bedwars"`
				CastleEntityAttackFinalDeathsBedwars              int      `json:"castle_entity_attack_final_deaths_bedwars"`
				CastleEntityAttackKillsBedwars                    int      `json:"castle_entity_attack_kills_bedwars"`
				CastleFinalDeathsBedwars                          int      `json:"castle_final_deaths_bedwars"`
				CastleGamesPlayedBedwars                          int      `json:"castle_games_played_bedwars"`
				CastleGoldResourcesCollectedBedwars               int      `json:"castle_gold_resources_collected_bedwars"`
				CastleIronResourcesCollectedBedwars               int      `json:"castle_iron_resources_collected_bedwars"`
				CastleKillsBedwars                                int      `json:"castle_kills_bedwars"`
				CastleLossesBedwars                               int      `json:"castle_losses_bedwars"`
				CastlePermanentItemsPurchasedBedwars              int      `json:"castle_permanent _items_purchased_bedwars"`
				CastleResourcesCollectedBedwars                   int      `json:"castle_resources_collected_bedwars"`
				CastleVoidDeathsBedwars                           int      `json:"castle_void_deaths_bedwars"`
				CastleVoidKillsBedwars                            int      `json:"castle_void_kills_bedwars"`
				CastleWinstreak                                   int      `json:"castle_winstreak"`
				EightTwoBedsBrokenBedwars                         int      `json:"eight_two_beds_broken_bedwars"`
				Favourites2                                       string   `json:"favourites_2"`
				EightTwoWinsBedwars                               int      `json:"eight_two_wins_bedwars"`
				WinsBedwars                                       int      `json:"wins_bedwars"`
				EightTwoProjectileDeathsBedwars                   int      `json:"eight_two_projectile_deaths_bedwars"`
				ProjectileDeathsBedwars                           int      `json:"projectile_deaths_bedwars"`
				EightTwoFallKillsBedwars                          int      `json:"eight_two_fall_kills_bedwars"`
				FallKillsBedwars                                  int      `json:"fall_kills_bedwars"`
				EightTwoProjectileKillsBedwars                    int      `json:"eight_two_projectile_kills_bedwars"`
				ProjectileKillsBedwars                            int      `json:"projectile_kills_bedwars"`
				EightTwoFallDeathsBedwars                         int      `json:"eight_two_fall_deaths_bedwars"`
				FallDeathsBedwars                                 int      `json:"fall_deaths_bedwars"`
				FourFourWinstreak                                 int      `json:"four_four_winstreak"`
				FourFourBedsBrokenBedwars                         int      `json:"four_four_beds_broken_bedwars"`
				FourFourDiamondResourcesCollectedBedwars          int      `json:"four_four_diamond_resources_collected_bedwars"`
				FourFourEmeraldResourcesCollectedBedwars          int      `json:"four_four_emerald_resources_collected_bedwars"`
				FourFourEntityAttackFinalKillsBedwars             int      `json:"four_four_entity_attack_final_kills_bedwars"`
				FourFourEntityAttackKillsBedwars                  int      `json:"four_four_entity_attack_kills_bedwars"`
				FourFourFallKillsBedwars                          int      `json:"four_four_fall_kills_bedwars"`
				FourFourFinalKillsBedwars                         int      `json:"four_four_final_kills_bedwars"`
				FourFourItemsPurchasedBedwars                     int      `json:"four_four_items_purchased_bedwars"`
				FourFourKillsBedwars                              int      `json:"four_four_kills_bedwars"`
				FourFourProjectileDeathsBedwars                   int      `json:"four_four_projectile_deaths_bedwars"`
				FourFourVoidFinalKillsBedwars                     int      `json:"four_four_void_final_kills_bedwars"`
				FourFourVoidKillsBedwars                          int      `json:"four_four_void_kills_bedwars"`
				FourFourWinsBedwars                               int      `json:"four_four_wins_bedwars"`
				FourFourBedsLostBedwars                           int      `json:"four_four_beds_lost_bedwars"`
				FourFourFallDeathsBedwars                         int      `json:"four_four_fall_deaths_bedwars"`
				FourFourFinalDeathsBedwars                        int      `json:"four_four_final_deaths_bedwars"`
				FourFourVoidFinalDeathsBedwars                    int      `json:"four_four_void_final_deaths_bedwars"`
				EntityExplosionDeathsBedwars                      int      `json:"entity_explosion_deaths_bedwars"`
				FourFourEntityAttackFinalDeathsBedwars            int      `json:"four_four_entity_attack_final_deaths_bedwars"`
				FourFourEntityExplosionDeathsBedwars              int      `json:"four_four_entity_explosion_deaths_bedwars"`
				EightOneVoidFinalKillsBedwars                     int      `json:"eight_one_void_final_kills_bedwars"`
				EightTwoEntityExplosionDeathsBedwars              int      `json:"eight_two_entity_explosion_deaths_bedwars"`
				EightTwoEntityExplosionKillsBedwars               int      `json:"eight_two_entity_explosion_kills_bedwars"`
				EntityExplosionKillsBedwars                       int      `json:"entity_explosion_kills_bedwars"`
				FourThreeWinstreak                                int      `json:"four_three_winstreak"`
				FourThreeItemsPurchasedBedwars                    int      `json:"four_three__items_purchased_bedwars"`
				FourThreeDeathsBedwars                            int      `json:"four_three_deaths_bedwars"`
				FourThreeEntityAttackDeathsBedwars                int      `json:"four_three_entity_attack_deaths_bedwars"`
				FourThreeGamesPlayedBedwars                       int      `json:"four_three_games_played_bedwars"`
				FourThreeGoldResourcesCollectedBedwars            int      `json:"four_three_gold_resources_collected_bedwars"`
				FourThreeIronResourcesCollectedBedwars            int      `json:"four_three_iron_resources_collected_bedwars"`
				FourThreeKillsBedwars                             int      `json:"four_three_kills_bedwars"`
				FourThreeResourcesCollectedBedwars                int      `json:"four_three_resources_collected_bedwars"`
				FourThreeVoidDeathsBedwars                        int      `json:"four_three_void_deaths_bedwars"`
				FourThreeVoidKillsBedwars                         int      `json:"four_three_void_kills_bedwars"`
				FourThreeWinsBedwars                              int      `json:"four_three_wins_bedwars"`
				EightTwoFallFinalKillsBedwars                     int      `json:"eight_two_fall_final_kills_bedwars"`
				FallFinalKillsBedwars                             int      `json:"fall_final_kills_bedwars"`
				EightTwoFallFinalDeathsBedwars                    int      `json:"eight_two_fall_final_deaths_bedwars"`
				FallFinalDeathsBedwars                            int      `json:"fall_final_deaths_bedwars"`
				EightTwoArmedWinstreak                            int      `json:"eight_two_armed_winstreak"`
				EightTwoArmedBedsLostBedwars                      int      `json:"eight_two_armed_beds_lost_bedwars"`
				EightTwoArmedDeathsBedwars                        int      `json:"eight_two_armed_deaths_bedwars"`
				EightTwoArmedFinalDeathsBedwars                   int      `json:"eight_two_armed_final_deaths_bedwars"`
				EightTwoArmedGamesPlayedBedwars                   int      `json:"eight_two_armed_games_played_bedwars"`
				EightTwoArmedGoldResourcesCollectedBedwars        int      `json:"eight_two_armed_gold_resources_collected_bedwars"`
				EightTwoArmedIronResourcesCollectedBedwars        int      `json:"eight_two_armed_iron_resources_collected_bedwars"`
				EightTwoArmedItemsPurchasedBedwars                int      `json:"eight_two_armed_items_purchased_bedwars"`
				EightTwoArmedLossesBedwars                        int      `json:"eight_two_armed_losses_bedwars"`
				EightTwoArmedProjectileDeathsBedwars              int      `json:"eight_two_armed_projectile_deaths_bedwars"`
				EightTwoArmedResourcesCollectedBedwars            int      `json:"eight_two_armed_resources_collected_bedwars"`
				EightTwoArmedVoidFinalDeathsBedwars               int      `json:"eight_two_armed_void_final_deaths_bedwars"`
				EightTwoArmedDiamondResourcesCollectedBedwars     int      `json:"eight_two_armed_diamond_resources_collected_bedwars"`
				EightTwoArmedEntityAttackFinalKillsBedwars        int      `json:"eight_two_armed_entity_attack_final_kills_bedwars"`
				EightTwoArmedFinalKillsBedwars                    int      `json:"eight_two_armed_final_kills_bedwars"`
				EightTwoArmedKillsBedwars                         int      `json:"eight_two_armed_kills_bedwars"`
				EightTwoArmedPermanentItemsPurchasedBedwars       int      `json:"eight_two_armed_permanent _items_purchased_bedwars"`
				EightTwoArmedProjectileFinalDeathsBedwars         int      `json:"eight_two_armed_projectile_final_deaths_bedwars"`
				EightTwoArmedProjectileKillsBedwars               int      `json:"eight_two_armed_projectile_kills_bedwars"`
				EightTwoArmedVoidDeathsBedwars                    int      `json:"eight_two_armed_void_deaths_bedwars"`
				EightTwoArmedVoidKillsBedwars                     int      `json:"eight_two_armed_void_kills_bedwars"`
				FourThreeBedsBrokenBedwars                        int      `json:"four_three_beds_broken_bedwars"`
				FourThreeEmeraldResourcesCollectedBedwars         int      `json:"four_three_emerald_resources_collected_bedwars"`
				FourThreeFinalKillsBedwars                        int      `json:"four_three_final_kills_bedwars"`
				FourThreeVoidFinalKillsBedwars                    int      `json:"four_three_void_final_kills_bedwars"`
				FourFourArmedWinstreak                            int      `json:"four_four_armed_winstreak"`
				FourThreeDiamondResourcesCollectedBedwars         int      `json:"four_three_diamond_resources_collected_bedwars"`
				FourThreeEntityAttackFinalKillsBedwars            int      `json:"four_three_entity_attack_final_kills_bedwars"`
				FourFourArmedBedsLostBedwars                      int      `json:"four_four_armed_beds_lost_bedwars"`
				FourFourArmedDeathsBedwars                        int      `json:"four_four_armed_deaths_bedwars"`
				FourFourArmedDiamondResourcesCollectedBedwars     int      `json:"four_four_armed_diamond_resources_collected_bedwars"`
				FourFourArmedFallFinalDeathsBedwars               int      `json:"four_four_armed_fall_final_deaths_bedwars"`
				FourFourArmedFinalDeathsBedwars                   int      `json:"four_four_armed_final_deaths_bedwars"`
				FourFourArmedGamesPlayedBedwars                   int      `json:"four_four_armed_games_played_bedwars"`
				FourFourArmedGoldResourcesCollectedBedwars        int      `json:"four_four_armed_gold_resources_collected_bedwars"`
				FourFourArmedIronResourcesCollectedBedwars        int      `json:"four_four_armed_iron_resources_collected_bedwars"`
				FourFourArmedItemsPurchasedBedwars                int      `json:"four_four_armed_items_purchased_bedwars"`
				FourFourArmedKillsBedwars                         int      `json:"four_four_armed_kills_bedwars"`
				FourFourArmedLossesBedwars                        int      `json:"four_four_armed_losses_bedwars"`
				FourFourArmedProjectileDeathsBedwars              int      `json:"four_four_armed_projectile_deaths_bedwars"`
				FourFourArmedProjectileKillsBedwars               int      `json:"four_four_armed_projectile_kills_bedwars"`
				FourFourArmedResourcesCollectedBedwars            int      `json:"four_four_armed_resources_collected_bedwars"`
				FourThreeBedsLostBedwars                          int      `json:"four_three_beds_lost_bedwars"`
				FourThreeFinalDeathsBedwars                       int      `json:"four_three_final_deaths_bedwars"`
				FourThreeLossesBedwars                            int      `json:"four_three_losses_bedwars"`
				FourThreeVoidFinalDeathsBedwars                   int      `json:"four_three_void_final_deaths_bedwars"`
				ActiveKillMessages                                string   `json:"activeKillMessages"`
				ActiveVictoryDance                                string   `json:"activeVictoryDance"`
				FourThreeEntityAttackFinalDeathsBedwars           int      `json:"four_three_entity_attack_final_deaths_bedwars"`
				EightTwoArmedEmeraldResourcesCollectedBedwars     int      `json:"eight_two_armed_emerald_resources_collected_bedwars"`
				FourThreeEntityExplosionKillsBedwars              int      `json:"four_three_entity_explosion_kills_bedwars"`
				FourThreeEntityAttackKillsBedwars                 int      `json:"four_three_entity_attack_kills_bedwars"`
				EightTwoEntityExplosionFinalKillsBedwars          int      `json:"eight_two_entity_explosion_final_kills_bedwars"`
				EntityExplosionFinalKillsBedwars                  int      `json:"entity_explosion_final_kills_bedwars"`
				BedwarsHalloweenBoxes                             int      `json:"bedwars_halloween_boxes"`
				FreeEventKeyBedwarsHalloweenBoxes2019             bool     `json:"free_event_key_bedwars_halloween_boxes_2019"`
				BedwarsOpenedLegendaries                          int      `json:"Bedwars_openedLegendaries"`
				EightTwoFireDeathsBedwars                         int      `json:"eight_two_fire_deaths_bedwars"`
				FireDeathsBedwars                                 int      `json:"fire_deaths_bedwars"`
				EightTwoFireTickDeathsBedwars                     int      `json:"eight_two_fire_tick_deaths_bedwars"`
				FireTickDeathsBedwars                             int      `json:"fire_tick_deaths_bedwars"`
				EightTwoRushWinstreak                             int      `json:"eight_two_rush_winstreak"`
				EightTwoRushItemsPurchasedBedwars                 int      `json:"eight_two_rush__items_purchased_bedwars"`
				EightTwoRushBedsLostBedwars                       int      `json:"eight_two_rush_beds_lost_bedwars"`
				EightTwoRushDeathsBedwars                         int      `json:"eight_two_rush_deaths_bedwars"`
				EightTwoRushEntityAttackDeathsBedwars             int      `json:"eight_two_rush_entity_attack_deaths_bedwars"`
				EightTwoRushEntityAttackFinalDeathsBedwars        int      `json:"eight_two_rush_entity_attack_final_deaths_bedwars"`
				EightTwoRushFinalDeathsBedwars                    int      `json:"eight_two_rush_final_deaths_bedwars"`
				EightTwoRushGamesPlayedBedwars                    int      `json:"eight_two_rush_games_played_bedwars"`
				EightTwoRushGoldResourcesCollectedBedwars         int      `json:"eight_two_rush_gold_resources_collected_bedwars"`
				EightTwoRushIronResourcesCollectedBedwars         int      `json:"eight_two_rush_iron_resources_collected_bedwars"`
				EightTwoRushLossesBedwars                         int      `json:"eight_two_rush_losses_bedwars"`
				EightTwoRushResourcesCollectedBedwars             int      `json:"eight_two_rush_resources_collected_bedwars"`
				EightTwoRushEmeraldResourcesCollectedBedwars      int      `json:"eight_two_rush_emerald_resources_collected_bedwars"`
				EightTwoRushEntityAttackKillsBedwars              int      `json:"eight_two_rush_entity_attack_kills_bedwars"`
				EightTwoRushKillsBedwars                          int      `json:"eight_two_rush_kills_bedwars"`
				EightTwoRushVoidKillsBedwars                      int      `json:"eight_two_rush_void_kills_bedwars"`
				EightTwoRushFinalKillsBedwars                     int      `json:"eight_two_rush_final_kills_bedwars"`
				EightTwoRushVoidDeathsBedwars                     int      `json:"eight_two_rush_void_deaths_bedwars"`
				EightTwoRushVoidFinalDeathsBedwars                int      `json:"eight_two_rush_void_final_deaths_bedwars"`
				EightTwoRushVoidFinalKillsBedwars                 int      `json:"eight_two_rush_void_final_kills_bedwars"`
				ActiveBedDestroy                                  string   `json:"activeBedDestroy"`
				SelectedUltimate                                  string   `json:"selected_ultimate"`
				EightTwoUltimateWinstreak                         int      `json:"eight_two_ultimate_winstreak"`
				EightTwoUltimateBedsBrokenBedwars                 int      `json:"eight_two_ultimate_beds_broken_bedwars"`
				EightTwoUltimateBedsLostBedwars                   int      `json:"eight_two_ultimate_beds_lost_bedwars"`
				EightTwoUltimateDiamondResourcesCollectedBedwars  int      `json:"eight_two_ultimate_diamond_resources_collected_bedwars"`
				EightTwoUltimateEmeraldResourcesCollectedBedwars  int      `json:"eight_two_ultimate_emerald_resources_collected_bedwars"`
				EightTwoUltimateGamesPlayedBedwars                int      `json:"eight_two_ultimate_games_played_bedwars"`
				EightTwoUltimateGoldResourcesCollectedBedwars     int      `json:"eight_two_ultimate_gold_resources_collected_bedwars"`
				EightTwoUltimateIronResourcesCollectedBedwars     int      `json:"eight_two_ultimate_iron_resources_collected_bedwars"`
				EightTwoUltimateItemsPurchasedBedwars             int      `json:"eight_two_ultimate_items_purchased_bedwars"`
				EightTwoUltimateResourcesCollectedBedwars         int      `json:"eight_two_ultimate_resources_collected_bedwars"`
				EightTwoUltimateWinsBedwars                       int      `json:"eight_two_ultimate_wins_bedwars"`
				EightTwoUltimateDeathsBedwars                     int      `json:"eight_two_ultimate_deaths_bedwars"`
				EightTwoUltimateEntityAttackFinalDeathsBedwars    int      `json:"eight_two_ultimate_entity_attack_final_deaths_bedwars"`
				EightTwoUltimateEntityAttackKillsBedwars          int      `json:"eight_two_ultimate_entity_attack_kills_bedwars"`
				EightTwoUltimateFinalDeathsBedwars                int      `json:"eight_two_ultimate_final_deaths_bedwars"`
				EightTwoUltimateKillsBedwars                      int      `json:"eight_two_ultimate_kills_bedwars"`
				EightTwoUltimateLossesBedwars                     int      `json:"eight_two_ultimate_losses_bedwars"`
				EightTwoUltimateVoidDeathsBedwars                 int      `json:"eight_two_ultimate_void_deaths_bedwars"`
				EightTwoUltimateVoidKillsBedwars                  int      `json:"eight_two_ultimate_void_kills_bedwars"`
				EightTwoUltimateVoidFinalDeathsBedwars            int      `json:"eight_two_ultimate_void_final_deaths_bedwars"`
				EightTwoUltimateEntityAttackDeathsBedwars         int      `json:"eight_two_ultimate_entity_attack_deaths_bedwars"`
				CastleWinsBedwars                                 int      `json:"castle_wins_bedwars"`
				CastleFallDeathsBedwars                           int      `json:"castle_fall_deaths_bedwars"`
				FourThreeFallFinalDeathsBedwars                   int      `json:"four_three_fall_final_deaths_bedwars"`
				FourFourVoidlessWinstreak                         int      `json:"four_four_voidless_winstreak"`
				FourFourVoidlessBedsLostBedwars                   int      `json:"four_four_voidless_beds_lost_bedwars"`
				FourFourVoidlessDeathsBedwars                     int      `json:"four_four_voidless_deaths_bedwars"`
				FourFourVoidlessEntityAttackDeathsBedwars         int      `json:"four_four_voidless_entity_attack_deaths_bedwars"`
				FourFourVoidlessEntityAttackFinalDeathsBedwars    int      `json:"four_four_voidless_entity_attack_final_deaths_bedwars"`
				FourFourVoidlessFinalDeathsBedwars                int      `json:"four_four_voidless_final_deaths_bedwars"`
				FourFourVoidlessGamesPlayedBedwars                int      `json:"four_four_voidless_games_played_bedwars"`
				FourFourVoidlessGoldResourcesCollectedBedwars     int      `json:"four_four_voidless_gold_resources_collected_bedwars"`
				FourFourVoidlessIronResourcesCollectedBedwars     int      `json:"four_four_voidless_iron_resources_collected_bedwars"`
				FourFourVoidlessItemsPurchasedBedwars             int      `json:"four_four_voidless_items_purchased_bedwars"`
				FourFourVoidlessLossesBedwars                     int      `json:"four_four_voidless_losses_bedwars"`
				FourFourVoidlessResourcesCollectedBedwars         int      `json:"four_four_voidless_resources_collected_bedwars"`
				FourFourFallFinalKillsBedwars                     int      `json:"four_four_fall_final_kills_bedwars"`
				FourThreeFireTickDeathsBedwars                    int      `json:"four_three_fire_tick_deaths_bedwars"`
				FourFourUltimateWinstreak                         int      `json:"four_four_ultimate_winstreak"`
				FourFourUltimateBedsLostBedwars                   int      `json:"four_four_ultimate_beds_lost_bedwars"`
				FourFourUltimateDeathsBedwars                     int      `json:"four_four_ultimate_deaths_bedwars"`
				FourFourUltimateEmeraldResourcesCollectedBedwars  int      `json:"four_four_ultimate_emerald_resources_collected_bedwars"`
				FourFourUltimateEntityAttackFinalDeathsBedwars    int      `json:"four_four_ultimate_entity_attack_final_deaths_bedwars"`
				FourFourUltimateEntityAttackKillsBedwars          int      `json:"four_four_ultimate_entity_attack_kills_bedwars"`
				FourFourUltimateFinalDeathsBedwars                int      `json:"four_four_ultimate_final_deaths_bedwars"`
				FourFourUltimateGamesPlayedBedwars                int      `json:"four_four_ultimate_games_played_bedwars"`
				FourFourUltimateGoldResourcesCollectedBedwars     int      `json:"four_four_ultimate_gold_resources_collected_bedwars"`
				FourFourUltimateIronResourcesCollectedBedwars     int      `json:"four_four_ultimate_iron_resources_collected_bedwars"`
				FourFourUltimateItemsPurchasedBedwars             int      `json:"four_four_ultimate_items_purchased_bedwars"`
				FourFourUltimateKillsBedwars                      int      `json:"four_four_ultimate_kills_bedwars"`
				FourFourUltimateLossesBedwars                     int      `json:"four_four_ultimate_losses_bedwars"`
				FourFourUltimateResourcesCollectedBedwars         int      `json:"four_four_ultimate_resources_collected_bedwars"`
				FourFourUltimateVoidDeathsBedwars                 int      `json:"four_four_ultimate_void_deaths_bedwars"`
				FourFourUltimateEntityAttackDeathsBedwars         int      `json:"four_four_ultimate_entity_attack_deaths_bedwars"`
				FourFourUltimateWinsBedwars                       int      `json:"four_four_ultimate_wins_bedwars"`
				FourFourUltimateVoidFinalDeathsBedwars            int      `json:"four_four_ultimate_void_final_deaths_bedwars"`
				FourFourUltimateVoidKillsBedwars                  int      `json:"four_four_ultimate_void_kills_bedwars"`
				EightTwoUltimateEntityAttackFinalKillsBedwars     int      `json:"eight_two_ultimate_entity_attack_final_kills_bedwars"`
				EightTwoUltimateFinalKillsBedwars                 int      `json:"eight_two_ultimate_final_kills_bedwars"`
				EightTwoUltimateVoidFinalKillsBedwars             int      `json:"eight_two_ultimate_void_final_kills_bedwars"`
				FourFourUltimateBedsBrokenBedwars                 int      `json:"four_four_ultimate_beds_broken_bedwars"`
				FourFourUltimateDiamondResourcesCollectedBedwars  int      `json:"four_four_ultimate_diamond_resources_collected_bedwars"`
				FourFourUltimateEntityAttackFinalKillsBedwars     int      `json:"four_four_ultimate_entity_attack_final_kills_bedwars"`
				FourFourUltimateFinalKillsBedwars                 int      `json:"four_four_ultimate_final_kills_bedwars"`
				FourFourUltimateVoidFinalKillsBedwars             int      `json:"four_four_ultimate_void_final_kills_bedwars"`
				FourFourUltimateEntityExplosionDeathsBedwars      int      `json:"four_four_ultimate_entity_explosion_deaths_bedwars"`
				FourThreeFallKillsBedwars                         int      `json:"four_three_fall_kills_bedwars"`
				FavoriteSlots                                     string   `json:"favorite_slots"`
				BedwarsChristmasBoxes                             int      `json:"bedwars_christmas_boxes"`
				FreeEventKeyBedwarsChristmasBoxes2019             bool     `json:"free_event_key_bedwars_christmas_boxes_2019"`
				BedwarsLunarBoxes                                 int      `json:"bedwars_lunar_boxes"`
				FourThreeFallFinalKillsBedwars                    int      `json:"four_three_fall_final_kills_bedwars"`
				EightTwoMagicKillsBedwars                         int      `json:"eight_two_magic_kills_bedwars"`
				MagicKillsBedwars                                 int      `json:"magic_kills_bedwars"`
				EightTwoMagicDeathsBedwars                        int      `json:"eight_two_magic_deaths_bedwars"`
				MagicDeathsBedwars                                int      `json:"magic_deaths_bedwars"`
				EightTwoMagicFinalKillsBedwars                    int      `json:"eight_two_magic_final_kills_bedwars"`
				MagicFinalKillsBedwars                            int      `json:"magic_final_kills_bedwars"`
				TwoFourWinstreak                                  int      `json:"two_four_winstreak"`
				TwoFourBedsLostBedwars                            int      `json:"two_four_beds_lost_bedwars"`
				TwoFourDeathsBedwars                              int      `json:"two_four_deaths_bedwars"`
				TwoFourEntityAttackDeathsBedwars                  int      `json:"two_four_entity_attack_deaths_bedwars"`
				TwoFourEntityAttackKillsBedwars                   int      `json:"two_four_entity_attack_kills_bedwars"`
				TwoFourFinalDeathsBedwars                         int      `json:"two_four_final_deaths_bedwars"`
				TwoFourGamesPlayedBedwars                         int      `json:"two_four_games_played_bedwars"`
				TwoFourGoldResourcesCollectedBedwars              int      `json:"two_four_gold_resources_collected_bedwars"`
				TwoFourIronResourcesCollectedBedwars              int      `json:"two_four_iron_resources_collected_bedwars"`
				TwoFourItemsPurchasedBedwars                      int      `json:"two_four_items_purchased_bedwars"`
				TwoFourKillsBedwars                               int      `json:"two_four_kills_bedwars"`
				TwoFourLossesBedwars                              int      `json:"two_four_losses_bedwars"`
				TwoFourPermanentItemsPurchasedBedwars             int      `json:"two_four_permanent _items_purchased_bedwars"`
				TwoFourResourcesCollectedBedwars                  int      `json:"two_four_resources_collected_bedwars"`
				TwoFourVoidDeathsBedwars                          int      `json:"two_four_void_deaths_bedwars"`
				TwoFourVoidFinalDeathsBedwars                     int      `json:"two_four_void_final_deaths_bedwars"`
				TwoFourVoidKillsBedwars                           int      `json:"two_four_void_kills_bedwars"`
				BedwarsEasterBoxes                                int      `json:"bedwars_easter_boxes"`
				FreeEventKeyBedwarsEasterBoxes2020                bool     `json:"free_event_key_bedwars_easter_boxes_2020"`
				ActiveWoodType                                    string   `json:"activeWoodType"`
				FireTickFinalDeathsBedwars                        int      `json:"fire_tick_final_deaths_bedwars"`
				FourFourFireTickFinalDeathsBedwars                int      `json:"four_four_fire_tick_final_deaths_bedwars"`
				TourneyBedwars4S1Winstreak2                       int      `json:"tourney_bedwars4s_1_winstreak2"`
				TourneyBedwars4S1ItemsPurchasedBedwars            int      `json:"tourney_bedwars4s_1__items_purchased_bedwars"`
				TourneyBedwars4S1BedsLostBedwars                  int      `json:"tourney_bedwars4s_1_beds_lost_bedwars"`
				TourneyBedwars4S1DiamondResourcesCollectedBedwars int      `json:"tourney_bedwars4s_1_diamond_resources_collected_bedwars"`
				TourneyBedwars4S1EntityAttackFinalDeathsBedwars   int      `json:"tourney_bedwars4s_1_entity_attack_final_deaths_bedwars"`
				TourneyBedwars4S1FinalDeathsBedwars               int      `json:"tourney_bedwars4s_1_final_deaths_bedwars"`
				TourneyBedwars4S1GamesPlayedBedwars               int      `json:"tourney_bedwars4s_1_games_played_bedwars"`
				TourneyBedwars4S1GoldResourcesCollectedBedwars    int      `json:"tourney_bedwars4s_1_gold_resources_collected_bedwars"`
				TourneyBedwars4S1IronResourcesCollectedBedwars    int      `json:"tourney_bedwars4s_1_iron_resources_collected_bedwars"`
				TourneyBedwars4S1KillsBedwars                     int      `json:"tourney_bedwars4s_1_kills_bedwars"`
				TourneyBedwars4S1LossesBedwars                    int      `json:"tourney_bedwars4s_1_losses_bedwars"`
				TourneyBedwars4S1ResourcesCollectedBedwars        int      `json:"tourney_bedwars4s_1_resources_collected_bedwars"`
				TourneyBedwars4S1VoidKillsBedwars                 int      `json:"tourney_bedwars4s_1_void_kills_bedwars"`
				TourneyBedwars4S1DeathsBedwars                    int      `json:"tourney_bedwars4s_1_deaths_bedwars"`
				TourneyBedwars4S1EntityAttackDeathsBedwars        int      `json:"tourney_bedwars4s_1_entity_attack_deaths_bedwars"`
				TourneyBedwars4S1EntityAttackFinalKillsBedwars    int      `json:"tourney_bedwars4s_1_entity_attack_final_kills_bedwars"`
				TourneyBedwars4S1FinalKillsBedwars                int      `json:"tourney_bedwars4s_1_final_kills_bedwars"`
				TourneyBedwars4S1VoidDeathsBedwars                int      `json:"tourney_bedwars4s_1_void_deaths_bedwars"`
				TourneyBedwars4S1VoidFinalDeathsBedwars           int      `json:"tourney_bedwars4s_1_void_final_deaths_bedwars"`
				TourneyBedwars4S1EntityAttackKillsBedwars         int      `json:"tourney_bedwars4s_1_entity_attack_kills_bedwars"`
				TourneyBedwars4S1PermanentItemsPurchasedBedwars   int      `json:"tourney_bedwars4s_1_permanent _items_purchased_bedwars"`
				TourneyBedwars4S1FallKillsBedwars                 int      `json:"tourney_bedwars4s_1_fall_kills_bedwars"`
				EightTwoMagicFinalDeathsBedwars                   int      `json:"eight_two_magic_final_deaths_bedwars"`
				MagicFinalDeathsBedwars                           int      `json:"magic_final_deaths_bedwars"`
				TourneyBedwars4S1WinsBedwars                      int      `json:"tourney_bedwars4s_1_wins_bedwars"`
				TourneyBedwars4S1EmeraldResourcesCollectedBedwars int      `json:"tourney_bedwars4s_1_emerald_resources_collected_bedwars"`
				EightTwoPermanentItemsPurchasedBedwars            int      `json:"eight_two_permanent_items_purchased_bedwars"`
				PermanentItemsPurchasedBedwars                    int      `json:"permanent_items_purchased_bedwars"`
				FreeEventKeyBedwarsHalloweenBoxes2020             bool     `json:"free_event_key_bedwars_halloween_boxes_2020"`
				EightOneMagicDeathsBedwars                        int      `json:"eight_one_magic_deaths_bedwars"`
				EightOnePermanentItemsPurchasedBedwars            int      `json:"eight_one_permanent_items_purchased_bedwars"`
				EightOneWinsBedwars                               int      `json:"eight_one_wins_bedwars"`
				FourFourUltimatePermanentItemsPurchasedBedwars    int      `json:"four_four_ultimate_permanent_items_purchased_bedwars"`
				EightTwoUltimatePermanentItemsPurchasedBedwars    int      `json:"eight_two_ultimate_permanent_items_purchased_bedwars"`
				EightTwoUltimateMagicDeathsBedwars                int      `json:"eight_two_ultimate_magic_deaths_bedwars"`
				EightOneFallFinalKillsBedwars                     int      `json:"eight_one_fall_final_kills_bedwars"`
				EightOneFallKillsBedwars                          int      `json:"eight_one_fall_kills_bedwars"`
				FourThreePermanentItemsPurchasedBedwars           int      `json:"four_three_permanent_items_purchased_bedwars"`
				FourThreeEntityExplosionDeathsBedwars             int      `json:"four_three_entity_explosion_deaths_bedwars"`
				FourThreeFallDeathsBedwars                        int      `json:"four_three_fall_deaths_bedwars"`
				FourThreeMagicFinalKillsBedwars                   int      `json:"four_three_magic_final_kills_bedwars"`
				Practice                                          struct {
					Selected string `json:"selected"`
					Records  struct {
						BridgingDistance30ElevationNONEAngleSTRAIGHT int `json:"bridging_distance_30:elevation_NONE:angle_STRAIGHT:"`
					} `json:"records"`
					Bridging struct {
						SuccessfulAttempts int `json:"successful_attempts"`
						FailedAttempts     int `json:"failed_attempts"`
						BlocksPlaced       int `json:"blocks_placed"`
					} `json:"bridging"`
					Mlg struct {
						FailedAttempts     int `json:"failed_attempts"`
						SuccessfulAttempts int `json:"successful_attempts"`
					} `json:"mlg"`
					FireballJumping struct {
						BlocksPlaced       int `json:"blocks_placed"`
						FailedAttempts     int `json:"failed_attempts"`
						SuccessfulAttempts int `json:"successful_attempts"`
					} `json:"fireball_jumping"`
				} `json:"practice"`
				FourFourPermanentItemsPurchasedBedwars int `json:"four_four_permanent_items_purchased_bedwars"`
				FourFourMagicFinalDeathsBedwars        int `json:"four_four_magic_final_deaths_bedwars"`
				FourFourMagicDeathsBedwars             int `json:"four_four_magic_deaths_bedwars"`
			} `json:"Bedwars"`
			TrueCombat struct {
				Packages                         []string `json:"packages"`
				WinStreak                        int      `json:"win_streak"`
				Games                            int      `json:"games"`
				CrazywallsDeathsTeamChaos        int      `json:"crazywalls_deaths_team_chaos"`
				Deaths                           int      `json:"deaths"`
				Coins                            int      `json:"coins"`
				Losses                           int      `json:"losses"`
				CrazywallsGamesTeamChaos         int      `json:"crazywalls_games_team_chaos"`
				CrazywallsLossesTeamChaos        int      `json:"crazywalls_losses_team_chaos"`
				SurvivedPlayers                  int      `json:"survived_players"`
				ArrowsShot                       int      `json:"arrows_shot"`
				ArrowsHit                        int      `json:"arrows_hit"`
				KillsWeeklyA                     int      `json:"kills_weekly_a"`
				CrazywallsLossesSoloChaos        int      `json:"crazywalls_losses_solo_chaos"`
				ItemsEnchanted                   int      `json:"items_enchanted"`
				CrazywallsGamesSoloChaos         int      `json:"crazywalls_games_solo_chaos"`
				Kills                            int      `json:"kills"`
				CrazywallsDeathsSoloChaos        int      `json:"crazywalls_deaths_solo_chaos"`
				CrazywallsKillsMonthlyBSoloChaos int      `json:"crazywalls_kills_monthly_b_solo_chaos"`
				SkullsGathered                   int      `json:"skulls_gathered"`
				CrazywallsKillsSoloChaos         int      `json:"crazywalls_kills_solo_chaos"`
				CrazywallsKillsWeeklyASoloChaos  int      `json:"crazywalls_kills_weekly_a_solo_chaos"`
				KillsMonthlyB                    int      `json:"kills_monthly_b"`
				GoldenSkulls                     int      `json:"golden_skulls"`
				GiantZombie                      int      `json:"giant_zombie"`
				SoloBountyHunter                 int      `json:"solo_bounty_hunter"`
			} `json:"TrueCombat"`
			SkyClash struct {
				CardPacks          int `json:"card_packs"`
				ActiveClass        int `json:"active_class"`
				SwordsmanInventory struct {
					SKULLITEM3     string `json:"SKULL_ITEM:3"`
					POTION9        string `json:"POTION:9"`
					SKULLITEM2     string `json:"SKULL_ITEM:2"`
					SKULLITEM1     string `json:"SKULL_ITEM:1"`
					COMPASS0       string `json:"COMPASS:0"`
					STONESWORD0    string `json:"STONE_SWORD:0"`
					LEATHERHELMET0 string `json:"LEATHER_HELMET:0"`
				} `json:"swordsman_inventory"`
				Packages                []string `json:"packages"`
				Killstreak              int      `json:"killstreak"`
				Playstreak              int      `json:"playstreak"`
				PlayStreak              int      `json:"play_streak"`
				WinStreak               int      `json:"win_streak"`
				DeathsKitSwordsman      int      `json:"deaths_kit_swordsman"`
				LossesPerkRampage       int      `json:"losses_perk_rampage"`
				Losses                  int      `json:"losses"`
				LossesTeamWar           int      `json:"losses_team_war"`
				DeathsPerkRampage       int      `json:"deaths_perk_rampage"`
				GamesPlayed             int      `json:"games_played"`
				LossesPerkHitAndRun     int      `json:"losses_perk_hit_and_run"`
				Quits                   int      `json:"quits"`
				DeathsPerkHitAndRun     int      `json:"deaths_perk_hit_and_run"`
				Deaths                  int      `json:"deaths"`
				GamesPlayedKitSwordsman int      `json:"games_played_kit_swordsman"`
				Coins                   int      `json:"coins"`
				DeathsPerkRegeneration  int      `json:"deaths_perk_regeneration"`
				DeathsTeamWar           int      `json:"deaths_team_war"`
				LossesPerkRegeneration  int      `json:"losses_perk_regeneration"`
				GuardianInventory       struct {
					POTION5              string `json:"POTION:5"`
					IRONBOOTS0           string `json:"IRON_BOOTS:0"`
					CHAINMAILCHESTPLATE0 string `json:"CHAINMAIL_CHESTPLATE:0"`
					SKULLITEM3           string `json:"SKULL_ITEM:3"`
					SKULLITEM2           string `json:"SKULL_ITEM:2"`
					SKULLITEM1           string `json:"SKULL_ITEM:1"`
					COMPASS0             string `json:"COMPASS:0"`
				} `json:"guardian_inventory"`
				DeathsSolo              int `json:"deaths_solo"`
				LossesSolo              int `json:"losses_solo"`
				DeathsPerkHeartyStart   int `json:"deaths_perk_hearty_start"`
				GamesPlayedKitGuardian  int `json:"games_played_kit_guardian"`
				LossesPerkHeartyStart   int `json:"losses_perk_hearty_start"`
				LossesPerkResistant     int `json:"losses_perk_resistant"`
				DeathsKitGuardian       int `json:"deaths_kit_guardian"`
				DeathsPerkResistant     int `json:"deaths_perk_resistant"`
				LossesDoubles           int `json:"losses_doubles"`
				MobsKilledKitSwordsman  int `json:"mobs_killed_kit_swordsman"`
				MobsKilled              int `json:"mobs_killed"`
				DeathsDoubles           int `json:"deaths_doubles"`
				MobsSpawnedKitSwordsman int `json:"mobs_spawned_kit_swordsman"`
				MobsSpawned             int `json:"mobs_spawned"`
			} `json:"SkyClash"`
			BuildBattle struct {
				WinsSoloNormal int  `json:"wins_solo_normal"`
				Wins           int  `json:"wins"`
				Music          bool `json:"music"`
				VotesBats      int  `json:"votes_Bats"`
				Coins          int  `json:"coins"`
				GamesPlayed    int  `json:"games_played"`
				MonthlyCoinsA  int  `json:"monthly_coins_a"`
				Score          int  `json:"score"`
				SoloMostPoints int  `json:"solo_most_points"`
				TotalVotes     int  `json:"total_votes"`
				WeeklyCoinsB   int  `json:"weekly_coins_b"`
			} `json:"BuildBattle"`
			Battleground struct {
				PaladinSpec               string   `json:"paladin_spec"`
				ShamanSpec                string   `json:"shaman_spec"`
				MageSpec                  string   `json:"mage_spec"`
				ChosenClass               string   `json:"chosen_class"`
				WarriorSpec               string   `json:"warrior_spec"`
				SelectedMount             string   `json:"selected_mount"`
				Packages                  []string `json:"packages"`
				PlayStreak                int      `json:"play_streak"`
				DamagePyromancer          int      `json:"damage_pyromancer"`
				HealMage                  int      `json:"heal_mage"`
				DamageMage                int      `json:"damage_mage"`
				HealPyromancer            int      `json:"heal_pyromancer"`
				FlagConquerTeam           int      `json:"flag_conquer_team"`
				PyromancerPlays           int      `json:"pyromancer_plays"`
				Deaths                    int      `json:"deaths"`
				PowerupsCollected         int      `json:"powerups_collected"`
				DamagePrevented           int      `json:"damage_prevented"`
				Damage                    int      `json:"damage"`
				DamagePreventedPyromancer int      `json:"damage_prevented_pyromancer"`
				Assists                   int      `json:"assists"`
				MagePlays                 int      `json:"mage_plays"`
				Heal                      int      `json:"heal"`
				DamagePreventedMage       int      `json:"damage_prevented_mage"`
				Coins                     int      `json:"coins"`
				DamageTaken               int      `json:"damage_taken"`
				BrokenInventory           int      `json:"broken_inventory"`
				RepairedRare              int      `json:"repaired_rare"`
				Repaired                  int      `json:"repaired"`
				WeaponInventory           []struct {
					Spec struct {
						Spec        int `json:"spec"`
						PlayerClass int `json:"playerClass"`
					} `json:"spec"`
					Ability      int    `json:"ability"`
					AbilityBoost int    `json:"abilityBoost"`
					Damage       int    `json:"damage"`
					Energy       int    `json:"energy"`
					Chance       int    `json:"chance"`
					Multiplier   int    `json:"multiplier"`
					Health       int    `json:"health"`
					Cooldown     int    `json:"cooldown"`
					Movement     int    `json:"movement"`
					Material     string `json:"material"`
					ID           int64  `json:"id"`
					Category     string `json:"category"`
					Crafted      bool   `json:"crafted"`
					UpgradeMax   int    `json:"upgradeMax"`
					UpgradeTimes int    `json:"upgradeTimes"`
				} `json:"weapon_inventory"`
				RepairedCommon        int   `json:"repaired_common"`
				CurrentWeapon         int64 `json:"current_weapon"`
				SalvagedWeapons       int   `json:"salvaged_weapons"`
				SalvagedDustReward    int   `json:"salvaged_dust_reward"`
				MagicDust             int   `json:"magic_dust"`
				SalvagedWeaponsCommon int   `json:"salvaged_weapons_common"`
				SalvagedShardsReward  int   `json:"salvaged_shards_reward"`
				WinStreak             int   `json:"win_streak"`
				WinsCapturetheflagA   int   `json:"wins_capturetheflag_a"`
				WinsCapturetheflag    int   `json:"wins_capturetheflag"`
				KillsCapturetheflag   int   `json:"kills_capturetheflag"`
				WinsRed               int   `json:"wins_red"`
				Wins                  int   `json:"wins"`
				Kills                 int   `json:"kills"`
				WinsCapturetheflagRed int   `json:"wins_capturetheflag_red"`
				WinsPyromancer        int   `json:"wins_pyromancer"`
				WinsMage              int   `json:"wins_mage"`
				MageEnergy            int   `json:"mage_energy"`
				Reroll                int   `json:"reroll"`
				RerollRare            int   `json:"reroll_rare"`
				SalvagedWeaponsRare   int   `json:"salvaged_weapons_rare"`
				WinsCapturetheflagBlu int   `json:"wins_capturetheflag_blu"`
				WinsBlu               int   `json:"wins_blu"`
				MageCooldown          int   `json:"mage_cooldown"`
				Hints                 bool  `json:"hints"`
				WinsDominationBlu     int   `json:"wins_domination_blu"`
				DomPointDefends       int   `json:"dom_point_defends"`
				WinsDomination        int   `json:"wins_domination"`
				DomPointCaptures      int   `json:"dom_point_captures"`
				TotalDominationScore  int   `json:"total_domination_score"`
				WinsDominationA       int   `json:"wins_domination_a"`
				Crafted               int   `json:"crafted"`
				CraftedRare           int   `json:"crafted_rare"`
				MageHealth            int   `json:"mage_health"`
				MageCritchance        int   `json:"mage_critchance"`
				MageCritmultiplier    int   `json:"mage_critmultiplier"`
				RewardInventory       int   `json:"reward_inventory"`
				Losses                int   `json:"losses"`
				LossesPyromancer      int   `json:"losses_pyromancer"`
				LossesMage            int   `json:"losses_mage"`
				KillsDomination       int   `json:"kills_domination"`
			} `json:"Battleground"`
			Mcgo struct {
				Coins                            int      `json:"coins"`
				GameWinsDeathmatch               int      `json:"game_wins_deathmatch"`
				GameWins                         int      `json:"game_wins"`
				GrenadeKills                     int      `json:"grenadeKills"`
				HeadshotKills                    int      `json:"headshot_kills"`
				PocketChange                     int      `json:"pocket_change"`
				Packages                         []string `json:"packages"`
				BombsPlanted                     int      `json:"bombs_planted"`
				KillsDeathmatch                  int      `json:"kills_deathmatch"`
				BombsDefused                     int      `json:"bombs_defused"`
				Kills                            int      `json:"kills"`
				LastTourneyAd                    int64    `json:"lastTourneyAd"`
				Deaths                           int      `json:"deaths"`
				BombsPlantedTourneyMcgoDefusal0  int      `json:"bombs_planted_tourney_mcgo_defusal_0"`
				DeathsTourneyMcgoDefusal0        int      `json:"deaths_tourney_mcgo_defusal_0"`
				GamePlaysTourneyMcgoDefusal0     int      `json:"game_plays_tourney_mcgo_defusal_0"`
				GameWinsTourneyMcgoDefusal0      int      `json:"game_wins_tourney_mcgo_defusal_0"`
				RoundWinsTourneyMcgoDefusal0     int      `json:"round_wins_tourney_mcgo_defusal_0"`
				ShotsFiredTourneyMcgoDefusal0    int      `json:"shots_fired_tourney_mcgo_defusal_0"`
				CriminalKillsTourneyMcgoDefusal0 int      `json:"criminal_kills_tourney_mcgo_defusal_0"`
				HeadshotKillsTourneyMcgoDefusal0 int      `json:"headshot_kills_tourney_mcgo_defusal_0"`
				KillsTourneyMcgoDefusal0         int      `json:"kills_tourney_mcgo_defusal_0"`
				CopKillsTourneyMcgoDefusal0      int      `json:"cop_kills_tourney_mcgo_defusal_0"`
				GameWinsSandstorm                int      `json:"game_wins_sandstorm"`
			} `json:"MCGO"`
			SpeedUHC struct {
				Coins                       int  `json:"coins"`
				FirstJoinLobbyInt           int  `json:"firstJoinLobbyInt"`
				Killstreak                  int  `json:"killstreak"`
				WinStreak                   int  `json:"win_streak"`
				DeathsTeam                  int  `json:"deaths_team"`
				Deaths                      int  `json:"deaths"`
				DeathsKitBasicNormalDefault int  `json:"deaths_kit_basic_normal_default"`
				DeathsMasteryWildSpecialist int  `json:"deaths_mastery_wild_specialist"`
				Losses                      int  `json:"losses"`
				LossesMasteryWildSpecialist int  `json:"losses_mastery_wild_specialist"`
				BlocksBroken                int  `json:"blocks_broken"`
				LossesKitBasicNormalDefault int  `json:"losses_kit_basic_normal_default"`
				DeathsNormal                int  `json:"deaths_normal"`
				DeathsTeamNormal            int  `json:"deaths_team_normal"`
				LossesTeam                  int  `json:"losses_team"`
				LossesNormal                int  `json:"losses_normal"`
				LossesTeamNormal            int  `json:"losses_team_normal"`
				Quits                       int  `json:"quits"`
				Score                       int  `json:"score"`
				MovedOver                   bool `json:"movedOver"`
			} `json:"SpeedUHC"`
			SuperSmash struct {
				Coins            int `json:"coins"`
				LastLevelTHEBULK int `json:"lastLevel_THE_BULK"`
			} `json:"SuperSmash"`
			Walls3 struct {
				Coins   int `json:"coins"`
				Classes struct {
					Zombie struct {
						Unlocked            bool `json:"unlocked"`
						Checked4            bool `json:"checked4"`
						SkillLevelDChecked5 bool `json:"skill_level_dChecked5"`
						SkillLevelD         int  `json:"skill_level_d"`
						SkillLevelA         int  `json:"skill_level_a"`
						SkillLevelAChecked5 bool `json:"skill_level_aChecked5"`
					} `json:"zombie"`
					Herobrine struct {
						Unlocked            bool `json:"unlocked"`
						Checked4            bool `json:"checked4"`
						SkillLevelD         int  `json:"skill_level_d"`
						SkillLevelDChecked5 bool `json:"skill_level_dChecked5"`
						SkillLevelA         int  `json:"skill_level_a"`
						SkillLevelAChecked5 bool `json:"skill_level_aChecked5"`
					} `json:"herobrine"`
					Skeleton struct {
						Unlocked            bool `json:"unlocked"`
						Checked4            bool `json:"checked4"`
						SkillLevelDChecked5 bool `json:"skill_level_dChecked5"`
						SkillLevelD         int  `json:"skill_level_d"`
						SkillLevelAChecked5 bool `json:"skill_level_aChecked5"`
						SkillLevelA         int  `json:"skill_level_a"`
					} `json:"skeleton"`
					Enderman struct {
						Checked4            bool `json:"checked4"`
						Unlocked            bool `json:"unlocked"`
						SkillLevelD         int  `json:"skill_level_d"`
						SkillLevelDChecked5 bool `json:"skill_level_dChecked5"`
						SkillLevelA         int  `json:"skill_level_a"`
						SkillLevelAChecked5 bool `json:"skill_level_aChecked5"`
					} `json:"enderman"`
					Golem struct {
						Unlocked       bool `json:"unlocked"`
						EnderchestRows int  `json:"enderchest_rows"`
					} `json:"golem"`
				} `json:"classes"`
				Packages                             []string `json:"packages"`
				NewEchest                            int      `json:"new_echest"`
				ChosenClass                          string   `json:"chosen_class"`
				ArrowsFired                          int      `json:"arrows_fired"`
				ArrowsFiredStandard                  int      `json:"arrows_fired_standard"`
				ArrowsHit                            int      `json:"arrows_hit"`
				ArrowsHitStandard                    int      `json:"arrows_hit_standard"`
				BlocksBroken                         int      `json:"blocks_broken"`
				BlocksBrokenStandard                 int      `json:"blocks_broken_standard"`
				BlocksPlaced                         int      `json:"blocks_placed"`
				BlocksPlacedPreparation              int      `json:"blocks_placed_preparation"`
				BlocksPlacedPreparationStandard      int      `json:"blocks_placed_preparation_standard"`
				BlocksPlacedStandard                 int      `json:"blocks_placed_standard"`
				Deaths                               int      `json:"deaths"`
				DeathsStandard                       int      `json:"deaths_standard"`
				GamesPlayed                          int      `json:"games_played"`
				GamesPlayedStandard                  int      `json:"games_played_standard"`
				GolemArrowsFired                     int      `json:"golem_arrows_fired"`
				GolemArrowsFiredStandard             int      `json:"golem_arrows_fired_standard"`
				GolemArrowsHit                       int      `json:"golem_arrows_hit"`
				GolemArrowsHitStandard               int      `json:"golem_arrows_hit_standard"`
				GolemBlocksBroken                    int      `json:"golem_blocks_broken"`
				GolemBlocksBrokenStandard            int      `json:"golem_blocks_broken_standard"`
				GolemBlocksPlaced                    int      `json:"golem_blocks_placed"`
				GolemBlocksPlacedPreparation         int      `json:"golem_blocks_placed_preparation"`
				GolemBlocksPlacedPreparationStandard int      `json:"golem_blocks_placed_preparation_standard"`
				GolemBlocksPlacedStandard            int      `json:"golem_blocks_placed_standard"`
				GolemDeaths                          int      `json:"golem_deaths"`
				GolemDeathsStandard                  int      `json:"golem_deaths_standard"`
				GolemGamesPlayed                     int      `json:"golem_games_played"`
				GolemGamesPlayedStandard             int      `json:"golem_games_played_standard"`
				GolemIronOreBroken                   int      `json:"golem_iron_ore_broken"`
				GolemIronOreBrokenStandard           int      `json:"golem_iron_ore_broken_standard"`
				GolemMetersFallen                    int      `json:"golem_meters_fallen"`
				GolemMetersFallenStandard            int      `json:"golem_meters_fallen_standard"`
				GolemMetersWalked                    int      `json:"golem_meters_walked"`
				GolemMetersWalkedStandard            int      `json:"golem_meters_walked_standard"`
				GolemTimePlayed                      int      `json:"golem_time_played"`
				GolemTimePlayedStandard              int      `json:"golem_time_played_standard"`
				GolemTotalDeaths                     int      `json:"golem_total_deaths"`
				GolemTotalDeathsStandard             int      `json:"golem_total_deaths_standard"`
				GolemTreasuresFound                  int      `json:"golem_treasures_found"`
				GolemTreasuresFoundStandard          int      `json:"golem_treasures_found_standard"`
				GolemWins                            int      `json:"golem_wins"`
				GolemWinsStandard                    int      `json:"golem_wins_standard"`
				GolemWoodChopped                     int      `json:"golem_wood_chopped"`
				GolemWoodChoppedStandard             int      `json:"golem_wood_chopped_standard"`
				IronOreBroken                        int      `json:"iron_ore_broken"`
				IronOreBrokenStandard                int      `json:"iron_ore_broken_standard"`
				MetersFallen                         int      `json:"meters_fallen"`
				MetersFallenStandard                 int      `json:"meters_fallen_standard"`
				MetersWalked                         int      `json:"meters_walked"`
				MetersWalkedStandard                 int      `json:"meters_walked_standard"`
				TimePlayed                           int      `json:"time_played"`
				TimePlayedStandard                   int      `json:"time_played_standard"`
				TotalDeaths                          int      `json:"total_deaths"`
				TotalDeathsStandard                  int      `json:"total_deaths_standard"`
				TreasuresFound                       int      `json:"treasures_found"`
				TreasuresFoundStandard               int      `json:"treasures_found_standard"`
				Wins                                 int      `json:"wins"`
				WinsStandard                         int      `json:"wins_standard"`
				WoodChopped                          int      `json:"wood_chopped"`
				WoodChoppedStandard                  int      `json:"wood_chopped_standard"`
			} `json:"Walls3"`
			Arena struct {
				Coins int `json:"coins"`
				Keys  int `json:"keys"`
			} `json:"Arena"`
			GingerBread struct {
				Coins              int      `json:"coins"`
				Packages           []string `json:"packages"`
				FrameActive        string   `json:"frame_active"`
				JacketActive       string   `json:"jacket_active"`
				HelmetActive       string   `json:"helmet_active"`
				EngineActive       string   `json:"engine_active"`
				BoosterActive      string   `json:"booster_active"`
				SkinActive         string   `json:"skin_active"`
				ShoesActive        string   `json:"shoes_active"`
				PantsActive        string   `json:"pants_active"`
				Parts              string   `json:"parts"`
				Horn               string   `json:"horn"`
				BoxPickupsMonthlyA int      `json:"box_pickups_monthly_a"`
				BoxPickups         int      `json:"box_pickups"`
				CoinsPickedUp      int      `json:"coins_picked_up"`
				BoxPickupsCanyon   int      `json:"box_pickups_canyon"`
				CanyonPlays        int      `json:"canyon_plays"`
				BoxPickupsWeeklyB  int      `json:"box_pickups_weekly_b"`
				LastTourneyAd      int64    `json:"lastTourneyAd"`
			} `json:"GingerBread"`
			Uhc struct {
				Coins               int      `json:"coins"`
				ClearupAchievement  bool     `json:"clearup_achievement"`
				SavedStats          bool     `json:"saved_stats"`
				Packages            []string `json:"packages"`
				PerkApprenticeLineA int      `json:"perk_apprentice_line_a"`
				PerkApprenticeLineC int      `json:"perk_apprentice_line_c"`
				PerkApprenticeLineB int      `json:"perk_apprentice_line_b"`
				DeathsSolo          int      `json:"deaths_solo"`
			} `json:"UHC"`
			Duels struct {
				UhcRookieTitlePrestige            int      `json:"uhc_rookie_title_prestige"`
				TournamentRookieTitlePrestige     int      `json:"tournament_rookie_title_prestige"`
				BlitzRookieTitlePrestige          int      `json:"blitz_rookie_title_prestige"`
				NoDebuffRookieTitlePrestige       int      `json:"no_debuff_rookie_title_prestige"`
				ClassicRookieTitlePrestige        int      `json:"classic_rookie_title_prestige"`
				SkywarsRookieTitlePrestige        int      `json:"skywars_rookie_title_prestige"`
				TntGamesRookieTitlePrestige       int      `json:"tnt_games_rookie_title_prestige"`
				SumoRookieTitlePrestige           int      `json:"sumo_rookie_title_prestige"`
				AllModesRookieTitlePrestige       int      `json:"all_modes_rookie_title_prestige"`
				BowRookieTitlePrestige            int      `json:"bow_rookie_title_prestige"`
				ComboRookieTitlePrestige          int      `json:"combo_rookie_title_prestige"`
				MegaWallsRookieTitlePrestige      int      `json:"mega_walls_rookie_title_prestige"`
				OpRookieTitlePrestige             int      `json:"op_rookie_title_prestige"`
				Selected1New                      string   `json:"selected_1_new"`
				Selected2New                      string   `json:"selected_2_new"`
				Packages                          []string `json:"packages"`
				BridgeRookieTitlePrestige         int      `json:"bridge_rookie_title_prestige"`
				DuelsRecentlyPlayed2              string   `json:"duels_recently_played2"`
				ShowLbOption                      string   `json:"show_lb_option"`
				ChatEnabled                       string   `json:"chat_enabled"`
				GamesPlayedDuels                  int      `json:"games_played_duels"`
				MapsWonOn                         []string `json:"maps_won_on"`
				CurrentWinstreak                  int      `json:"current_winstreak"`
				CurrentWinstreakModeClassicDuel   int      `json:"current_winstreak_mode_classic_duel"`
				CurrentClassicWinstreak           int      `json:"current_classic_winstreak"`
				ClassicDuelRoundsPlayed           int      `json:"classic_duel_rounds_played"`
				ClassicDuelHealthRegenerated      int      `json:"classic_duel_health_regenerated"`
				ClassicDuelLosses                 int      `json:"classic_duel_losses"`
				MeleeSwings                       int      `json:"melee_swings"`
				Losses                            int      `json:"losses"`
				Deaths                            int      `json:"deaths"`
				ClassicDuelDeaths                 int      `json:"classic_duel_deaths"`
				HealthRegenerated                 int      `json:"health_regenerated"`
				RoundsPlayed                      int      `json:"rounds_played"`
				ClassicDuelMeleeSwings            int      `json:"classic_duel_melee_swings"`
				ClassicDuelMeleeHits              int      `json:"classic_duel_melee_hits"`
				MeleeHits                         int      `json:"melee_hits"`
				ClassicDuelDamageDealt            int      `json:"classic_duel_damage_dealt"`
				DamageDealt                       int      `json:"damage_dealt"`
				BowShots                          int      `json:"bow_shots"`
				BowHits                           int      `json:"bow_hits"`
				ClassicDuelBowHits                int      `json:"classic_duel_bow_hits"`
				ClassicDuelBowShots               int      `json:"classic_duel_bow_shots"`
				DuelsChests                       int      `json:"duels_chests"`
				BlocksPlaced                      int      `json:"blocks_placed"`
				BridgeDuelBlocksPlaced            int      `json:"bridge_duel_blocks_placed"`
				BridgeDuelBowHits                 int      `json:"bridge_duel_bow_hits"`
				BridgeDuelBowShots                int      `json:"bridge_duel_bow_shots"`
				BridgeDuelDamageDealt             int      `json:"bridge_duel_damage_dealt"`
				BridgeDuelHealthRegenerated       int      `json:"bridge_duel_health_regenerated"`
				BridgeDuelMeleeHits               int      `json:"bridge_duel_melee_hits"`
				BridgeDuelMeleeSwings             int      `json:"bridge_duel_melee_swings"`
				BridgeDuelRoundsPlayed            int      `json:"bridge_duel_rounds_played"`
				CurrentWinstreakModeSumoDuel      int      `json:"current_winstreak_mode_sumo_duel"`
				CurrentSumoWinstreak              int      `json:"current_sumo_winstreak"`
				SumoDuelDeaths                    int      `json:"sumo_duel_deaths"`
				SumoDuelLosses                    int      `json:"sumo_duel_losses"`
				SumoDuelMeleeHits                 int      `json:"sumo_duel_melee_hits"`
				SumoDuelMeleeSwings               int      `json:"sumo_duel_melee_swings"`
				SumoDuelRoundsPlayed              int      `json:"sumo_duel_rounds_played"`
				BestOverallWinstreak              int      `json:"best_overall_winstreak"`
				BestSumoWinstreak                 int      `json:"best_sumo_winstreak"`
				BestWinstreakModeSumoDuel         int      `json:"best_winstreak_mode_sumo_duel"`
				Coins                             int      `json:"coins"`
				Kills                             int      `json:"kills"`
				SumoDuelKills                     int      `json:"sumo_duel_kills"`
				SumoDuelWins                      int      `json:"sumo_duel_wins"`
				Wins                              int      `json:"wins"`
				PingPreference                    int      `json:"pingPreference"`
				LeaderboardPageWinStreak          int      `json:"leaderboardPage_win_streak"`
				DuelsWinstreakBestSumoDuel        int      `json:"duels_winstreak_best_sumo_duel"`
				BridgeMapWins                     []string `json:"bridgeMapWins"`
				CurrentWinstreakModeBridgeDoubles int      `json:"current_winstreak_mode_bridge_doubles"`
				BestBridgeWinstreak               int      `json:"best_bridge_winstreak"`
				BestWinstreakModeBridgeDoubles    int      `json:"best_winstreak_mode_bridge_doubles"`
				CurrentBridgeWinstreak            int      `json:"current_bridge_winstreak"`
				BridgeDeaths                      int      `json:"bridge_deaths"`
				BridgeDoublesBlocksPlaced         int      `json:"bridge_doubles_blocks_placed"`
				BridgeDoublesBowShots             int      `json:"bridge_doubles_bow_shots"`
				BridgeDoublesBridgeDeaths         int      `json:"bridge_doubles_bridge_deaths"`
				BridgeDoublesBridgeKills          int      `json:"bridge_doubles_bridge_kills"`
				BridgeDoublesDamageDealt          int      `json:"bridge_doubles_damage_dealt"`
				BridgeDoublesGoals                int      `json:"bridge_doubles_goals"`
				BridgeDoublesHealthRegenerated    int      `json:"bridge_doubles_health_regenerated"`
				BridgeDoublesMeleeHits            int      `json:"bridge_doubles_melee_hits"`
				BridgeDoublesMeleeSwings          int      `json:"bridge_doubles_melee_swings"`
				BridgeDoublesRoundsPlayed         int      `json:"bridge_doubles_rounds_played"`
				BridgeDoublesWins                 int      `json:"bridge_doubles_wins"`
				BridgeKills                       int      `json:"bridge_kills"`
				Goals                             int      `json:"goals"`
				BridgeDoublesBowHits              int      `json:"bridge_doubles_bow_hits"`
				BridgeDoublesLosses               int      `json:"bridge_doubles_losses"`
				LeaderboardPageGoals              int      `json:"leaderboardPage_goals"`
				LeaderboardPageWins               int      `json:"leaderboardPage_wins"`
				BestClassicWinstreak              int      `json:"best_classic_winstreak"`
				BestWinstreakModeClassicDuel      int      `json:"best_winstreak_mode_classic_duel"`
				ClassicDuelKills                  int      `json:"classic_duel_kills"`
				ClassicDuelWins                   int      `json:"classic_duel_wins"`
				CurrentNoDebuffWinstreak          int      `json:"current_no_debuff_winstreak"`
				CurrentWinstreakModePotionDuel    int      `json:"current_winstreak_mode_potion_duel"`
				PotionDuelDamageDealt             int      `json:"potion_duel_damage_dealt"`
				PotionDuelDeaths                  int      `json:"potion_duel_deaths"`
				PotionDuelHealthRegenerated       int      `json:"potion_duel_health_regenerated"`
				PotionDuelLosses                  int      `json:"potion_duel_losses"`
				PotionDuelMeleeHits               int      `json:"potion_duel_melee_hits"`
				PotionDuelMeleeSwings             int      `json:"potion_duel_melee_swings"`
				PotionDuelRoundsPlayed            int      `json:"potion_duel_rounds_played"`
				CurrentWinstreakModeUhcDoubles    int      `json:"current_winstreak_mode_uhc_doubles"`
				CurrentUhcWinstreak               int      `json:"current_uhc_winstreak"`
				GoldenApplesEaten                 int      `json:"golden_apples_eaten"`
				UhcDoublesBlocksPlaced            int      `json:"uhc_doubles_blocks_placed"`
				UhcDoublesBowHits                 int      `json:"uhc_doubles_bow_hits"`
				UhcDoublesBowShots                int      `json:"uhc_doubles_bow_shots"`
				UhcDoublesDamageDealt             int      `json:"uhc_doubles_damage_dealt"`
				UhcDoublesDeaths                  int      `json:"uhc_doubles_deaths"`
				UhcDoublesGoldenApplesEaten       int      `json:"uhc_doubles_golden_apples_eaten"`
				UhcDoublesHealthRegenerated       int      `json:"uhc_doubles_health_regenerated"`
				UhcDoublesLosses                  int      `json:"uhc_doubles_losses"`
				UhcDoublesMeleeHits               int      `json:"uhc_doubles_melee_hits"`
				UhcDoublesMeleeSwings             int      `json:"uhc_doubles_melee_swings"`
				UhcDoublesRoundsPlayed            int      `json:"uhc_doubles_rounds_played"`
				BestWinstreakModeUhcDoubles       int      `json:"best_winstreak_mode_uhc_doubles"`
				BestUhcWinstreak                  int      `json:"best_uhc_winstreak"`
				UhcDoublesKills                   int      `json:"uhc_doubles_kills"`
				UhcDoublesWins                    int      `json:"uhc_doubles_wins"`
				UhcDuelBlocksPlaced               int      `json:"uhc_duel_blocks_placed"`
				UhcDuelDamageDealt                int      `json:"uhc_duel_damage_dealt"`
				UhcDuelHealthRegenerated          int      `json:"uhc_duel_health_regenerated"`
				UhcDuelMeleeHits                  int      `json:"uhc_duel_melee_hits"`
				UhcDuelMeleeSwings                int      `json:"uhc_duel_melee_swings"`
				UhcDuelRoundsPlayed               int      `json:"uhc_duel_rounds_played"`
				UhcDuelBowShots                   int      `json:"uhc_duel_bow_shots"`
				UhcDuelBowHits                    int      `json:"uhc_duel_bow_hits"`
				UhcDuelGoldenApplesEaten          int      `json:"uhc_duel_golden_apples_eaten"`
				Bridge2V2V2V2BlocksPlaced         int      `json:"bridge_2v2v2v2_blocks_placed"`
				Bridge2V2V2V2BowHits              int      `json:"bridge_2v2v2v2_bow_hits"`
				Bridge2V2V2V2BowShots             int      `json:"bridge_2v2v2v2_bow_shots"`
				Bridge2V2V2V2DamageDealt          int      `json:"bridge_2v2v2v2_damage_dealt"`
				Bridge2V2V2V2HealthRegenerated    int      `json:"bridge_2v2v2v2_health_regenerated"`
				Bridge2V2V2V2MeleeHits            int      `json:"bridge_2v2v2v2_melee_hits"`
				Bridge2V2V2V2MeleeSwings          int      `json:"bridge_2v2v2v2_melee_swings"`
				Bridge2V2V2V2RoundsPlayed         int      `json:"bridge_2v2v2v2_rounds_played"`
				BridgeFourBlocksPlaced            int      `json:"bridge_four_blocks_placed"`
				BridgeFourBowShots                int      `json:"bridge_four_bow_shots"`
				BridgeFourHealthRegenerated       int      `json:"bridge_four_health_regenerated"`
				BridgeFourMeleeSwings             int      `json:"bridge_four_melee_swings"`
				BridgeFourRoundsPlayed            int      `json:"bridge_four_rounds_played"`
				BowDuelBowHits                    int      `json:"bow_duel_bow_hits"`
				BowDuelBowShots                   int      `json:"bow_duel_bow_shots"`
				BowDuelDamageDealt                int      `json:"bow_duel_damage_dealt"`
				BowDuelHealthRegenerated          int      `json:"bow_duel_health_regenerated"`
				BowDuelRoundsPlayed               int      `json:"bow_duel_rounds_played"`
				MwDuelsClass                      string   `json:"mw_duels_class"`
				MwDuelDamageDealt                 int      `json:"mw_duel_damage_dealt"`
				MwDuelHealthRegenerated           int      `json:"mw_duel_health_regenerated"`
				MwDuelMeleeHits                   int      `json:"mw_duel_melee_hits"`
				MwDuelMeleeSwings                 int      `json:"mw_duel_melee_swings"`
				MwDuelRoundsPlayed                int      `json:"mw_duel_rounds_played"`
				ComboDuelGoldenApplesEaten        int      `json:"combo_duel_golden_apples_eaten"`
				ComboDuelHealthRegenerated        int      `json:"combo_duel_health_regenerated"`
				ComboDuelMeleeHits                int      `json:"combo_duel_melee_hits"`
				ComboDuelMeleeSwings              int      `json:"combo_duel_melee_swings"`
				ComboDuelRoundsPlayed             int      `json:"combo_duel_rounds_played"`
				BlitzDuelsKit                     string   `json:"blitz_duels_kit"`
				BlitzDuelRoundsPlayed             int      `json:"blitz_duel_rounds_played"`
				BlitzDuelBlocksPlaced             int      `json:"blitz_duel_blocks_placed"`
				BlitzDuelDamageDealt              int      `json:"blitz_duel_damage_dealt"`
				BlitzDuelHealthRegenerated        int      `json:"blitz_duel_health_regenerated"`
				BlitzDuelMeleeHits                int      `json:"blitz_duel_melee_hits"`
				BlitzDuelMeleeSwings              int      `json:"blitz_duel_melee_swings"`
				SwDuelsKitNew3                    string   `json:"sw_duels_kit_new3"`
				SwDuelBlocksPlaced                int      `json:"sw_duel_blocks_placed"`
				SwDuelDamageDealt                 int      `json:"sw_duel_damage_dealt"`
				SwDuelHealthRegenerated           int      `json:"sw_duel_health_regenerated"`
				SwDuelMeleeHits                   int      `json:"sw_duel_melee_hits"`
				SwDuelMeleeSwings                 int      `json:"sw_duel_melee_swings"`
				SwDuelRoundsPlayed                int      `json:"sw_duel_rounds_played"`
				DuelsWinstreakBestClassicDuel     int      `json:"duels_winstreak_best_classic_duel"`
				CurrentWinstreakModeSwDuel        int      `json:"current_winstreak_mode_sw_duel"`
				CurrentSkywarsWinstreak           int      `json:"current_skywars_winstreak"`
				SwDuelDeaths                      int      `json:"sw_duel_deaths"`
				SwDuelLosses                      int      `json:"sw_duel_losses"`
				BestWinstreakModeSwDuel           int      `json:"best_winstreak_mode_sw_duel"`
				BestSkywarsWinstreak              int      `json:"best_skywars_winstreak"`
				KitWins                           int      `json:"kit_wins"`
				ScoutKitWins                      int      `json:"scout_kit_wins"`
				SwDuelKitWins                     int      `json:"sw_duel_kit_wins"`
				SwDuelScoutKitWins                int      `json:"sw_duel_scout_kit_wins"`
				SwDuelWins                        int      `json:"sw_duel_wins"`
				SwDuelKills                       int      `json:"sw_duel_kills"`
				DuelsWinstreakBestSwDuel          int      `json:"duels_winstreak_best_sw_duel"`
				HealPotsUsed                      int      `json:"heal_pots_used"`
				PotionDuelHealPotsUsed            int      `json:"potion_duel_heal_pots_used"`
				CurrentWinstreakModeSwDoubles     int      `json:"current_winstreak_mode_sw_doubles"`
				SwDoublesBlocksPlaced             int      `json:"sw_doubles_blocks_placed"`
				SwDoublesDamageDealt              int      `json:"sw_doubles_damage_dealt"`
				SwDoublesDeaths                   int      `json:"sw_doubles_deaths"`
				SwDoublesHealthRegenerated        int      `json:"sw_doubles_health_regenerated"`
				SwDoublesLosses                   int      `json:"sw_doubles_losses"`
				SwDoublesMeleeHits                int      `json:"sw_doubles_melee_hits"`
				SwDoublesMeleeSwings              int      `json:"sw_doubles_melee_swings"`
				SwDoublesRoundsPlayed             int      `json:"sw_doubles_rounds_played"`
				BowspleefDuelBowShots             int      `json:"bowspleef_duel_bow_shots"`
				BowspleefDuelRoundsPlayed         int      `json:"bowspleef_duel_rounds_played"`
				CurrentWinstreakModeOpDoubles     int      `json:"current_winstreak_mode_op_doubles"`
				BestWinstreakModeOpDoubles        int      `json:"best_winstreak_mode_op_doubles"`
				CurrentOpWinstreak                int      `json:"current_op_winstreak"`
				BestOpWinstreak                   int      `json:"best_op_winstreak"`
				OpDoublesDamageDealt              int      `json:"op_doubles_damage_dealt"`
				OpDoublesHealthRegenerated        int      `json:"op_doubles_health_regenerated"`
				OpDoublesKills                    int      `json:"op_doubles_kills"`
				OpDoublesMeleeHits                int      `json:"op_doubles_melee_hits"`
				OpDoublesMeleeSwings              int      `json:"op_doubles_melee_swings"`
				OpDoublesRoundsPlayed             int      `json:"op_doubles_rounds_played"`
				OpDoublesWins                     int      `json:"op_doubles_wins"`
				BestComboWinstreak                int      `json:"best_combo_winstreak"`
				CurrentWinstreakModeComboDuel     int      `json:"current_winstreak_mode_combo_duel"`
				CurrentComboWinstreak             int      `json:"current_combo_winstreak"`
				BestWinstreakModeComboDuel        int      `json:"best_winstreak_mode_combo_duel"`
				ComboDuelKills                    int      `json:"combo_duel_kills"`
				ComboDuelWins                     int      `json:"combo_duel_wins"`
				DuelsOpenedRares                  int      `json:"Duels_openedRares"`
				DuelsOpenedChests                 int      `json:"Duels_openedChests"`
				DuelsOpenedCommons                int      `json:"Duels_openedCommons"`
				DuelsChestHistory                 []string `json:"duels_chest_history"`
				ActiveProjectileTrail             string   `json:"active_projectile_trail"`
				ActiveWeaponpacks                 string   `json:"active_weaponpacks"`
				ActiveAuras                       string   `json:"active_auras"`
				ActiveKillEffect                  string   `json:"active_kill_effect"`
				ActiveEmblem                      string   `json:"active_emblem"`
				BestWinstreakModePotionDuel       int      `json:"best_winstreak_mode_potion_duel"`
				BestNoDebuffWinstreak             int      `json:"best_no_debuff_winstreak"`
				PotionDuelKills                   int      `json:"potion_duel_kills"`
				PotionDuelWins                    int      `json:"potion_duel_wins"`
				GolemKitWins                      int      `json:"golem_kit_wins"`
				SwDuelGolemKitWins                int      `json:"sw_duel_golem_kit_wins"`
				SwDuelBowHits                     int      `json:"sw_duel_bow_hits"`
				SwDuelBowShots                    int      `json:"sw_duel_bow_shots"`
				ActiveCosmetictitle               string   `json:"active_cosmetictitle"`
				ProgressMode                      string   `json:"progress_mode"`
				BestWinstreakModeSwDoubles        int      `json:"best_winstreak_mode_sw_doubles"`
				SwDoublesGolemKitWins             int      `json:"sw_doubles_golem_kit_wins"`
				SwDoublesKills                    int      `json:"sw_doubles_kills"`
				SwDoublesKitWins                  int      `json:"sw_doubles_kit_wins"`
				SwDoublesWins                     int      `json:"sw_doubles_wins"`
				SkywarsIronTitlePrestige          int      `json:"skywars_iron_title_prestige"`
				ActiveCage                        string   `json:"active_cage"`
				AllModesIronTitlePrestige         int      `json:"all_modes_iron_title_prestige"`
				SkywarsGoldTitlePrestige          int      `json:"skywars_gold_title_prestige"`
				OpDuelDamageDealt                 int      `json:"op_duel_damage_dealt"`
				OpDuelHealthRegenerated           int      `json:"op_duel_health_regenerated"`
				OpDuelMeleeHits                   int      `json:"op_duel_melee_hits"`
				OpDuelMeleeSwings                 int      `json:"op_duel_melee_swings"`
				OpDuelRoundsPlayed                int      `json:"op_duel_rounds_played"`
				ActiveVictoryDance                string   `json:"active_victory_dance"`
				LayoutClassicDuelLayout           struct {
					Num0 string `json:"0"`
					Num1 string `json:"1"`
					Num2 string `json:"2"`
					Num3 string `json:"3"`
					Num7 string `json:"7"`
				} `json:"layout_classic_duel_layout"`
				CurrentWinstreakModeOpDuel int    `json:"current_winstreak_mode_op_duel"`
				OpDuelDeaths               int    `json:"op_duel_deaths"`
				OpDuelLosses               int    `json:"op_duel_losses"`
				BestWinstreakModeOpDuel    int    `json:"best_winstreak_mode_op_duel"`
				OpDuelKills                int    `json:"op_duel_kills"`
				OpDuelWins                 int    `json:"op_duel_wins"`
				DuelsWinstreakBestOpDuel   int    `json:"duels_winstreak_best_op_duel"`
				ClassicIronTitlePrestige   int    `json:"classic_iron_title_prestige"`
				ActiveKillmessages         string `json:"active_killmessages"`
				LayoutUhcDuelLayout        struct {
					Num0  string `json:"0"`
					Num1  string `json:"1"`
					Num2  string `json:"2"`
					Num3  string `json:"3"`
					Num4  string `json:"4"`
					Num5  string `json:"5"`
					Num6  string `json:"6"`
					Num7  string `json:"7"`
					Num8  string `json:"8"`
					Num9  string `json:"9"`
					Num24 string `json:"24"`
					Num33 string `json:"33"`
					Num34 string `json:"34"`
				} `json:"layout_uhc_duel_layout"`
				CurrentWinstreakModeUhcDuel int    `json:"current_winstreak_mode_uhc_duel"`
				BestWinstreakModeUhcDuel    int    `json:"best_winstreak_mode_uhc_duel"`
				UhcDuelKills                int    `json:"uhc_duel_kills"`
				UhcDuelWins                 int    `json:"uhc_duel_wins"`
				DuelsWinstreakBestUhcDuel   int    `json:"duels_winstreak_best_uhc_duel"`
				UhcDuelDeaths               int    `json:"uhc_duel_deaths"`
				UhcDuelLosses               int    `json:"uhc_duel_losses"`
				StatusField                 string `json:"status_field"`
				LayoutBridgeDuelLayout      struct {
					Num0 string `json:"0"`
					Num1 string `json:"1"`
					Num2 string `json:"2"`
					Num3 string `json:"3"`
					Num4 string `json:"4"`
					Num5 string `json:"5"`
					Num7 string `json:"7"`
					Num8 string `json:"8"`
				} `json:"layout_bridge_duel_layout"`
				MwDuelBlocksPlaced                int `json:"mw_duel_blocks_placed"`
				MwDuelBowHits                     int `json:"mw_duel_bow_hits"`
				MwDuelBowShots                    int `json:"mw_duel_bow_shots"`
				DuelsWinstreakBestBridgeDoubles   int `json:"duels_winstreak_best_bridge_doubles"`
				AllModesGoldTitlePrestige         int `json:"all_modes_gold_title_prestige"`
				CurrentWinstreakModeBridge3V3V3V3 int `json:"current_winstreak_mode_bridge_3v3v3v3"`
				Bridge3V3V3V3BlocksPlaced         int `json:"bridge_3v3v3v3_blocks_placed"`
				Bridge3V3V3V3BridgeDeaths         int `json:"bridge_3v3v3v3_bridge_deaths"`
				Bridge3V3V3V3BridgeKills          int `json:"bridge_3v3v3v3_bridge_kills"`
				Bridge3V3V3V3DamageDealt          int `json:"bridge_3v3v3v3_damage_dealt"`
				Bridge3V3V3V3Goals                int `json:"bridge_3v3v3v3_goals"`
				Bridge3V3V3V3HealthRegenerated    int `json:"bridge_3v3v3v3_health_regenerated"`
				Bridge3V3V3V3Losses               int `json:"bridge_3v3v3v3_losses"`
				Bridge3V3V3V3MeleeHits            int `json:"bridge_3v3v3v3_melee_hits"`
				Bridge3V3V3V3MeleeSwings          int `json:"bridge_3v3v3v3_melee_swings"`
				Bridge3V3V3V3RoundsPlayed         int `json:"bridge_3v3v3v3_rounds_played"`
				Bridge3V3V3V3BowShots             int `json:"bridge_3v3v3v3_bow_shots"`
				SwDoublesScoutKitWins             int `json:"sw_doubles_scout_kit_wins"`
			} `json:"Duels"`
			MurderMystery struct {
				MurdermysteryBooks                                     []string `json:"murdermystery_books"`
				DetectiveChance                                        int      `json:"detective_chance"`
				MurdererChance                                         int      `json:"murderer_chance"`
				CoinsPickedupMURDERCLASSIC                             int      `json:"coins_pickedup_MURDER_CLASSIC"`
				DeathsWidowSDen                                        int      `json:"deaths_widow's_den"`
				Coins                                                  int      `json:"coins"`
				DeathsWidowSDenMURDERCLASSIC                           int      `json:"deaths_widow's_den_MURDER_CLASSIC"`
				CoinsPickedupWidowSDen                                 int      `json:"coins_pickedup_widow's_den"`
				Deaths                                                 int      `json:"deaths"`
				GamesWidowSDenMURDERCLASSIC                            int      `json:"games_widow's_den_MURDER_CLASSIC"`
				Games                                                  int      `json:"games"`
				GamesMURDERCLASSIC                                     int      `json:"games_MURDER_CLASSIC"`
				CoinsPickedup                                          int      `json:"coins_pickedup"`
				CoinsPickedupWidowSDenMURDERCLASSIC                    int      `json:"coins_pickedup_widow's_den_MURDER_CLASSIC"`
				DeathsMURDERCLASSIC                                    int      `json:"deaths_MURDER_CLASSIC"`
				GamesWidowSDen                                         int      `json:"games_widow's_den"`
				Showqueuebook                                          bool     `json:"showqueuebook"`
				CoinsPickedupMountainMURDERCLASSIC                     int      `json:"coins_pickedup_mountain_MURDER_CLASSIC"`
				Wins                                                   int      `json:"wins"`
				WinsMountain                                           int      `json:"wins_mountain"`
				GamesMountainMURDERCLASSIC                             int      `json:"games_mountain_MURDER_CLASSIC"`
				WinsMountainMURDERCLASSIC                              int      `json:"wins_mountain_MURDER_CLASSIC"`
				GamesMountain                                          int      `json:"games_mountain"`
				CoinsPickedupMountain                                  int      `json:"coins_pickedup_mountain"`
				WinsMURDERCLASSIC                                      int      `json:"wins_MURDER_CLASSIC"`
				DeathsLibraryMURDERCLASSIC                             int      `json:"deaths_library_MURDER_CLASSIC"`
				GamesLibraryMURDERCLASSIC                              int      `json:"games_library_MURDER_CLASSIC"`
				DeathsLibrary                                          int      `json:"deaths_library"`
				CoinsPickedupLibrary                                   int      `json:"coins_pickedup_library"`
				CoinsPickedupLibraryMURDERCLASSIC                      int      `json:"coins_pickedup_library_MURDER_CLASSIC"`
				GamesLibrary                                           int      `json:"games_library"`
				GrantedChests                                          int      `json:"granted_chests"`
				MmChests                                               int      `json:"mm_chests"`
				Packages                                               []string `json:"packages"`
				MurderMysteryOpenedChests                              int      `json:"MurderMystery_openedChests"`
				ChestHistoryNew                                        []string `json:"chest_history_new"`
				MurderMysteryOpenedCommons                             int      `json:"MurderMystery_openedCommons"`
				MurderMysteryOpenedRares                               int      `json:"MurderMystery_openedRares"`
				ActiveLastWords                                        string   `json:"active_last_words"`
				ActiveKnifeSkin                                        string   `json:"active_knife_skin"`
				CoinsPickedupAquariumMURDERCLASSIC                     int      `json:"coins_pickedup_aquarium_MURDER_CLASSIC"`
				GamesAquariumMURDERCLASSIC                             int      `json:"games_aquarium_MURDER_CLASSIC"`
				DeathsAquarium                                         int      `json:"deaths_aquarium"`
				GamesAquarium                                          int      `json:"games_aquarium"`
				CoinsPickedupAquarium                                  int      `json:"coins_pickedup_aquarium"`
				WinsAquarium                                           int      `json:"wins_aquarium"`
				WinsAquariumMURDERCLASSIC                              int      `json:"wins_aquarium_MURDER_CLASSIC"`
				DeathsAquariumMURDERCLASSIC                            int      `json:"deaths_aquarium_MURDER_CLASSIC"`
				WinsWidowSDenMURDERCLASSIC                             int      `json:"wins_widow's_den_MURDER_CLASSIC"`
				WinsWidowSDen                                          int      `json:"wins_widow's_den"`
				WinsHollywood                                          int      `json:"wins_hollywood"`
				WinsHollywoodMURDERCLASSIC                             int      `json:"wins_hollywood_MURDER_CLASSIC"`
				CoinsPickedupHollywoodMURDERCLASSIC                    int      `json:"coins_pickedup_hollywood_MURDER_CLASSIC"`
				CoinsPickedupHollywood                                 int      `json:"coins_pickedup_hollywood"`
				GamesHollywood                                         int      `json:"games_hollywood"`
				GamesHollywoodMURDERCLASSIC                            int      `json:"games_hollywood_MURDER_CLASSIC"`
				GamesHypixelWorldMURDERCLASSIC                         int      `json:"games_hypixel_world_MURDER_CLASSIC"`
				WinsHypixelWorldMURDERCLASSIC                          int      `json:"wins_hypixel_world_MURDER_CLASSIC"`
				GamesHypixelWorld                                      int      `json:"games_hypixel_world"`
				CoinsPickedupHypixelWorldMURDERCLASSIC                 int      `json:"coins_pickedup_hypixel_world_MURDER_CLASSIC"`
				CoinsPickedupHypixelWorld                              int      `json:"coins_pickedup_hypixel_world"`
				WinsHypixelWorld                                       int      `json:"wins_hypixel_world"`
				DeathsHollywoodMURDERCLASSIC                           int      `json:"deaths_hollywood_MURDER_CLASSIC"`
				DeathsHollywood                                        int      `json:"deaths_hollywood"`
				GamesHeadquartersMURDERCLASSIC                         int      `json:"games_headquarters_MURDER_CLASSIC"`
				WinsHeadquartersMURDERCLASSIC                          int      `json:"wins_headquarters_MURDER_CLASSIC"`
				CoinsPickedupHeadquarters                              int      `json:"coins_pickedup_headquarters"`
				WinsHeadquarters                                       int      `json:"wins_headquarters"`
				GamesHeadquarters                                      int      `json:"games_headquarters"`
				CoinsPickedupHeadquartersMURDERCLASSIC                 int      `json:"coins_pickedup_headquarters_MURDER_CLASSIC"`
				GamesAncientTomb                                       int      `json:"games_ancient_tomb"`
				GamesAncientTombMURDERCLASSIC                          int      `json:"games_ancient_tomb_MURDER_CLASSIC"`
				WinsAncientTombMURDERCLASSIC                           int      `json:"wins_ancient_tomb_MURDER_CLASSIC"`
				CoinsPickedupAncientTombMURDERCLASSIC                  int      `json:"coins_pickedup_ancient_tomb_MURDER_CLASSIC"`
				CoinsPickedupAncientTomb                               int      `json:"coins_pickedup_ancient_tomb"`
				WinsAncientTomb                                        int      `json:"wins_ancient_tomb"`
				WinsDarkfallMURDERCLASSIC                              int      `json:"wins_darkfall_MURDER_CLASSIC"`
				WinsDarkfall                                           int      `json:"wins_darkfall"`
				CoinsPickedupDarkfallMURDERCLASSIC                     int      `json:"coins_pickedup_darkfall_MURDER_CLASSIC"`
				GamesDarkfallMURDERCLASSIC                             int      `json:"games_darkfall_MURDER_CLASSIC"`
				GamesDarkfall                                          int      `json:"games_darkfall"`
				CoinsPickedupDarkfall                                  int      `json:"coins_pickedup_darkfall"`
				CoinsPickedupSkywayPier                                int      `json:"coins_pickedup_skyway_pier"`
				CoinsPickedupSkywayPierMURDERCLASSIC                   int      `json:"coins_pickedup_skyway_pier_MURDER_CLASSIC"`
				DeathsSkywayPier                                       int      `json:"deaths_skyway_pier"`
				DeathsSkywayPierMURDERCLASSIC                          int      `json:"deaths_skyway_pier_MURDER_CLASSIC"`
				GamesSkywayPier                                        int      `json:"games_skyway_pier"`
				GamesSkywayPierMURDERCLASSIC                           int      `json:"games_skyway_pier_MURDER_CLASSIC"`
				WinsSkywayPier                                         int      `json:"wins_skyway_pier"`
				WinsSkywayPierMURDERCLASSIC                            int      `json:"wins_skyway_pier_MURDER_CLASSIC"`
				CoinsPickedupGoldRush                                  int      `json:"coins_pickedup_gold_rush"`
				CoinsPickedupGoldRushMURDERCLASSIC                     int      `json:"coins_pickedup_gold_rush_MURDER_CLASSIC"`
				GamesGoldRush                                          int      `json:"games_gold_rush"`
				GamesGoldRushMURDERCLASSIC                             int      `json:"games_gold_rush_MURDER_CLASSIC"`
				WinsGoldRush                                           int      `json:"wins_gold_rush"`
				WinsGoldRushMURDERCLASSIC                              int      `json:"wins_gold_rush_MURDER_CLASSIC"`
				ActiveKillNote                                         string   `json:"active_kill_note"`
				ActiveVictoryDance                                     string   `json:"active_victory_dance"`
				CoinsPickedupArchivesTopFloor                          int      `json:"coins_pickedup_archives_top_floor"`
				CoinsPickedupArchivesTopFloorMURDERCLASSIC             int      `json:"coins_pickedup_archives_top_floor_MURDER_CLASSIC"`
				GamesArchivesTopFloor                                  int      `json:"games_archives_top_floor"`
				GamesArchivesTopFloorMURDERCLASSIC                     int      `json:"games_archives_top_floor_MURDER_CLASSIC"`
				CoinsPickedupSpookyMansion                             int      `json:"coins_pickedup_spooky_mansion"`
				CoinsPickedupSpookyMansionMURDERCLASSIC                int      `json:"coins_pickedup_spooky_mansion_MURDER_CLASSIC"`
				DeathsSpookyMansion                                    int      `json:"deaths_spooky_mansion"`
				DeathsSpookyMansionMURDERCLASSIC                       int      `json:"deaths_spooky_mansion_MURDER_CLASSIC"`
				GamesSpookyMansion                                     int      `json:"games_spooky_mansion"`
				GamesSpookyMansionMURDERCLASSIC                        int      `json:"games_spooky_mansion_MURDER_CLASSIC"`
				Kills                                                  int      `json:"kills"`
				KillsMURDERCLASSIC                                     int      `json:"kills_MURDER_CLASSIC"`
				KillsAsMurderer                                        int      `json:"kills_as_murderer"`
				KillsAsMurdererMURDERCLASSIC                           int      `json:"kills_as_murderer_MURDER_CLASSIC"`
				KillsAsMurdererSpookyMansion                           int      `json:"kills_as_murderer_spooky_mansion"`
				KillsAsMurdererSpookyMansionMURDERCLASSIC              int      `json:"kills_as_murderer_spooky_mansion_MURDER_CLASSIC"`
				KillsSpookyMansion                                     int      `json:"kills_spooky_mansion"`
				KillsSpookyMansionMURDERCLASSIC                        int      `json:"kills_spooky_mansion_MURDER_CLASSIC"`
				KnifeKills                                             int      `json:"knife_kills"`
				KnifeKillsMURDERCLASSIC                                int      `json:"knife_kills_MURDER_CLASSIC"`
				KnifeKillsSpookyMansion                                int      `json:"knife_kills_spooky_mansion"`
				KnifeKillsSpookyMansionMURDERCLASSIC                   int      `json:"knife_kills_spooky_mansion_MURDER_CLASSIC"`
				DeathsMURDERINFECTION                                  int      `json:"deaths_MURDER_INFECTION"`
				DeathsWidowSDenMURDERINFECTION                         int      `json:"deaths_widow's_den_MURDER_INFECTION"`
				GamesMURDERINFECTION                                   int      `json:"games_MURDER_INFECTION"`
				GamesWidowSDenMURDERINFECTION                          int      `json:"games_widow's_den_MURDER_INFECTION"`
				LongestTimeAsSurvivorSeconds                           int      `json:"longest_time_as_survivor_seconds"`
				LongestTimeAsSurvivorSecondsMURDERINFECTION            int      `json:"longest_time_as_survivor_seconds_MURDER_INFECTION"`
				LongestTimeAsSurvivorSecondsWidowSDen                  int      `json:"longest_time_as_survivor_seconds_widow's_den"`
				LongestTimeAsSurvivorSecondsWidowSDenMURDERINFECTION   int      `json:"longest_time_as_survivor_seconds_widow's_den_MURDER_INFECTION"`
				TotalTimeSurvivedSeconds                               int      `json:"total_time_survived_seconds"`
				TotalTimeSurvivedSecondsMURDERINFECTION                int      `json:"total_time_survived_seconds_MURDER_INFECTION"`
				TotalTimeSurvivedSecondsWidowSDen                      int      `json:"total_time_survived_seconds_widow's_den"`
				TotalTimeSurvivedSecondsWidowSDenMURDERINFECTION       int      `json:"total_time_survived_seconds_widow's_den_MURDER_INFECTION"`
				CoinsPickedupSnowglobe                                 int      `json:"coins_pickedup_snowglobe"`
				CoinsPickedupSnowglobeMURDERCLASSIC                    int      `json:"coins_pickedup_snowglobe_MURDER_CLASSIC"`
				DeathsSnowglobe                                        int      `json:"deaths_snowglobe"`
				DeathsSnowglobeMURDERCLASSIC                           int      `json:"deaths_snowglobe_MURDER_CLASSIC"`
				GamesSnowglobe                                         int      `json:"games_snowglobe"`
				GamesSnowglobeMURDERCLASSIC                            int      `json:"games_snowglobe_MURDER_CLASSIC"`
				WinsSnowglobe                                          int      `json:"wins_snowglobe"`
				WinsSnowglobeMURDERCLASSIC                             int      `json:"wins_snowglobe_MURDER_CLASSIC"`
				KillsSnowglobe                                         int      `json:"kills_snowglobe"`
				KillsSnowglobeMURDERCLASSIC                            int      `json:"kills_snowglobe_MURDER_CLASSIC"`
				TrapKills                                              int      `json:"trap_kills"`
				TrapKillsMURDERCLASSIC                                 int      `json:"trap_kills_MURDER_CLASSIC"`
				TrapKillsSnowglobe                                     int      `json:"trap_kills_snowglobe"`
				TrapKillsSnowglobeMURDERCLASSIC                        int      `json:"trap_kills_snowglobe_MURDER_CLASSIC"`
				DeathsArchives                                         int      `json:"deaths_archives"`
				DeathsArchivesMURDERCLASSIC                            int      `json:"deaths_archives_MURDER_CLASSIC"`
				GamesArchives                                          int      `json:"games_archives"`
				GamesArchivesMURDERCLASSIC                             int      `json:"games_archives_MURDER_CLASSIC"`
				WinsArchivesTopFloor                                   int      `json:"wins_archives_top_floor"`
				WinsArchivesTopFloorMURDERCLASSIC                      int      `json:"wins_archives_top_floor_MURDER_CLASSIC"`
				DeathsGoldRush                                         int      `json:"deaths_gold_rush"`
				DeathsGoldRushMURDERCLASSIC                            int      `json:"deaths_gold_rush_MURDER_CLASSIC"`
				DeathsArchivesTopFloor                                 int      `json:"deaths_archives_top_floor"`
				DeathsArchivesTopFloorMURDERCLASSIC                    int      `json:"deaths_archives_top_floor_MURDER_CLASSIC"`
				DeathsSanPeraticoV2                                    int      `json:"deaths_san_peratico_v2"`
				DeathsSanPeraticoV2MURDERCLASSIC                       int      `json:"deaths_san_peratico_v2_MURDER_CLASSIC"`
				GamesSanPeraticoV2                                     int      `json:"games_san_peratico_v2"`
				GamesSanPeraticoV2MURDERCLASSIC                        int      `json:"games_san_peratico_v2_MURDER_CLASSIC"`
				CoinsPickedupArchives                                  int      `json:"coins_pickedup_archives"`
				CoinsPickedupArchivesMURDERCLASSIC                     int      `json:"coins_pickedup_archives_MURDER_CLASSIC"`
				KillsArchives                                          int      `json:"kills_archives"`
				KillsArchivesMURDERCLASSIC                             int      `json:"kills_archives_MURDER_CLASSIC"`
				KillsAsMurdererArchives                                int      `json:"kills_as_murderer_archives"`
				KillsAsMurdererArchivesMURDERCLASSIC                   int      `json:"kills_as_murderer_archives_MURDER_CLASSIC"`
				KnifeKillsArchives                                     int      `json:"knife_kills_archives"`
				KnifeKillsArchivesMURDERCLASSIC                        int      `json:"knife_kills_archives_MURDER_CLASSIC"`
				BowKills                                               int      `json:"bow_kills"`
				BowKillsMURDERCLASSIC                                  int      `json:"bow_kills_MURDER_CLASSIC"`
				BowKillsHypixelWorld                                   int      `json:"bow_kills_hypixel_world"`
				BowKillsHypixelWorldMURDERCLASSIC                      int      `json:"bow_kills_hypixel_world_MURDER_CLASSIC"`
				KillsHypixelWorld                                      int      `json:"kills_hypixel_world"`
				KillsHypixelWorldMURDERCLASSIC                         int      `json:"kills_hypixel_world_MURDER_CLASSIC"`
				WasHero                                                int      `json:"was_hero"`
				WasHeroMURDERCLASSIC                                   int      `json:"was_hero_MURDER_CLASSIC"`
				WasHeroHypixelWorld                                    int      `json:"was_hero_hypixel_world"`
				WasHeroHypixelWorldMURDERCLASSIC                       int      `json:"was_hero_hypixel_world_MURDER_CLASSIC"`
				DeathsMountain                                         int      `json:"deaths_mountain"`
				DeathsMountainMURDERCLASSIC                            int      `json:"deaths_mountain_MURDER_CLASSIC"`
				CoinsPickedupSnowfall                                  int      `json:"coins_pickedup_snowfall"`
				CoinsPickedupSnowfallMURDERCLASSIC                     int      `json:"coins_pickedup_snowfall_MURDER_CLASSIC"`
				GamesSnowfall                                          int      `json:"games_snowfall"`
				GamesSnowfallMURDERCLASSIC                             int      `json:"games_snowfall_MURDER_CLASSIC"`
				WinsSnowfall                                           int      `json:"wins_snowfall"`
				WinsSnowfallMURDERCLASSIC                              int      `json:"wins_snowfall_MURDER_CLASSIC"`
				BowKillsLibrary                                        int      `json:"bow_kills_library"`
				BowKillsLibraryMURDERCLASSIC                           int      `json:"bow_kills_library_MURDER_CLASSIC"`
				KillsLibrary                                           int      `json:"kills_library"`
				KillsLibraryMURDERCLASSIC                              int      `json:"kills_library_MURDER_CLASSIC"`
				WasHeroLibrary                                         int      `json:"was_hero_library"`
				WasHeroLibraryMURDERCLASSIC                            int      `json:"was_hero_library_MURDER_CLASSIC"`
				MmChristmasChests                                      int      `json:"mm_christmas_chests"`
				CoinsPickedupTowerfall                                 int      `json:"coins_pickedup_towerfall"`
				CoinsPickedupTowerfallMURDERCLASSIC                    int      `json:"coins_pickedup_towerfall_MURDER_CLASSIC"`
				DeathsTowerfall                                        int      `json:"deaths_towerfall"`
				DeathsTowerfallMURDERCLASSIC                           int      `json:"deaths_towerfall_MURDER_CLASSIC"`
				GamesTowerfall                                         int      `json:"games_towerfall"`
				GamesTowerfallMURDERCLASSIC                            int      `json:"games_towerfall_MURDER_CLASSIC"`
				WinsTowerfall                                          int      `json:"wins_towerfall"`
				WinsTowerfallMURDERCLASSIC                             int      `json:"wins_towerfall_MURDER_CLASSIC"`
				KillsAncientTomb                                       int      `json:"kills_ancient_tomb"`
				KillsAncientTombMURDERCLASSIC                          int      `json:"kills_ancient_tomb_MURDER_CLASSIC"`
				KillsAsMurdererAncientTomb                             int      `json:"kills_as_murderer_ancient_tomb"`
				KillsAsMurdererAncientTombMURDERCLASSIC                int      `json:"kills_as_murderer_ancient_tomb_MURDER_CLASSIC"`
				KnifeKillsAncientTomb                                  int      `json:"knife_kills_ancient_tomb"`
				KnifeKillsAncientTombMURDERCLASSIC                     int      `json:"knife_kills_ancient_tomb_MURDER_CLASSIC"`
				MurdererWins                                           int      `json:"murderer_wins"`
				MurdererWinsMURDERCLASSIC                              int      `json:"murderer_wins_MURDER_CLASSIC"`
				MurdererWinsAncientTomb                                int      `json:"murderer_wins_ancient_tomb"`
				MurdererWinsAncientTombMURDERCLASSIC                   int      `json:"murderer_wins_ancient_tomb_MURDER_CLASSIC"`
				QuickestMurdererWinTimeSeconds                         int      `json:"quickest_murderer_win_time_seconds"`
				QuickestMurdererWinTimeSecondsMURDERCLASSIC            int      `json:"quickest_murderer_win_time_seconds_MURDER_CLASSIC"`
				QuickestMurdererWinTimeSecondsAncientTomb              int      `json:"quickest_murderer_win_time_seconds_ancient_tomb"`
				QuickestMurdererWinTimeSecondsAncientTombMURDERCLASSIC int      `json:"quickest_murderer_win_time_seconds_ancient_tomb_MURDER_CLASSIC"`
				CoinsPickedupMURDERINFECTION                           int      `json:"coins_pickedup_MURDER_INFECTION"`
				CoinsPickedupLibraryMURDERINFECTION                    int      `json:"coins_pickedup_library_MURDER_INFECTION"`
				GamesLibraryMURDERINFECTION                            int      `json:"games_library_MURDER_INFECTION"`
				KillsAsInfected                                        int      `json:"kills_as_infected"`
				KillsAsInfectedMURDERINFECTION                         int      `json:"kills_as_infected_MURDER_INFECTION"`
				KillsAsInfectedLibrary                                 int      `json:"kills_as_infected_library"`
				KillsAsInfectedLibraryMURDERINFECTION                  int      `json:"kills_as_infected_library_MURDER_INFECTION"`
				SurvivorWins                                           int      `json:"survivor_wins"`
				SurvivorWinsMURDERINFECTION                            int      `json:"survivor_wins_MURDER_INFECTION"`
				SurvivorWinsLibrary                                    int      `json:"survivor_wins_library"`
				SurvivorWinsLibraryMURDERINFECTION                     int      `json:"survivor_wins_library_MURDER_INFECTION"`
				TotalTimeSurvivedSecondsLibrary                        int      `json:"total_time_survived_seconds_library"`
				TotalTimeSurvivedSecondsLibraryMURDERINFECTION         int      `json:"total_time_survived_seconds_library_MURDER_INFECTION"`
				WinsMURDERINFECTION                                    int      `json:"wins_MURDER_INFECTION"`
				WinsLibrary                                            int      `json:"wins_library"`
				WinsLibraryMURDERINFECTION                             int      `json:"wins_library_MURDER_INFECTION"`
				CoinsPickedupTransport                                 int      `json:"coins_pickedup_transport"`
				CoinsPickedupTransportMURDERINFECTION                  int      `json:"coins_pickedup_transport_MURDER_INFECTION"`
				DeathsTransport                                        int      `json:"deaths_transport"`
				DeathsTransportMURDERINFECTION                         int      `json:"deaths_transport_MURDER_INFECTION"`
				GamesTransport                                         int      `json:"games_transport"`
				GamesTransportMURDERINFECTION                          int      `json:"games_transport_MURDER_INFECTION"`
				TotalTimeSurvivedSecondsTransport                      int      `json:"total_time_survived_seconds_transport"`
				TotalTimeSurvivedSecondsTransportMURDERINFECTION       int      `json:"total_time_survived_seconds_transport_MURDER_INFECTION"`
				CoinsPickedupAncientTombMURDERINFECTION                int      `json:"coins_pickedup_ancient_tomb_MURDER_INFECTION"`
				GamesAncientTombMURDERINFECTION                        int      `json:"games_ancient_tomb_MURDER_INFECTION"`
				KillsAsInfectedAncientTomb                             int      `json:"kills_as_infected_ancient_tomb"`
				KillsAsInfectedAncientTombMURDERINFECTION              int      `json:"kills_as_infected_ancient_tomb_MURDER_INFECTION"`
				KillsAsSurvivor                                        int      `json:"kills_as_survivor"`
				KillsAsSurvivorMURDERINFECTION                         int      `json:"kills_as_survivor_MURDER_INFECTION"`
				KillsAsSurvivorAncientTomb                             int      `json:"kills_as_survivor_ancient_tomb"`
				KillsAsSurvivorAncientTombMURDERINFECTION              int      `json:"kills_as_survivor_ancient_tomb_MURDER_INFECTION"`
				SurvivorWinsAncientTomb                                int      `json:"survivor_wins_ancient_tomb"`
				SurvivorWinsAncientTombMURDERINFECTION                 int      `json:"survivor_wins_ancient_tomb_MURDER_INFECTION"`
				TotalTimeSurvivedSecondsAncientTomb                    int      `json:"total_time_survived_seconds_ancient_tomb"`
				TotalTimeSurvivedSecondsAncientTombMURDERINFECTION     int      `json:"total_time_survived_seconds_ancient_tomb_MURDER_INFECTION"`
				WinsAncientTombMURDERINFECTION                         int      `json:"wins_ancient_tomb_MURDER_INFECTION"`
				CoinsPickedupHeadquartersMURDERINFECTION               int      `json:"coins_pickedup_headquarters_MURDER_INFECTION"`
				GamesHeadquartersMURDERINFECTION                       int      `json:"games_headquarters_MURDER_INFECTION"`
				SurvivorWinsHeadquarters                               int      `json:"survivor_wins_headquarters"`
				SurvivorWinsHeadquartersMURDERINFECTION                int      `json:"survivor_wins_headquarters_MURDER_INFECTION"`
				TotalTimeSurvivedSecondsHeadquarters                   int      `json:"total_time_survived_seconds_headquarters"`
				TotalTimeSurvivedSecondsHeadquartersMURDERINFECTION    int      `json:"total_time_survived_seconds_headquarters_MURDER_INFECTION"`
				WinsHeadquartersMURDERINFECTION                        int      `json:"wins_headquarters_MURDER_INFECTION"`
				CoinsPickedupGoldRushMURDERINFECTION                   int      `json:"coins_pickedup_gold_rush_MURDER_INFECTION"`
				GamesGoldRushMURDERINFECTION                           int      `json:"games_gold_rush_MURDER_INFECTION"`
				TotalTimeSurvivedSecondsGoldRush                       int      `json:"total_time_survived_seconds_gold_rush"`
				TotalTimeSurvivedSecondsGoldRushMURDERINFECTION        int      `json:"total_time_survived_seconds_gold_rush_MURDER_INFECTION"`
			} `json:"MurderMystery"`
			Legacy struct {
				NextTokensSeconds int      `json:"next_tokens_seconds"`
				PaintballTokens   int      `json:"paintball_tokens"`
				TotalTokens       int      `json:"total_tokens"`
				Tokens            int      `json:"tokens"`
				GingerbreadTokens int      `json:"gingerbread_tokens"`
				QuakecraftTokens  int      `json:"quakecraft_tokens"`
				Packages          []string `json:"packages"`
				Speed             string   `json:"speed"`
				PreferredChannel  string   `json:"preferredChannel"`
			} `json:"Legacy"`
			Pit struct {
				PitStatsPtl struct {
					Joins                 int `json:"joins"`
					DamageReceived        int `json:"damage_received"`
					MeleeDamageReceived   int `json:"melee_damage_received"`
					Assists               int `json:"assists"`
					BowDamageReceived     int `json:"bow_damage_received"`
					CashEarned            int `json:"cash_earned"`
					ChatMessages          int `json:"chat_messages"`
					DamageDealt           int `json:"damage_dealt"`
					Deaths                int `json:"deaths"`
					JumpedIntoPit         int `json:"jumped_into_pit"`
					Kills                 int `json:"kills"`
					LaunchedByLaunchers   int `json:"launched_by_launchers"`
					LeftClicks            int `json:"left_clicks"`
					MaxStreak             int `json:"max_streak"`
					MeleeDamageDealt      int `json:"melee_damage_dealt"`
					PlaytimeMinutes       int `json:"playtime_minutes"`
					SwordHits             int `json:"sword_hits"`
					ArrowsFired           int `json:"arrows_fired"`
					GappleEaten           int `json:"gapple_eaten"`
					ArrowHits             int `json:"arrow_hits"`
					BowDamageDealt        int `json:"bow_damage_dealt"`
					GheadEaten            int `json:"ghead_eaten"`
					EnderchestOpened      int `json:"enderchest_opened"`
					LavaBucketEmptied     int `json:"lava_bucket_emptied"`
					DiamondItemsPurchased int `json:"diamond_items_purchased"`
					WheatFarmed           int `json:"wheat_farmed"`
					ContractsCompleted    int `json:"contracts_completed"`
					ContractsStarted      int `json:"contracts_started"`
					BlocksBroken          int `json:"blocks_broken"`
					BlocksPlaced          int `json:"blocks_placed"`
					EnchantedTier1        int `json:"enchanted_tier1"`
					EnchantedTier2        int `json:"enchanted_tier2"`
					EnchantedTier3        int `json:"enchanted_tier3"`
					FishingRodLaunched    int `json:"fishing_rod_launched"`
					SoupsDrank            int `json:"soups_drank"`
					FishedAnything        int `json:"fished_anything"`
					SewerTreasuresFound   int `json:"sewer_treasures_found"`
					IngotsCash            int `json:"ingots_cash"`
					IngotsPickedUp        int `json:"ingots_picked_up"`
					VampireHealedHp       int `json:"vampire_healed_hp"`
					LaunchedByDemonSpawn  int `json:"launched_by_demon_spawn"`
					BountiesOf500GWithBh  int `json:"bounties_of_500g_with_bh"`
					RagePotatoesEaten     int `json:"rage_potatoes_eaten"`
				} `json:"pit_stats_ptl"`
				Profile struct {
					OutgoingOffers  []interface{} `json:"outgoing_offers"`
					ContractChoices interface{}   `json:"contract_choices"`
					LastSave        int64         `json:"last_save"`
					KingQuest       struct {
						Kills        int   `json:"kills"`
						Renown       int   `json:"renown"`
						LastAccepted int64 `json:"last_accepted"`
					} `json:"king_quest"`
					TradeTimestamps []int64 `json:"trade_timestamps"`
					Unlocks1        []struct {
						Tier        int    `json:"tier"`
						AcquireDate int64  `json:"acquireDate"`
						Key         string `json:"key"`
					} `json:"unlocks_1"`
					DeathRecaps struct {
						Type int   `json:"type"`
						Data []int `json:"data"`
					} `json:"death_recaps"`
					InvEnderchest struct {
						Type int   `json:"type"`
						Data []int `json:"data"`
					} `json:"inv_enderchest"`
					Unlocks2 []struct {
						Tier        int    `json:"tier"`
						AcquireDate int64  `json:"acquireDate"`
						Key         string `json:"key"`
					} `json:"unlocks_2"`
					GenesisSpawnInBase bool    `json:"genesis_spawn_in_base"`
					Cash               float64 `json:"cash"`
					LeaderboardStats  map[string]int `json:"leaderboard_stats"`
					SelectedPerk3    interface{} `json:"selected_perk_3"`
					SelectedPerk2    interface{} `json:"selected_perk_2"`
					SelectedPerk1    string      `json:"selected_perk_1"`
					SelectedPerk0    string      `json:"selected_perk_0"`
					LastContract     int64       `json:"last_contract"`
					GoldTransactions []struct {
						Amount    int   `json:"amount"`
						Timestamp int64 `json:"timestamp"`
					} `json:"gold_transactions"`
					CashDuringPrestige2 float64 `json:"cash_during_prestige_2"`
					GenesisAllegiance   string  `json:"genesis_allegiance"`
					InvContents         struct {
						Type int   `json:"type"`
						Data []int `json:"data"`
					} `json:"inv_contents"`
					Unlocks []struct {
						Tier        int    `json:"tier"`
						AcquireDate int64  `json:"acquireDate"`
						Key         string `json:"key"`
					} `json:"unlocks"`
					CashDuringPrestige1 float64 `json:"cash_during_prestige_1"`
					CashDuringPrestige0 float64 `json:"cash_during_prestige_0"`
					Renown              int     `json:"renown"`
					MovedAchievements1  bool    `json:"moved_achievements_1"`
					MovedAchievements2  bool    `json:"moved_achievements_2"`
					ItemsLastBuy map[string]int64 `json:"items_last_buy"`
					GenesisPoints int `json:"genesis_points"`
					Prestiges     []struct {
						Index        int   `json:"index"`
						XpOnPrestige int   `json:"xp_on_prestige"`
						Timestamp    int64 `json:"timestamp"`
					} `json:"prestiges"`
					SpireStashInv struct {
						Type int   `json:"type"`
						Data []int `json:"data"`
					} `json:"spire_stash_inv"`
					CheapMilk                  bool `json:"cheap_milk"`
					ZeroPointThreeGoldTransfer bool `json:"zero_point_three_gold_transfer"`
					RenownUnlocks              []struct {
						Tier        int    `json:"tier"`
						AcquireDate int64  `json:"acquireDate"`
						Key         string `json:"key"`
					} `json:"renown_unlocks"`
					SpireStashArmor struct {
						Type int   `json:"type"`
						Data []int `json:"data"`
					} `json:"spire_stash_armor"`
					SelectedLeaderboard    string `json:"selected_leaderboard"`
					LastMidfightDisconnect int64  `json:"last_midfight_disconnect"`
					InvArmor               struct {
						Type int   `json:"type"`
						Data []int `json:"data"`
					} `json:"inv_armor"`
					ItemStash struct {
						Type int   `json:"type"`
						Data []int `json:"data"`
					} `json:"item_stash"`
					SelectedKillstreak2   interface{}   `json:"selected_killstreak_2"`
					GenesisAllegianceTime int64         `json:"genesis_allegiance_time"`
					SelectedKillstreak1   string        `json:"selected_killstreak_1"`
					LoginMessages         []interface{} `json:"login_messages"`
					HotbarFavorites       []int         `json:"hotbar_favorites"`
					SelectedKillstreak0   interface{}   `json:"selected_killstreak_0"`
					SelectedKillstreak3   interface{}   `json:"selected_killstreak_3"`
					Xp                    int           `json:"xp"`
					EndedContracts        []struct {
						Difficulty   string `json:"difficulty"`
						GoldReward   int    `json:"gold_reward"`
						Requirements struct {
							Kills int `json:"kills"`
						} `json:"requirements,omitempty"`
						Progress struct {
							Kills int `json:"kills"`
						} `json:"progress,omitempty"`
						ChunkOfVilesReward int    `json:"chunk_of_viles_reward"`
						CompletionDate     int64  `json:"completion_date"`
						RemainingTicks     int    `json:"remaining_ticks"`
						Key                string `json:"key"`
					} `json:"ended_contracts"`
					Bounties []interface{} `json:"bounties"`
				} `json:"profile"`
				StatsMove1 int64 `json:"stats_move_1"`
			} `json:"Pit"`
			SkyBlock struct {
				Profiles struct {
					OneC6825Bf2Ae44169Aa0Fde74D1Cf007F struct {
						ProfileID string `json:"profile_id"`
						CuteName  string `json:"cute_name"`
					} `json:"1c6825bf2ae44169aa0fde74d1cf007f"`
					FourCc740Ae195940C591Bcf77Feb8Eed9D struct {
						ProfileID string `json:"profile_id"`
						CuteName  string `json:"cute_name"`
					} `json:"4cc740ae195940c591bcf77feb8eed9d"`
				} `json:"profiles"`
			} `json:"SkyBlock"`
		} `json:"stats"`
		TimePlaying           int           `json:"timePlaying"`
		UUID                  string        `json:"uuid"`
		FlashingSalePopup     int64         `json:"flashingSalePopup"`
		FlashingSalePoppedUp  int           `json:"flashingSalePoppedUp"`
		FlashingSaleOpens     int           `json:"flashingSaleOpens"`
		McVersionRp           string        `json:"mcVersionRp"`
		LastLogout            int64         `json:"lastLogout"`
		FriendRequestsUUID    []interface{} `json:"friendRequestsUuid"`
		UserLanguage          string        `json:"userLanguage"`
		LevelingReward0       bool          `json:"levelingReward_0"`
		LevelingReward1       bool          `json:"levelingReward_1"`
		LevelingReward2       bool          `json:"levelingReward_2"`
		LevelingReward3       bool          `json:"levelingReward_3"`
		LevelingReward4       bool          `json:"levelingReward_4"`
		LevelingReward5       bool          `json:"levelingReward_5"`
		AchievementTracking   []interface{} `json:"achievementTracking"`
		NetworkUpdateBook     string        `json:"network_update_book"`
		AchievementPoints     int           `json:"achievementPoints"`
		AchievementRewardsNew map[string]int `json:"achievementRewardsNew"`
		LevelingReward6 bool `json:"levelingReward_6"`
		PetConsumables  struct {
			CarrotItem   int `json:"CARROT_ITEM"`
			Cookie       int `json:"COOKIE"`
			Feather      int `json:"FEATHER"`
			Bread        int `json:"BREAD"`
			Cake         int `json:"CAKE"`
			RedRose      int `json:"RED_ROSE"`
			CookedBeef   int `json:"COOKED_BEEF"`
			WaterBucket  int `json:"WATER_BUCKET"`
			Stick        int `json:"STICK"`
			WoodSword    int `json:"WOOD_SWORD"`
			MilkBucket   int `json:"MILK_BUCKET"`
			GoldRecord   int `json:"GOLD_RECORD"`
			PumpkinPie   int `json:"PUMPKIN_PIE"`
			Leash        int `json:"LEASH"`
			LavaBucket   int `json:"LAVA_BUCKET"`
			Apple        int `json:"APPLE"`
			Wheat        int `json:"WHEAT"`
			HayBlock     int `json:"HAY_BLOCK"`
			RottenFlesh  int `json:"ROTTEN_FLESH"`
			SlimeBall    int `json:"SLIME_BALL"`
			Bone         int `json:"BONE"`
			MagmaCream   int `json:"MAGMA_CREAM"`
			MushroomSoup int `json:"MUSHROOM_SOUP"`
			Melon        int `json:"MELON"`
			Pork         int `json:"PORK"`
			RawFish      int `json:"RAW_FISH"`
			BakedPotato  int `json:"BAKED_POTATO"`
		} `json:"petConsumables"`
		VanityMeta struct {
			Packages []string `json:"packages"`
		} `json:"vanityMeta"`
		HousingMeta struct {
			FirstHouseJoinMs   int64    `json:"firstHouseJoinMs"`
			TutorialStep       string   `json:"tutorialStep"`
			AllowedBlocks      []string `json:"allowedBlocks"`
			Packages           []string `json:"packages"`
			GivenCookies104977 []string `json:"given_cookies_104977"`
			PlayerSettings     struct {
				Visibility string `json:"VISIBILITY"`
			} `json:"playerSettings"`
			SelectedChannelsV3 []string `json:"selectedChannels_v3"`
			GivenCookies105042 []string `json:"given_cookies_105042"`
			PlotSize           string   `json:"plotSize"`
			GivenCookies105073 []string `json:"given_cookies_105073"`
			GivenCookies105106 []string `json:"given_cookies_105106"`
			GivenCookies105118 []string `json:"given_cookies_105118"`
		} `json:"housingMeta"`
		Quests struct {
			WarlordsCtf struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
					} `json:"objectives"`
				} `json:"active"`
			} `json:"warlords_ctf"`
			WarlordsTdm struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
					} `json:"objectives"`
				} `json:"active"`
			} `json:"warlords_tdm"`
			WarlordsDomination struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
					} `json:"objectives"`
				} `json:"active"`
			} `json:"warlords_domination"`
			WarlordsVictorious struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"warlords_victorious"`
			WarlordsObjectives struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
						WarlordsDailyObjectives int `json:"warlords_daily_objectives"`
					} `json:"objectives"`
				} `json:"active"`
			} `json:"warlords_objectives"`
			WarlordsAllStar struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
						WarlordsWeeklyDamage int `json:"warlords_weekly_damage"`
						WarlordsWeeklyHeal   int `json:"warlords_weekly_heal"`
					} `json:"objectives"`
				} `json:"active"`
			} `json:"warlords_all_star"`
			WarlordsDedication struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
					} `json:"objectives"`
				} `json:"active"`
			} `json:"warlords_dedication"`
			NormalBrawler struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
					} `json:"objectives"`
				} `json:"active"`
			} `json:"normal_brawler"`
			InsaneBrawler struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
					} `json:"objectives"`
				} `json:"active"`
			} `json:"insane_brawler"`
			HuntingSeason struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
					} `json:"objectives"`
				} `json:"active"`
			} `json:"hunting_season"`
			UhcAddict struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
						Play int `json:"play"`
					} `json:"objectives"`
				} `json:"active"`
			} `json:"uhc_addict"`
			UhcMadness struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
					} `json:"objectives"`
				} `json:"active"`
			} `json:"uhc_madness"`
			TntPvprunDaily struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
						TntPvprunDaily int `json:"tnt_pvprun_daily"`
					} `json:"objectives"`
				} `json:"active"`
			} `json:"tnt_pvprun_daily"`
			TntDailyWin struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
					} `json:"objectives"`
				} `json:"active"`
			} `json:"tnt_daily_win"`
			TntBowspleefDaily struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"tnt_bowspleef_daily"`
			TntWeeklyPlay struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
						TntWeeklyPlay int `json:"tnt_weekly_play"`
					} `json:"objectives"`
				} `json:"active"`
			} `json:"tnt_weekly_play"`
			TntTnttagDaily struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"tnt_tnttag_daily"`
			TntTnttagWeekly struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
						TntTnttagWeekly int `json:"tnt_tnttag_weekly"`
					} `json:"objectives"`
				} `json:"active"`
			} `json:"tnt_tnttag_weekly"`
			TntBowspleefWeekly struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"tnt_bowspleef_weekly"`
			TntPvprunWeekly struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
						TntPvprunWeekly int `json:"tnt_pvprun_weekly"`
					} `json:"objectives"`
				} `json:"active"`
			} `json:"tnt_pvprun_weekly"`
			TntTntrunWeekly struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"tnt_tntrun_weekly"`
			TntTntrunDaily struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"tnt_tntrun_daily"`
			TntWizardsDaily struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
					} `json:"objectives"`
				} `json:"active"`
			} `json:"tnt_wizards_daily"`
			TntWizardsWeekly struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
					} `json:"objectives"`
				} `json:"active"`
			} `json:"tnt_wizards_weekly"`
			SkywarsTeamKills struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"skywars_team_kills"`
			SkywarsTeamWin struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"skywars_team_win"`
			SkywarsSoloKills struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"skywars_solo_kills"`
			SkywarsSoloWin struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"skywars_solo_win"`
			SkywarsArcadeWin struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"skywars_arcade_win"`
			SkywarsCorruptWin struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"skywars_corrupt_win"`
			SkywarsWeeklyKills struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"skywars_weekly_kills"`
			SkywarsWeeklyArcadeWinAll struct {
				Active struct {
					Started    int64 `json:"started"`
					Objectives struct {
						SkywarsArcadeWeeklyWin int `json:"skywars_arcade_weekly_win"`
					} `json:"objectives"`
				} `json:"active"`
			} `json:"skywars_weekly_arcade_win_all"`
			PrototypePitDailyKills struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"prototype_pit_daily_kills"`
			PrototypePitDailyContract struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"prototype_pit_daily_contract"`
			PrototypePitWeeklyGold struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"prototype_pit_weekly_gold"`
			BedwarsDailyOneMore struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"bedwars_daily_one_more"`
			BedwarsDailyWin struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"bedwars_daily_win"`
			BedwarsWeeklyBedElims struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"bedwars_weekly_bed_elims"`
			BedwarsWeeklyDreamWin struct {
				Active struct {
					Objectives struct {
						BedwarsDreamWins int `json:"bedwars_dream_wins"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"bedwars_weekly_dream_win"`
			BedwarsDailyNightmares struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"bedwars_daily_nightmares"`
			BedwarsWeeklyPumpkinator struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"bedwars_weekly_pumpkinator"`
			SkywarsWeeklyFreeLootChest struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"skywars_weekly_free_loot_chest"`
			SkywarsHalloweenHarvest2019 struct {
				Active struct {
					Objectives struct {
						SkywarsHalloweenKills int `json:"skywars_halloween_kills"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"skywars_halloween_harvest_2019"`
			SkywarsDailyTokens struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"skywars_daily_tokens"`
			SkywarsSpecialNorthPole struct {
				Active struct {
					Objectives struct {
						SkywarsSpecialGifts int `json:"skywars_special_gifts"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"skywars_special_north_pole"`
			BedwarsDailyGifts struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"bedwars_daily_gifts"`
			DuelsPlayer struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"duels_player"`
			DuelsWinner struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"duels_winner"`
			DuelsWeeklyWins struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"duels_weekly_wins"`
			DuelsKiller struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"duels_killer"`
			DuelsWeeklyKills struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"duels_weekly_kills"`
			BlitzGameOfTheDay struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"blitz_game_of_the_day"`
			BlitzWin struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"blitz_win"`
			BlitzLootChestDaily struct {
				Active struct {
					Objectives struct {
						Lootchestblitz int `json:"lootchestblitz"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"blitz_loot_chest_daily"`
			BlitzWeeklyMaster struct {
				Active struct {
					Objectives struct {
						BlitzGamesPlayed int `json:"blitz_games_played"`
						Killblitz10      int `json:"killblitz10"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"blitz_weekly_master"`
			BlitzKills struct {
				Active struct {
					Objectives struct {
						Killblitz10 int `json:"killblitz10"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"blitz_kills"`
			BlitzLootChestWeekly struct {
				Active struct {
					Objectives struct {
						Lootchestblitz  int `json:"lootchestblitz"`
						Dealdamageblitz int `json:"dealdamageblitz"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"blitz_loot_chest_weekly"`
			CvcWinDailyNormal struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"cvc_win_daily_normal"`
			CvcKillDailyNormal struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"cvc_kill_daily_normal"`
			CvcKill struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"cvc_kill"`
			CvcWinDailyDeathmatch struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"cvc_win_daily_deathmatch"`
			CvcKillWeekly struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"cvc_kill_weekly"`
			WallsDailyKill struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"walls_daily_kill"`
			WallsDailyPlay struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"walls_daily_play"`
			WallsDailyWin struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"walls_daily_win"`
			WallsWeekly struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"walls_weekly"`
			Paintballer struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"paintballer"`
			PaintballKiller struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"paintball_killer"`
			PaintballExpert struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"paintball_expert"`
			VampirezWeeklyKill struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"vampirez_weekly_kill"`
			VampirezWeeklyWin struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"vampirez_weekly_win"`
			VampirezDailyHumanKill struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"vampirez_daily_human_kill"`
			VampirezDailyKill struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"vampirez_daily_kill"`
			VampirezWeeklyHumanKill struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"vampirez_weekly_human_kill"`
			VampirezDailyWin struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"vampirez_daily_win"`
			VampirezDailyPlay struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"vampirez_daily_play"`
			QuakeWeeklyPlay struct {
				Active struct {
					Objectives struct {
						QuakeWeeklyPlay int `json:"quake_weekly_play"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"quake_weekly_play"`
			QuakeDailyWin struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"quake_daily_win"`
			QuakeDailyKill struct {
				Active struct {
					Objectives struct {
						QuakeDailyKill int `json:"quake_daily_kill"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"quake_daily_kill"`
			QuakeDailyPlay struct {
				Active struct {
					Objectives struct {
						QuakeDailyPlay int `json:"quake_daily_play"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"quake_daily_play"`
			ArcadeSpecialist struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"arcade_specialist"`
			ArcadeWinner struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"arcade_winner"`
			ArcadeGamer struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"arcade_gamer"`
			MegaWallsPlay struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"mega_walls_play"`
			MegaWallsFaithful struct {
				Active struct {
					Objectives struct {
						MegaWallsFaithfulPlay int `json:"mega_walls_faithful_play"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"mega_walls_faithful"`
			MegaWallsWin struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"mega_walls_win"`
			MegaWallsKill struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"mega_walls_kill"`
			MegaWallsWeekly struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"mega_walls_weekly"`
			ArenaDailyPlay struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"arena_daily_play"`
			ArenaDailyKills struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"arena_daily_kills"`
			ArenaDailyWins struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"arena_daily_wins"`
			ArenaWeeklyPlay struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"arena_weekly_play"`
			CrazyWallsDailyKill struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"crazy_walls_daily_kill"`
			CrazyWallsDailyPlay struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"crazy_walls_daily_play"`
			CrazyWallsDailyWin struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"crazy_walls_daily_win"`
			CrazyWallsWeekly struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"crazy_walls_weekly"`
			UhcTeam struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"uhc_team"`
			UhcSolo struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"uhc_solo"`
			UhcDm struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"uhc_dm"`
			UhcWeekly struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"uhc_weekly"`
			SoloBrawler struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"solo_brawler"`
			TeamBrawler struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"team_brawler"`
			GingerbreadBlingBling struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"gingerbread_bling_bling"`
			GingerbreadMaps struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"gingerbread_maps"`
			GingerbreadRacer struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"gingerbread_racer"`
			GingerbreadMastery struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"gingerbread_mastery"`
			SupersmashSoloWin struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"supersmash_solo_win"`
			SupersmashTeamWin struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"supersmash_team_win"`
			SupersmashSoloKills struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"supersmash_solo_kills"`
			SupersmashWeeklyKills struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"supersmash_weekly_kills"`
			SupersmashTeamKills struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"supersmash_team_kills"`
			MmWeeklyWins struct {
				Active struct {
					Objectives struct {
						MmWeeklyWin int `json:"mm_weekly_win"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"mm_weekly_wins"`
			MmWeeklyMurdererKills struct {
				Active struct {
					Objectives struct {
						MmWeeklyKillsAsMurderer int `json:"mm_weekly_kills_as_murderer"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"mm_weekly_murderer_kills"`
			MmDailyTargetKill struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"mm_daily_target_kill"`
			MmDailyPowerPlay struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"mm_daily_power_play"`
			MmDailyWin struct {
				Completions []struct {
					Time int64 `json:"time"`
				} `json:"completions"`
			} `json:"mm_daily_win"`
			BuildBattleWeekly struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"build_battle_weekly"`
			BuildBattleWinner struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"build_battle_winner"`
			BuildBattlePlayer struct {
				Active struct {
					Objectives struct {
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"build_battle_player"`
			SkywarsHalloweenHarvest2020 struct {
				Active struct {
					Objectives struct {
						SkywarsHalloweenKills         int `json:"skywars_halloween_kills"`
						SkywarsHalloweenCorruptedWins int `json:"skywars_halloween_corrupted_wins"`
					} `json:"objectives"`
					Started int64 `json:"started"`
				} `json:"active"`
			} `json:"skywars_halloween_harvest_2020"`
		} `json:"quests"`
		LastAdsenseGenerateTime int64 `json:"lastAdsenseGenerateTime"`
		LastClaimedReward       int64 `json:"lastClaimedReward"`
		TotalRewards            int   `json:"totalRewards"`
		TotalDailyRewards       int   `json:"totalDailyRewards"`
		RewardStreak            int   `json:"rewardStreak"`
		RewardScore             int   `json:"rewardScore"`
		RewardHighScore         int   `json:"rewardHighScore"`
		AdsenseTokens           int   `json:"adsense_tokens"`
		Challenges              struct {
			AllTime struct {
				ARCADEHypixelSaysChallenge       int `json:"ARCADE__hypixel_says_challenge"`
				DUELSFeedTheVoidChallenge        int `json:"DUELS__feed_the_void_challenge"`
				BEDWARSOffensive                 int `json:"BEDWARS__offensive"`
				BEDWARSSupport                   int `json:"BEDWARS__support"`
				UHCPerfectStartChallenge         int `json:"UHC__perfect_start_challenge"`
				UHCThreatChallenge               int `json:"UHC__threat_challenge"`
				ARCADEZombiesChallenge           int `json:"ARCADE__zombies_challenge"`
				SKYWARSRushChallenge             int `json:"SKYWARS__rush_challenge"`
				DUELSTeamsChallenge              int `json:"DUELS__teams_challenge"`
				SKYWARSFeedingTheVoidChallenge   int `json:"SKYWARS__feeding_the_void_challenge"`
				TNTGAMESTntTagChallenge          int `json:"TNTGAMES__tnt_tag_challenge"`
				TNTGAMESPvpRunChallenge          int `json:"TNTGAMES__pvp_run_challenge"`
				SKYWARSEndermanChallenge         int `json:"SKYWARS__enderman_challenge"`
				ARCADECreeperAttackChallenge     int `json:"ARCADE__creeper_attack_challenge"`
				SURVIVALGAMESResistanceChallenge int `json:"SURVIVAL_GAMES__resistance_challenge"`
				MURDERMYSTERYMurderSpree         int `json:"MURDER_MYSTERY__murder_spree"`
				MURDERMYSTERYHero                int `json:"MURDER_MYSTERY__hero"`
				MURDERMYSTERYSherlock            int `json:"MURDER_MYSTERY__sherlock"`
			} `json:"all_time"`
		} `json:"challenges"`
		AchievementSync struct {
			QuakeTiered int `json:"quake_tiered"`
		} `json:"achievementSync"`
		Voting struct {
			Total           int   `json:"total"`
			TotalMcsorg     int   `json:"total_mcsorg"`
			SecondaryMcsorg int   `json:"secondary_mcsorg"`
			LastMcsorg      int64 `json:"last_mcsorg"`
			LastVote        int64 `json:"last_vote"`
			VotesToday      int   `json:"votesToday"`
		} `json:"voting"`
		LevelingReward8 bool `json:"levelingReward_8"`
		LevelingReward7 bool `json:"levelingReward_7"`
		SocialMedia     struct {
			Links struct {
				Discord string `json:"DISCORD"`
			} `json:"links"`
			Prompt bool `json:"prompt"`
		} `json:"socialMedia"`
		LevelingReward9  bool `json:"levelingReward_9"`
		LevelingReward10 bool `json:"levelingReward_10"`
		LevelingReward11 bool `json:"levelingReward_11"`
		LevelingReward12 bool `json:"levelingReward_12"`
		LevelingReward13 bool `json:"levelingReward_13"`
		LevelingReward14 bool `json:"levelingReward_14"`
		LevelingReward15 bool `json:"levelingReward_15"`
		LevelingReward16 bool `json:"levelingReward_16"`
		LevelingReward17 bool `json:"levelingReward_17"`
		LevelingReward18 bool `json:"levelingReward_18"`
		LevelingReward19 bool `json:"levelingReward_19"`
		LevelingReward20 bool `json:"levelingReward_20"`
		LevelingReward21 bool `json:"levelingReward_21"`
		GiftingMeta      struct {
			BundlesReceived     int      `json:"bundlesReceived"`
			RealBundlesReceived int      `json:"realBundlesReceived"`
			BundlesGiven        int      `json:"bundlesGiven"`
			GiftsGiven          int      `json:"giftsGiven"`
			RealBundlesGiven    int      `json:"realBundlesGiven"`
			Milestones          []string `json:"milestones"`
		} `json:"giftingMeta"`
		FortuneBuff      int  `json:"fortuneBuff"`
		LevelingReward22 bool `json:"levelingReward_22"`
		LevelingReward23 bool `json:"levelingReward_23"`
		LevelingReward24 bool `json:"levelingReward_24"`
		Eugene           struct {
			DailyTwoKExp int64 `json:"dailyTwoKExp"`
		} `json:"eugene"`
		LevelingReward25           bool   `json:"levelingReward_25"`
		LevelingReward26           bool   `json:"levelingReward_26"`
		OnetimeAchievementMenuSort string `json:"onetime_achievement_menu_sort"`
		SpecFirstPerson            bool   `json:"spec_first_person"`
		ClaimedSoloBank            int64  `json:"claimed_solo_bank"`
		LevelingReward27           bool   `json:"levelingReward_27"`
		LevelingReward28           bool   `json:"levelingReward_28"`
		ParkourCheckpointBests     struct {
			Bedwars struct {
				Num0 int `json:"0"`
				Num1 int `json:"1"`
				Num2 int `json:"2"`
				Num3 int `json:"3"`
			} `json:"Bedwars"`
			MainLobby2017 struct {
				Num0 int `json:"0"`
				Num1 int `json:"1"`
				Num2 int `json:"2"`
			} `json:"mainLobby2017"`
			Duels struct {
				Num0 int `json:"0"`
				Num1 int `json:"1"`
				Num2 int `json:"2"`
				Num3 int `json:"3"`
			} `json:"Duels"`
			Prototype struct {
				Num0 int `json:"0"`
			} `json:"Prototype"`
			Tourney struct {
				Num0 int `json:"0"`
				Num1 int `json:"1"`
				Num2 int `json:"2"`
				Num3 int `json:"3"`
			} `json:"Tourney"`
		} `json:"parkourCheckpointBests"`
		LevelingReward29   bool `json:"levelingReward_29"`
		LevelingReward30   bool `json:"levelingReward_30"`
		ParkourCompletions struct {
			MainLobby2017 []struct {
				TimeStart int64 `json:"timeStart"`
				TimeTook  int   `json:"timeTook"`
			} `json:"mainLobby2017"`
			Tourney []struct {
				TimeStart int64 `json:"timeStart"`
				TimeTook  int   `json:"timeTook"`
			} `json:"Tourney"`
			Bedwars []struct {
				TimeStart int64 `json:"timeStart"`
				TimeTook  int   `json:"timeTook"`
			} `json:"Bedwars"`
			Duels []struct {
				TimeStart int64 `json:"timeStart"`
				TimeTook  int   `json:"timeTook"`
			} `json:"Duels"`
		} `json:"parkourCompletions"`
		LevelingReward31 bool `json:"levelingReward_31"`
		LevelingReward32 bool `json:"levelingReward_32"`
		Monthlycrates    struct {
			One02018 struct {
				Regular bool `json:"REGULAR"`
			} `json:"10-2018"`
			One02019 struct {
				Regular bool `json:"REGULAR"`
			} `json:"10-2019"`
			One12018 struct {
				Regular bool `json:"REGULAR"`
			} `json:"11-2018"`
			One22018 struct {
				Regular bool `json:"REGULAR"`
			} `json:"12-2018"`
			Six2019 struct {
				Regular bool `json:"REGULAR"`
			} `json:"6-2019"`
			Seven2019 struct {
				Regular bool `json:"REGULAR"`
			} `json:"7-2019"`
			Nine2018 struct {
				Regular bool `json:"REGULAR"`
			} `json:"9-2018"`
			Nine2019 struct {
				Regular bool `json:"REGULAR"`
			} `json:"9-2019"`
			One12019 struct {
				Regular bool `json:"REGULAR"`
			} `json:"11-2019"`
			One22019 struct {
				Regular bool `json:"REGULAR"`
			} `json:"12-2019"`
			One2020 struct {
				Regular bool `json:"REGULAR"`
				Vip     bool `json:"VIP"`
			} `json:"1-2020"`
			Two2020 struct {
				Vip     bool `json:"VIP"`
				Regular bool `json:"REGULAR"`
			} `json:"2-2020"`
			Three2020 struct {
				Regular bool `json:"REGULAR"`
				Vip     bool `json:"VIP"`
			} `json:"3-2020"`
			Four2020 struct {
				Regular bool `json:"REGULAR"`
				Vip     bool `json:"VIP"`
			} `json:"4-2020"`
			Five2020 struct {
				Regular bool `json:"REGULAR"`
				Vip     bool `json:"VIP"`
			} `json:"5-2020"`
			Six2020 struct {
				Regular bool `json:"REGULAR"`
				Vip     bool `json:"VIP"`
			} `json:"6-2020"`
			Seven2020 struct {
				Regular bool `json:"REGULAR"`
				Vip     bool `json:"VIP"`
			} `json:"7-2020"`
			Eight2020 struct {
				Regular bool `json:"REGULAR"`
				Vip     bool `json:"VIP"`
			} `json:"8-2020"`
			One02020 struct {
				Regular bool `json:"REGULAR"`
				Vip     bool `json:"VIP"`
			} `json:"10-2020"`
			One12020 struct {
				Vip     bool `json:"VIP"`
				Regular bool `json:"REGULAR"`
			} `json:"11-2020"`
			One22020 struct {
				Vip     bool `json:"VIP"`
				Regular bool `json:"REGULAR"`
			} `json:"12-2020"`
			One2021 struct {
				Regular bool `json:"REGULAR"`
				Vip     bool `json:"VIP"`
			} `json:"1-2021"`
		} `json:"monthlycrates"`
		LevelingReward33 bool `json:"levelingReward_33"`
		LevelingReward34 bool `json:"levelingReward_34"`
		LevelingReward35 bool `json:"levelingReward_35"`
		PetStats         struct {
			BouncySpider struct {
				Exercise struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"EXERCISE"`
				Thirst struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"THIRST"`
				Hunger struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"HUNGER"`
				Name       string `json:"name"`
				Experience int    `json:"experience"`
			} `json:"BOUNCY_SPIDER"`
			CreeperPowered struct {
				Thirst struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"THIRST"`
				Hunger struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"HUNGER"`
				Exercise struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"EXERCISE"`
				Experience int    `json:"experience"`
				Name       string `json:"name"`
			} `json:"CREEPER_POWERED"`
			Creeper struct {
				Hunger struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"HUNGER"`
				Thirst struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"THIRST"`
				Exercise struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"EXERCISE"`
				Experience int `json:"experience"`
			} `json:"CREEPER"`
			HorseCreamy struct {
				Hunger struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"HUNGER"`
				Thirst struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"THIRST"`
				Exercise struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"EXERCISE"`
				Experience int `json:"experience"`
			} `json:"HORSE_CREAMY"`
			Silverfish struct {
				Hunger struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"HUNGER"`
				Thirst struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"THIRST"`
				Exercise struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"EXERCISE"`
				Experience int `json:"experience"`
			} `json:"SILVERFISH"`
			CatRed struct {
				Exercise struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"EXERCISE"`
				Thirst struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"THIRST"`
				Hunger struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"HUNGER"`
				Experience int `json:"experience"`
			} `json:"CAT_RED"`
			CatSiamese struct {
				Hunger struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"HUNGER"`
				Thirst struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"THIRST"`
				Exercise struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"EXERCISE"`
				Experience int `json:"experience"`
			} `json:"CAT_SIAMESE"`
			Chicken struct {
				Hunger struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"HUNGER"`
				Thirst struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"THIRST"`
				Exercise struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"EXERCISE"`
				Experience int `json:"experience"`
			} `json:"CHICKEN"`
			Cow struct {
				Hunger struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"HUNGER"`
				Thirst struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"THIRST"`
				Exercise struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"EXERCISE"`
				Experience int `json:"experience"`
			} `json:"COW"`
			Herobrine struct {
				Thirst struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"THIRST"`
				Hunger struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"HUNGER"`
				Exercise struct {
					Timestamp int64 `json:"timestamp"`
					Value     int   `json:"value"`
				} `json:"EXERCISE"`
				Experience int    `json:"experience"`
				Name       string `json:"name"`
			} `json:"HEROBRINE"`
		} `json:"petStats"`
		TieredAchievementMenuSort string `json:"tiered_achievement_menu_sort"`
		Tourney                   struct {
			FirstJoinLobby int64 `json:"first_join_lobby"`
			McgoDefusal0   struct {
				SeenRPbook     bool  `json:"seenRPbook"`
				Playtime       int   `json:"playtime"`
				FirstWin       int64 `json:"first_win"`
				TributesEarned int   `json:"tributes_earned"`
			} `json:"mcgo_defusal_0"`
			TotalTributes    int `json:"total_tributes"`
			SwInsaneDoubles0 struct {
				GamesPlayed    int   `json:"games_played"`
				Playtime       int   `json:"playtime"`
				TributesEarned int   `json:"tributes_earned"`
				FirstWin       int64 `json:"first_win"`
			} `json:"sw_insane_doubles_0"`
			Bedwars4S1 struct {
				GamesPlayed    int   `json:"games_played"`
				Playtime       int   `json:"playtime"`
				TributesEarned int   `json:"tributes_earned"`
				FirstWin       int64 `json:"first_win"`
			} `json:"bedwars4s_1"`
		} `json:"tourney"`
		LevelingReward36    bool   `json:"levelingReward_36"`
		LevelingReward37    bool   `json:"levelingReward_37"`
		LevelingReward38    bool   `json:"levelingReward_38"`
		LevelingReward39    bool   `json:"levelingReward_39"`
		LevelingReward40    bool   `json:"levelingReward_40"`
		LevelingReward41    bool   `json:"levelingReward_41"`
		LevelingReward42    bool   `json:"levelingReward_42"`
		LevelingReward43    bool   `json:"levelingReward_43"`
		LevelingReward44    bool   `json:"levelingReward_44"`
		LevelingReward45    bool   `json:"levelingReward_45"`
		GiftsGrinch         int    `json:"gifts_grinch"`
		LevelingReward46    bool   `json:"levelingReward_46"`
		LevelingReward47    bool   `json:"levelingReward_47"`
		NewPackageRank      string `json:"newPackageRank"`
		LevelUpVIP          int64  `json:"levelUp_VIP"`
		PetJourneyTimestamp int64  `json:"petJourneyTimestamp"`
		LevelingReward48    bool   `json:"levelingReward_48"`
		LevelingReward49    bool   `json:"levelingReward_49"`
		LevelingReward50    bool   `json:"levelingReward_50"`
		LevelingReward51    bool   `json:"levelingReward_51"`
		LevelingReward52    bool   `json:"levelingReward_52"`
		CurrentCloak        string `json:"currentCloak"`
		LevelingReward53    bool   `json:"levelingReward_53"`
		LevelingReward54    bool   `json:"levelingReward_54"`
		LevelingReward55    bool   `json:"levelingReward_55"`
		LevelingReward56    bool   `json:"levelingReward_56"`
		LevelingReward57    bool   `json:"levelingReward_57"`
		LevelingReward58    bool   `json:"levelingReward_58"`
		LevelingReward59    bool   `json:"levelingReward_59"`
		LevelingReward60    bool   `json:"levelingReward_60"`
		LevelingReward61    bool   `json:"levelingReward_61"`
		LevelingReward62    bool   `json:"levelingReward_62"`
		LevelingReward63    bool   `json:"levelingReward_63"`
		LevelingReward64    bool   `json:"levelingReward_64"`
		LevelingReward65    bool   `json:"levelingReward_65"`
		LevelingReward66    bool   `json:"levelingReward_66"`
		LevelingReward67    bool   `json:"levelingReward_67"`
		LevelingReward68    bool   `json:"levelingReward_68"`
		LevelingReward69    bool   `json:"levelingReward_69"`
		LevelingReward70    bool   `json:"levelingReward_70"`
		AchievementTotem    struct {
			CanCustomize     bool     `json:"canCustomize"`
			AllowedMaxHeight int      `json:"allowed_max_height"`
			UnlockedParts    []string `json:"unlockedParts"`
			SelectedParts    struct {
				Slot0 string `json:"slot_0"`
				Slot1 string `json:"slot_1"`
			} `json:"selectedParts"`
			UnlockedColors []string `json:"unlockedColors"`
			SelectedColors struct {
				Slotcolor0 string `json:"slotcolor_0"`
			} `json:"selectedColors"`
		} `json:"achievementTotem"`
		LevelingReward71      bool   `json:"levelingReward_71"`
		CollectiblesMenuSort  string `json:"collectibles_menu_sort"`
		ParticlePack          string `json:"particlePack"`
		LevelingReward72      bool   `json:"levelingReward_72"`
		LevelingReward73      bool   `json:"levelingReward_73"`
		LevelingReward74      bool   `json:"levelingReward_74"`
		LevelingReward75      bool   `json:"levelingReward_75"`
		LevelingReward76      bool   `json:"levelingReward_76"`
		VanityFavorites       string `json:"vanityFavorites"`
		CurrentPet            string `json:"currentPet"`
		LevelingReward77      bool   `json:"levelingReward_77"`
		LevelingReward78      bool   `json:"levelingReward_78"`
		LevelingReward79      bool   `json:"levelingReward_79"`
		LevelingReward80      bool   `json:"levelingReward_80"`
		LevelingReward81      bool   `json:"levelingReward_81"`
		LevelingReward82      bool   `json:"levelingReward_82"`
		LevelingReward83      bool   `json:"levelingReward_83"`
		LevelingReward84      bool   `json:"levelingReward_84"`
		LevelingReward85      bool   `json:"levelingReward_85"`
		LevelingReward86      bool   `json:"levelingReward_86"`
		LevelingReward87      bool   `json:"levelingReward_87"`
		LevelingReward88      bool   `json:"levelingReward_88"`
		LevelingReward89      bool   `json:"levelingReward_89"`
		ClaimedPotatoTalisman int64  `json:"claimed_potato_talisman"`
		LevelingReward90      bool   `json:"levelingReward_90"`
		Main2017Tutorial      bool   `json:"main2017Tutorial"`
		LevelingReward91      bool   `json:"levelingReward_91"`
		LevelingReward92      bool   `json:"levelingReward_92"`
		LevelingReward93      bool   `json:"levelingReward_93"`
		LevelingReward94      bool   `json:"levelingReward_94"`
		LevelingReward95      bool   `json:"levelingReward_95"`
		LevelingReward96      bool   `json:"levelingReward_96"`
		LevelingReward97      bool   `json:"levelingReward_97"`
		LevelingReward98      bool   `json:"levelingReward_98"`
		LevelingReward99      bool   `json:"levelingReward_99"`
		SkyblockFreeCookie    int64  `json:"skyblock_free_cookie"`
		ScorpiusBribe96       int64  `json:"scorpius_bribe_96"`
		ClaimedCenturyCake    int64  `json:"claimed_century_cake"`
		Channel               string `json:"channel"`
		ScorpiusBribe120      int64  `json:"scorpius_bribe_120"`
		AdventRewards2019 map[string]int64 `json:"adventRewards2019"`
		AdventRewards2020 map[string]int64 `json:"adventRewards2020"`
		MostRecentGameType string `json:"mostRecentGameType"`
	} `json:"player"`
}
