package api_test

import (
	"testing"
	"os"

	"github.com/zemlyak/hypetrain/pkg/api"
)

func TestKey(t *testing.T) {
	hypetrain := api.New(os.Getenv("HYPIXEL_KEY"));
	res, err := hypetrain.Key(os.Getenv("HYPIXEL_KEY"))
    t.Logf("%+v", res)
	if err != nil {
		t.Errorf("Error: %s", err)
	}
}
