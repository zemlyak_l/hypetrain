package api

import "encoding/json"

type Params struct{Id string; Player string; Name string};

func (client *Client) getJson(url string, target interface{}) (err error) {
	_, res, err := client.http.Get(nil, url);
	if err != nil {
		return
	}
	return json.Unmarshal(res, &target)
}
